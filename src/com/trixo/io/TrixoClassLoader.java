package com.trixo.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.trixo.data.DataCenter;

@SuppressWarnings("unused")
public class TrixoClassLoader {
    private final Class<?>[] parameters = new Class[] {
            URL.class
    };

    /** PackLoader Functions **/
    public ArrayList<Class<?>> loadJars(File folder) {
        if (!folder.exists())
            folder.mkdirs();

        if (folder.isDirectory()) {
            ArrayList<Class<?>> classes = new ArrayList<Class<?>>();

            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    this.loadJars(file);
                } else {
                    if (file.getName().endsWith(".jar")) {
                        try {
                            this.addFile(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        try {
                            classes.add(this.getClassLoader().loadClass(this.readInfo(file).get("class")));
                        } catch (SecurityException | ClassNotFoundException | IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            return classes;
        }

        return new ArrayList<Class<?>>();
    }

    public ArrayList<Class<?>> loadClasses(File folder) {
        if (!folder.exists())
            folder.mkdirs();

        if (folder.isDirectory()) {
            ArrayList<Class<?>> classes = new ArrayList<Class<?>>();

            try {
                this.addFile(folder);
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    return this.loadClasses(file);
                } else {
                    if (file.getName().endsWith(".class") && !file.getName().contains("$")) {                        
                        try {
                            classes.add(this.getClassLoader().loadClass(file.getName().replace(".class", "")));
                        } catch (SecurityException | ClassNotFoundException | IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            return classes;
        }

        return new ArrayList<Class<?>>();
    }

    public Class<?> loadClass(File classFile) {
        if (classFile.isFile()) {
            if (classFile.getName().endsWith(".class")) {
                try {
                    this.addFile(classFile.getParentFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    return this.getClassLoader().loadClass(classFile.getName().replace(".class", ""));
                } catch (SecurityException | ClassNotFoundException | IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public HashMap<String, String> readInfo(File file) {
        HashMap<String, String> info = new HashMap<String, String>();

        try {
            URL url = new URL(
                    "jar:file:/" + (file.getAbsolutePath().replace(File.separator, "/")) + "!/com/trixo/io/info.txt");

            InputStream is = url.openStream();
            BufferedReader input = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            info.put("ID", input.readLine().split(":")[1].trim());
            info.put("name", input.readLine().split(":")[1].trim());
            info.put("class", input.readLine().split(":")[1].trim());

            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return info;
    }

    /** Getters, Adders **/
    /* Getters */
    public ClassLoader getClassLoader() {
        return DataCenter.retrieveData("api", "pluginClassLoader", ClassLoader.class);
    }

    /* Adders */
    private void addFile(String file) throws IOException {
        this.addFile(new File(file));
    }

    private void addFile(File file) throws IOException {
        this.addURL(file.toURI().toURL());
    }

    private void addURL(URL url) throws IOException {
        URLClassLoader sysloader = (URLClassLoader) this.getClassLoader();
        Class<?> sysclass = URLClassLoader.class;

        try {
            Method method = sysclass.getDeclaredMethod("addURL", this.parameters);
            method.setAccessible(true);
            method.invoke(sysloader, new Object[] {
                    url
            });
        } catch (Throwable t) {
            t.printStackTrace();
            throw new IOException("Error! Could not add the URL to system classloader.");
        }
    }
}