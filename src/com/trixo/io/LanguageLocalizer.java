package com.trixo.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.trixo.data.DataContainer;

@SuppressWarnings("unchecked")
public class LanguageLocalizer {
    private static DataContainer<String, Object> data = null;

    /** Constructor **/
    public LanguageLocalizer() {
        data = new DataContainer<String, Object>(false);
        data.put("tokens", new DataContainer<String, DataContainer<String, String>>(false));
    }

    /** LanguageLocalizer Functions **/
    public void loadLanguages(String loc) {
        File folder = new File(loc);

        if (!folder.isDirectory()) {
            if (!folder.exists()) {
                folder.mkdirs();
            }

            return;
        }

        for (File file : folder.listFiles()) {
            if (file.isFile()) {
                try {
                    DataContainer<String, String> langTokens = new DataContainer<String, String>(false);
                    String language = file.getName().replace(".trixlang", "");

                    {
                        List<String> lines = Files.readLines(file, Charsets.UTF_8);

                        for (String line : lines) {
                            String key = line.split("=")[0].trim();
                            String value = line.split("=")[1].replace("\"", "").trim();

                            langTokens.put(key, value);
                        }
                    }

                    getTokens().put(language, langTokens);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /** Getters **/
    private static DataContainer<String, DataContainer<String, String>> getTokens() {
        return (DataContainer<String, DataContainer<String, String>>) data.get("tokens");
    }

    public static String get(String key, String language) {
        return getTokens().containsKey(language) ? getTokens().get(language).get(key) : key;
    }
}
