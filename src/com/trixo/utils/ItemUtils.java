package com.trixo.utils;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

public class ItemUtils {
    public static void addGlow(org.bukkit.inventory.ItemStack stack) {
        ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
        NBTTagCompound compound = nmsStack.getTag();

        if (compound == null) {
            compound = new NBTTagCompound();
            nmsStack.setTag(compound);
        }

        compound.set("ench", new NBTTagList());
    }

    public static boolean skullEquals(org.bukkit.inventory.ItemStack s1, org.bukkit.inventory.ItemStack s2) {
        if (s1.equals(s2) || s1.isSimilar(s2))
            return true;

        if (s1.getType() == Material.SKULL_ITEM && s2.getType() == Material.SKULL_ITEM) {
            if (((SkullMeta) s1.getItemMeta()).getOwner().equals(((SkullMeta) s2.getItemMeta()).getOwner())
                    && s1.getItemMeta().getDisplayName().equals(s2.getItemMeta().getDisplayName())
                    && s1.getAmount() == s2.getAmount() && s1.getDurability() == s2.getDurability()
                    && s1.getEnchantments() == s2.getEnchantments()) {
                return true;
            }
        }

        return false;
    }
}
