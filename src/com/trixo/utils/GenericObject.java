package com.trixo.utils;

import java.lang.reflect.ParameterizedType;

public abstract class GenericObject<T> {
    public Class<?> getGenericType() throws Exception {
        return (Class<?>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
}
