package com.trixo.utils;

import java.lang.reflect.Field;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.util.UnsafeList;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.PathfinderGoal;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;

public class EntityUtils {
    /** EntityUtils Functions **/
    public static void clearAI(Entity entity) {
        clearAI(((CraftEntity) entity).getHandle());
    }

    public static void clearAI(net.minecraft.server.v1_8_R3.Entity entity) {
        try {
            EntityLiving ev = (EntityLiving) entity;

            // Goal Selector
            {
                Field goalsField = EntityInsentient.class.getDeclaredField("goalSelector");
                goalsField.setAccessible(true);

                Object goals = goalsField.get(ev);

                Field listField = PathfinderGoalSelector.class.getDeclaredField("b");
                listField.setAccessible(true);
                listField.set(goals, new UnsafeList<Object>());
            }

            // Target Selector
            {
                Field targetsField = EntityInsentient.class.getDeclaredField("targetSelector");
                targetsField.setAccessible(true);

                Object targets = targetsField.get(ev);

                Field listField2 = PathfinderGoalSelector.class.getDeclaredField("b");
                listField2.setAccessible(true);
                listField2.set(targets, new UnsafeList<Object>());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Setters **/
    /* Setters */
    public static void setEntitySilent(Entity entity, boolean silent) {
        net.minecraft.server.v1_8_R3.Entity mcEntity = ((CraftEntity) entity).getHandle();
        mcEntity.b(silent);
    }

    public static void setNoAI(Entity entity) {
        net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity) entity).getHandle();

        NBTTagCompound tag = nmsEntity.getNBTTag();
        if (tag == null) {
            tag = new NBTTagCompound();
        }

        nmsEntity.c(tag);
        tag.setInt("NoAI", 1);
        nmsEntity.f(tag);
    }

    public static void setAIGoal(Entity entity, int index, PathfinderGoal aiGoal) {
        setAIGoal(((CraftLivingEntity) entity).getHandle(), index, aiGoal);
    }

    public static void setAIGoal(net.minecraft.server.v1_8_R3.Entity entity, int index, PathfinderGoal aiGoal) {
        try {
            EntityLiving ev = (EntityLiving) entity;

            // Goal Selector
            Field goalsField = EntityInsentient.class.getDeclaredField("goalSelector");
            goalsField.setAccessible(true);

            PathfinderGoalSelector goals = (PathfinderGoalSelector) goalsField.get(ev);
            goals.a(index, aiGoal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setAITarget(Entity entity, int index, PathfinderGoal aiGoal) {
        setAITarget(entity, index, aiGoal);
    }

    public static void setAITarget(net.minecraft.server.v1_8_R3.Entity entity, int index, PathfinderGoal aiGoal) {
        try {
            EntityLiving ev = (EntityLiving) entity;

            // Target Selector
            Field targetsField = EntityInsentient.class.getDeclaredField("targetSelector");
            targetsField.setAccessible(true);

            PathfinderGoalSelector targets = (PathfinderGoalSelector) targetsField.get(ev);
            targets.a(index, aiGoal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setVelocity(Entity entity, double strafe, double yAdd, double yMax, boolean groundBoost) {
        setVelocity(entity, entity.getLocation().getDirection(), strafe, false, 0, yAdd, yMax, groundBoost);
    }

    public static void setVelocity(Entity entity, Vector direction, double strafe, boolean ySet, double yBase, double yAdd,
            double yMax, boolean groundBoost) {
        if (Double.isNaN(direction.getX()) || Double.isNaN(direction.getY()) || Double.isNaN(direction.getZ()) || direction.length() == 0)
            return;

        if (ySet)
            direction.setY(yBase);

        direction.normalize();
        direction.multiply(strafe);
        direction.setY(direction.getY() + yAdd);

        if (direction.getY() > yMax)
            direction.setY(yMax);

        if (groundBoost)
            if (entity.isOnGround())
                direction.setY(direction.getY() + 0.2);

        entity.setFallDistance(0);
        entity.setVelocity(direction);
    }
}
