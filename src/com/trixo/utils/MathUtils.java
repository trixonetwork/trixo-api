package com.trixo.utils;

public class MathUtils {
    public static int toHash(double value) {
        long bits = Double.doubleToLongBits(value);
        return (int) (bits ^ (bits >>> 32));
    }

    public static int toHash(float value) {
        return Float.floatToIntBits(value);
    }
}
