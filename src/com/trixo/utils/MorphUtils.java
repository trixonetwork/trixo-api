package com.trixo.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.trixo.constants.PlayerConstants.MorphType;
import com.trixo.data.DataContainer;
import com.trixo.server.player.disguisetest.ReflectionUtil;
import com.trixo.server.player.disguisetest.ReflectionUtil.RefClass;
import com.trixo.server.player.disguisetest.ReflectionUtil.RefConstructor;
import com.trixo.server.player.disguisetest.ReflectionUtil.RefField;
import com.trixo.server.player.disguisetest.ReflectionUtil.RefMethod;

public class MorphUtils {
    public static void morphToAll(Entity toMorph, MorphType type) {
        DataContainer<String, Object> data = getEntity("Entity" + type.getEntityName(), toMorph);

        {
            Location location = toMorph.getLocation();

            {
                Object obj = data.get("object");
                RefClass entity = (RefClass) data.get("objectRefClass");

                if(type == MorphType.WITHER_SKELETON) {
                    RefMethod methodSkeleton = entity.findMethodByName("setSkeletonType");
                    methodSkeleton.of(obj).call(1);
                }
                
                RefMethod m = entity.getMethod("setPosition", double.class, double.class, double.class);
                RefMethod mm = entity.getMethod("d", int.class);
                RefMethod mmm = entity.getMethod("setCustomName", String.class);
                RefMethod mmmm = entity.getMethod("setCustomNameVisible", boolean.class);

                m.of(obj).call(location.getX(), location.getY(), location.getZ());
                mm.of(obj).call(toMorph.getEntityId());
                mmm.of(obj).call(ChatColor.YELLOW + toMorph.getName());
                mmmm.of(obj).call(true);

                RefField rf = entity.getField("locX");
                rf.of(obj).set(location.getX());

                RefField rf1 = entity.getField("locY");
                rf1.of(obj).set(location.getY());

                RefField rf2 = entity.getField("locZ");
                rf2.of(obj).set(location.getZ());

                RefField rf3 = entity.getField("yaw");
                rf3.of(obj).set(location.getYaw());

                RefField rf4 = entity.getField("pitch");
                rf4.of(obj).set(location.getPitch());
            }
        }

        RefClass p29 = ReflectionUtil.getRefClass("{nms}.PacketPlayOutEntityDestroy");
        RefClass p20 = ReflectionUtil.getRefClass("{nms}.PacketPlayOutSpawnEntityLiving");

        RefConstructor pp20 = p20.getConstructor(ReflectionUtil.getRefClass("{nms}.EntityLiving"));
        RefConstructor pp29 = p29.getConstructor(int[].class);

        int[] entityId = new int[1];
        entityId[0] = toMorph.getEntityId();

        Object packetEntityDestroy = pp29.create(entityId);
        Object packetNamedEntitySpawn = pp20.create(data.get("object"));

        RefClass classCraftEntity = ReflectionUtil.getRefClass("{cb}.entity.CraftEntity");
        RefMethod methodGetHandle = classCraftEntity.getMethod("getHandle");
        RefClass classEntityPlayer = ReflectionUtil.getRefClass("{nms}.EntityPlayer");
        RefField fieldPlayerConnection = classEntityPlayer.getField("playerConnection");
        RefClass classPlayerConnection = ReflectionUtil.getRefClass("{nms}.PlayerConnection");
        RefMethod methodSendPacket = classPlayerConnection.findMethodByName("sendPacket");

        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p != toMorph) {
                Object handle = methodGetHandle.of(p).call();
                Object connection = fieldPlayerConnection.of(handle).get();

                methodSendPacket.of(connection).call(packetEntityDestroy);
                methodSendPacket.of(connection).call(packetNamedEntitySpawn);
            }
        }
    }

    public static void removeMorph(Entity toMorph) {
        RefClass p29 = ReflectionUtil.getRefClass("{nms}.PacketPlayOutEntityDestroy");
        RefClass p20 = ReflectionUtil.getRefClass("{nms}.PacketPlayOutSpawnEntityLiving");
        
        RefConstructor pp20 = p20.getConstructor(ReflectionUtil.getRefClass("{nms}.EntityLiving"));
        RefConstructor pp29 = p29.getConstructor(int[].class);

        int[] entityId = new int[1];
        entityId[0] = toMorph.getEntityId();

        Object packetEntityDestroy = pp29.create(entityId);
        Object packetNamedEntitySpawn = pp20.create(
                (ReflectionUtil.getRefClass("{cb}.entity.CraftEntity")).getMethod("getHandle").of(toMorph).call());

        RefClass classCraftEntity = ReflectionUtil.getRefClass("{cb}.entity.CraftEntity");
        RefMethod methodGetHandle = classCraftEntity.getMethod("getHandle");
        RefClass classEntityPlayer = ReflectionUtil.getRefClass("{nms}.EntityPlayer");
        RefField fieldPlayerConnection = classEntityPlayer.getField("playerConnection");
        RefClass classPlayerConnection = ReflectionUtil.getRefClass("{nms}.PlayerConnection");
        RefMethod methodSendPacket = classPlayerConnection.findMethodByName("sendPacket");

        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p != toMorph) {
                Object handle = methodGetHandle.of(p).call();
                Object connection = fieldPlayerConnection.of(handle).get();

                methodSendPacket.of(connection).call(packetEntityDestroy);
                methodSendPacket.of(connection).call(packetNamedEntitySpawn);
            }
        }
    }

    private static DataContainer<String, Object> getEntity(String entity, Entity toMorph) {
        RefClass ent = ReflectionUtil.getRefClass("{nms}." + entity);
        RefConstructor entConstructor = ent.getConstructor(ReflectionUtil.getRefClass("{nms}.World"));

        RefClass classCraftWorld = ReflectionUtil.getRefClass("{cb}.CraftWorld");
        RefMethod methodGetHandle = classCraftWorld.getMethod("getHandle");

        Object handle = methodGetHandle.of(toMorph.getWorld()).call();
        Object fin = entConstructor.create(handle);

        DataContainer<String, Object> toReturn = new DataContainer<String, Object>(false);
        toReturn.put("object", fin);
        toReturn.put("objectClass", fin.getClass());
        toReturn.put("objectRefClass", ReflectionUtil.getRefClass(fin.getClass()));

        return toReturn;
    }
}
