package com.trixo.data;

import com.trixo.constants.DataConstants.DataResponseCode;
import com.trixo.interfaces.IDataObject;

public class DataResponse implements IDataObject<String, Object> {
    private String responseMessage = null;
    private int responseCode = -1;

    public DataResponse(int responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    /* DataResponse Functions */
    public String getResponseMessage() {
        return this.responseMessage;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    /* Static Functions */
    public static DataResponse create(DataResponseCode responseCode, String responseMessage) {
        return new DataResponse(responseCode.getCode(), responseMessage);
    }

    /* IDataObject Functions */
    @Override
    public DataContainer<String, Object> asDataContainer() {
        DataContainer<String, Object> newContainer = new DataContainer<String, Object>(false);
        newContainer.put("responseCode", this.responseCode);
        newContainer.put("responeMessage", this.responseMessage);

        return newContainer;
    }
}
