package com.trixo.data;

import com.trixo.interfaces.IDataObject;

public class DataCenter implements IDataObject<DataPointer<?>, Object> {
    private static DataContainer<String, DataContainer<Object, Object>> data = null;

    static {
        data = new DataContainer<String, DataContainer<Object, Object>>(true);
    }

    /** DataCenter Functions **/
    public static <T> T retrieveData(DataPointer<?> pointer, Class<T> clazz) {
        return clazz.cast(data.get(pointer.getChannel()).get(pointer.getAddress()));
    }

    public static <T> T retrieveData(String channel, String address, Class<T> clazz) {
        return clazz.cast(data.get(channel).get(address));
    }

    public static <T> T retrieveData(String address, Class<T> clazz) {
        return clazz.cast(data.get("_global_").get(address));
    }

    public static Object retrieveData(DataPointer<?> pointer) {
        return data.get(pointer.getChannel()).get(pointer.getAddress());
    }

    public static Object retrieveData(String channel, Object pointer) {
        return data.get(channel).get(pointer);
    }

    public static Object retrieveData(String address) {
        return data.get("_global_").get(address);
    }

    @Deprecated
    public static DataPointer<?> putData(Object inData) {
        DataPointer<String> pointer = new DataPointer<String>(inData.toString());
        updateChannel("_global_", pointer, inData);

        return pointer;
    }

    public static <T> DataPointer<T> putData(String channel, T pointer, Object inData) {
        updateChannel(channel, pointer, inData);

        return new DataPointer<T>(pointer);
    }

    public static <T> DataPointer<T> putData(T pointer, Object inData) {
        updateChannel("_global_", pointer, inData);

        return new DataPointer<T>(pointer);
    }

    public static DataPointer<?> putData(DataPointer<?> pointer, Object inData) {
        updateChannel(pointer.getChannel(), pointer.getAddress(), inData);

        return pointer;
    }

    private static void updateChannel(String channel, Object address, Object inData) {
        if (data.containsKey(channel)) {
            DataContainer<Object, Object> existingChannel = data.get(channel);
            existingChannel.put(address, inData);

            data.put(channel, existingChannel);
        } else {
            DataContainer<Object, Object> newChannel = new DataContainer<Object, Object>(true);
            newChannel.put(address, inData);

            data.put(channel, newChannel);
        }
    }
    
    /** IDataObject Functions **/
    @Override
    public DataContainer<DataPointer<?>, Object> asDataContainer() {
        DataContainer<DataPointer<?>, Object> returnData = new DataContainer<DataPointer<?>, Object>(true);

        for (String channel : data.keySet()) {
            for (Object address : data.get(channel).keySet()) {
                returnData.put(DataPointer.create(channel, address), data.get(channel).get(address));
            }
        }

        return returnData;
    }
}
