package com.trixo.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.function.BiConsumer;

public class DataContainer<K, V> {
    private HashMap<K, V> dataCollection = null;

    /** Constructor **/
    public DataContainer(boolean linked) {
        if (linked) {
            this.dataCollection = new LinkedHashMap<K, V>();
        } else {
            this.dataCollection = new HashMap<K, V>();
        }
    }

    /** DataContainer Functions **/
    public void put(K key, V value) {
        this.dataCollection.put(key, value);
    }

    public V get(K key) {
        return this.containsKey(key) ? this.dataCollection.get(key) : null;
    }

    public <T> T get(K key, Class<T> clazz) {
        return clazz.cast(this.get(key));
    }

    public void remove(K key) {
        this.dataCollection.remove(key);
    }

    public boolean containsKey(K key) {
        return this.dataCollection.containsKey(key);
    }

    public boolean containsValue(V value) {
        return this.dataCollection.containsValue(value);
    }

    @SuppressWarnings("rawtypes")
    public Iterator iterator(boolean K) {
        return (K ? this.dataCollection.keySet().iterator() : this.dataCollection.values().iterator());
    }
    
    public void forEach(BiConsumer<? super K, ? super V> action) {
        this.dataCollection.forEach(action);
    }

    public int size() {
        return this.dataCollection.size();
    }

    public void clear() {
        this.dataCollection.clear();
    }

    public Set<K> keySet() {
        return this.dataCollection.keySet();
    }

    public Collection<V> valueSet() {
        return this.dataCollection.values();
    }

    public HashMap<K, V> toHashMap() {
        return this.dataCollection;
    }

    public void flush() {
        this.dataCollection.forEach((key, value) -> {
            System.out.println(key + ", " + value);
        });
    }
}
