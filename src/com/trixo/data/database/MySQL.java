package com.trixo.data.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.trixo.constants.DataConstants.DataResponseCode;
import com.trixo.data.DataContainer;
import com.trixo.data.DataResponse;
import com.trixo.interfaces.IDatabase;

public class MySQL implements IDatabase {
    private boolean driverLoaded = false;
    private Connection connection = null;

    /** Getters **/
    @Override
    public Object getConnection() {
        return this.connection;
    }

    @Override
    public Object getDatabase() {
        return null;
    }

    /** Database Functions **/
    @Override
    public DataResponse loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            return DataResponse.create(DataResponseCode.NO_ERROR, "");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();

            return DataResponse.create(DataResponseCode.FAILLOAD_DATABASE_DRIVER,
                    "Failed to load the database driver.");
        } finally {
            this.driverLoaded = true;
        }
    }

    @Override
    public DataResponse connect(DataContainer<String, Object> connectionData) {
        if (this.driverLoaded) {
            try {
                if (this.connection == null || this.connection.isClosed())
                    this.connection = DriverManager.getConnection((String) connectionData.get("dbUrl"),
                            (String) connectionData.get("dbUser"), (String) connectionData.get("dbPass"));

                return DataResponse.create(DataResponseCode.NO_ERROR, "");
            } catch (SQLException e) {
                e.printStackTrace();

                return DataResponse.create(DataResponseCode.DATABASE_CONNECT_ERROR,
                        "Couldn't connect to the database.");
            }
        } else {
            return DataResponse.create(DataResponseCode.NO_DATABASE_DRIVER, "No database driver has been loaded.");
        }
    }

    @Override
    public DataResponse disconnect() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return DataResponse.create(DataResponseCode.NO_ERROR, "");
    }

    @Override
    public DataResponse dropTable(DataContainer<String, Object> data) {
        return DataResponse.create(DataResponseCode.NO_ERROR, "");
    }

    @Override
    public DataContainer<String, Object> insert(DataContainer<String, Object> data) {
        return null;
    }

    @Override
    public DataContainer<String, Object> upsert(DataContainer<String, Object> data) {
        return null;
    }

    @Override
    public DataContainer<String, Object> update(DataContainer<String, Object> data) {
        return null;
    }

    @Override
    public DataContainer<String, Object> remove(DataContainer<String, Object> data) {
        return null;
    }

    @Override
    public DataContainer<String, Object> select(DataContainer<String, Object> data) {
        return null;
    }
}