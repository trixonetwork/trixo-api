package com.trixo.data.database;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.trixo.constants.DataConstants.DataResponseCode;
import com.trixo.data.DataContainer;
import com.trixo.data.DataResponse;
import com.trixo.interfaces.IDatabase;

@SuppressWarnings("deprecation")
public class MongoDB implements IDatabase {
    private Mongo connection = null;
    private DB database = null;

    /** Getters **/
    @Override
    public Object getConnection() {
        return this.connection;
    }

    @Override
    public Object getDatabase() {
        return this.database;
    }

    /** Database Functions **/
    @Override
    public DataResponse loadDriver() {
        return DataResponse.create(DataResponseCode.NO_ERROR, "");
    }

    @Override
    public DataResponse connect(DataContainer<String, Object> connectionData) {
        try {
            this.connection = new Mongo((String) connectionData.get("host"), (int) connectionData.get("port"));
        } catch (UnknownHostException e) {
            e.printStackTrace();

            return DataResponse.create(DataResponseCode.DATABASE_CONNECT_ERROR, "Cannot connect to the database.");
        }

        this.database = this.connection.getDB((String) connectionData.get("database"));

        if (this.database.authenticate((String) connectionData.get("database"),
                ((String) connectionData.get("password")).toCharArray())) {
            return DataResponse.create(DataResponseCode.NO_ERROR, "");
        } else {
            return DataResponse.create(DataResponseCode.DATABASE_LOGIN_ERROR, "Cannot login to the database.");
        }
    }

    @Override
    public DataResponse disconnect() {
        this.connection.close();

        return DataResponse.create(DataResponseCode.NO_ERROR, "");
    }

    @Override
    public DataResponse dropTable(DataContainer<String, Object> data) {
        DBCollection collection = this.database.getCollection((String) data.get("collection"));
        collection.drop();

        return DataResponse.create(DataResponseCode.NO_ERROR, "");
    }

    @Override
    public DataContainer<String, Object> insert(DataContainer<String, Object> data) {
        DBCollection collection = this.database.getCollection((String) data.get("collection"));

        DataContainer<String, Object> tmp = new DataContainer<String, Object>(false);
        tmp.put("result", collection.insert((BasicDBObject) data.get("data")));
        tmp.put("response", DataResponse.create(DataResponseCode.NO_ERROR, ""));

        return tmp;
    }

    @Override
    public DataContainer<String, Object> upsert(DataContainer<String, Object> data) {
        DBCollection collection = this.database.getCollection((String) data.get("collection"));

        DataContainer<String, Object> tmp = new DataContainer<String, Object>(false);
        tmp.put("result", collection.update((BasicDBObject) data.get("query"), (BasicDBObject) data.get("data"), true,
                (boolean) data.get("multi")));
        tmp.put("response", DataResponse.create(DataResponseCode.NO_ERROR, ""));

        return tmp;
    }

    @Override
    public DataContainer<String, Object> update(DataContainer<String, Object> data) {
        DBCollection collection = this.database.getCollection((String) data.get("collection"));

        DataContainer<String, Object> tmp = new DataContainer<String, Object>(false);
        tmp.put("result", collection.update((BasicDBObject) data.get("query"), (BasicDBObject) data.get("data")));
        tmp.put("response", DataResponse.create(DataResponseCode.NO_ERROR, ""));

        return tmp;
    }

    @Override
    public DataContainer<String, Object> remove(DataContainer<String, Object> data) {
        DBCollection collection = this.database.getCollection((String) data.get("collection"));

        DataContainer<String, Object> tmp = new DataContainer<String, Object>(false);
        tmp.put("result", collection.remove((BasicDBObject) data.get("query")));
        tmp.put("response", DataResponse.create(DataResponseCode.NO_ERROR, ""));

        return tmp;
    }

    @Override
    public DataContainer<String, Object> select(DataContainer<String, Object> data) {
        DBCollection collection = this.database.getCollection((String) data.get("collection"));
        DBCursor cursor = null;

        if ((boolean) data.get("selectAll")) {
            cursor = collection.find();
        } else {
            cursor = collection.find((BasicDBObject) data.get("query"));
        }

        DataContainer<String, Object> tmp = new DataContainer<String, Object>(false);
        tmp.put("result", cursor);
        tmp.put("response", DataResponse.create(DataResponseCode.NO_ERROR, ""));

        return tmp;
    }
}