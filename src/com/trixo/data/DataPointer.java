package com.trixo.data;

@SuppressWarnings("unchecked")
public class DataPointer<T> {
    private T address = null;
    private String channel = null;
    
    /** Constructor **/
    public DataPointer(T address) {
        this.address = address;
        this.channel = "_global_";
    }
    
    public DataPointer(String channel, T address) {
        this.address = address;
        this.channel = channel;
    }

    /** DataPointer Functions **/
    public static <T> DataPointer<T> create(T address) {
        return new DataPointer<T>(address);
    }
    
    public static <T> DataPointer<T> create(String channel, T address) {
        return new DataPointer<T>(channel, address);
    }

    /** Getters **/
    /* Getters */
    public T getAddress(Class<T> clazz) {
        return clazz.cast(this.address);
    }

    public T getAddress() {
        return this.address;
    }
    
    public String getChannel() {
        return this.channel;
    }

    /** Object Functions **/
    @Override
    public boolean equals(Object other) {
        return ((DataPointer<T>) other).getAddress() == this.getAddress() && ((DataPointer<T>) other).getChannel() == this.getChannel();
    }
    
    @Override
    public String toString() {
        return this.channel + ":" + this.address;
    }
}
