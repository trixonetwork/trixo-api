package com.trixo;

import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.trixo.data.DataCenter;
import com.trixo.io.LanguageLocalizer;
import com.trixo.network.PMCManager;
import com.trixo.network.ServerManager;
import com.trixo.server.chat.ChatManager;
import com.trixo.server.event.ITrixoEvent;
import com.trixo.server.player.disguisetest.TrixoPacketReceiver;

public class TrixoAPI extends JavaPlugin implements ITrixoEvent {
    /** JavaPlugin Functions **/
    @Override
    public void onEnable() {
        /** Adding Default Data **/
        /* Global Data */
        DataCenter.putData("api", "plugin", this);
        DataCenter.putData("api", "pluginClassLoader", this.getClassLoader());
        DataCenter.putData("api", "serverManager", new ServerManager());
        DataCenter.putData("api", "chatManager", new ChatManager());

        new TrixoPacketReceiver(this);
        new LanguageLocalizer().loadLanguages("trixo/data/languages/");

        /* Plugin Messaging Channels */
        {
            PMCManager pmcManager = new PMCManager();
            // pmcManager.registerIPC("BungeeCord", IPCListener.class);
            pmcManager.registerOPC("BungeeCord");

            DataCenter.putData("api", "pmcManager", pmcManager);
        }

        /** Register Events **/
        this.register(this);
    }

    @Override
    public void onDisable() {
        this.unregister();
    }

    /** Events **/
    /* Register */
    @Override
    public void register(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        HandlerList.unregisterAll((Listener) this);
    }

    /* Events */
    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().equalsIgnoreCase("/restart")) {
            event.getPlayer().sendMessage("You cannot use the restart command, use /stop instead.");

            event.setCancelled(true);
        }
    }
}