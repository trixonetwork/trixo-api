package com.trixo.math.colour;

import com.trixo.constants.ColourConstants.BlendMode;
import com.trixo.data.DataContainer;
import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.MathObject;
import com.trixo.math.vector.Vector;

@SuppressWarnings("unchecked")
public class Colour<T> extends MathObject<T> {
    /** Colour Functions **/
    /* Math: Colour Distance */
    public double distance(Colour<?> other) {
        if (this.isMutable()) {
            ColourRGB<IMutable> otherColour = (ColourRGB<IMutable>) other;

            return this.distance(new double[] {
                    otherColour.getR(), otherColour.getG(), otherColour.getB()
            });
        } else {
            ColourRGB<IImmutable> otherColour = (ColourRGB<IImmutable>) other;

            return this.distance(new double[] {
                    otherColour.getR(), otherColour.getG(), otherColour.getB()
            });
        }
    }

    public double distance(double[] colours) {
        return 0.0;
    }

    /* Math: Utils */
    public Colour<?> blendColour(Colour<?> other, BlendMode mode, DataContainer<String, Object> data) {
        return null;
    }

    public Colour<?> makeLighter(double fraction) {
        return null;
    }

    public Colour<?> makeDarker(double fraction) {
        return null;
    }

    public Colour<?> convertColour() {
        return null;
    }

    /** Utility Functions **/
    public void set(Vector<?> colour) {

    }

    public void set(Colour<?> other) {

    }

    public Colour<?> fromInt(int colour) {
        return null;
    }

    public int toInt() {
        return 0;
    }

    public Vector<?> toVector() {
        return null;
    }

    public boolean isDarkColour() {
        return false;
    }

    public boolean isByteColour() {
        return false;
    }
}
