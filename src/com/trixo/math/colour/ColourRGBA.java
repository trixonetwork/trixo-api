package com.trixo.math.colour;

import com.trixo.constants.ColourConstants.BlendMode;
import com.trixo.data.DataContainer;
import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector4;

@SuppressWarnings("unchecked")
public class ColourRGBA<T> extends Colour<T> {
    private double r = 0, g = 0, b = 0, a = 0;

    /** Constructors **/
    public ColourRGBA() {
        this(1.0, 1.0, 1.0, 1.0);
    }

    public ColourRGBA(double N) {
        this(N, N, N, N);
    }

    public ColourRGBA(double N, double A) {
        this(N, N, N, A);
    }

    public ColourRGBA(Vector4<?> colour) {
        this(colour.getX(), colour.getY(), colour.getZ(), colour.getW());
    }

    public ColourRGBA(double R, double G, double B, double A) {
        this.r = R;
        this.g = G;
        this.b = B;
        this.a = A;
    }

    /** Getters and Setters **/
    /* Getters */
    @Override
    public void set(Vector<?> colour) {
        if (this.isMutable()) {
            Vector4<IMutable> otherColour = (Vector4<IMutable>) colour;

            this.r = otherColour.getX();
            this.g = otherColour.getY();
            this.b = otherColour.getZ();
            this.a = otherColour.getW();
        } else {
            Vector4<IImmutable> otherColour = (Vector4<IImmutable>) colour;

            this.r = otherColour.getX();
            this.g = otherColour.getY();
            this.b = otherColour.getZ();
            this.a = otherColour.getW();
        }
    }

    @Override
    public void set(Colour<?> other) {
        if (this.isMutable()) {
            ColourRGBA<IMutable> otherColour = (ColourRGBA<IMutable>) other;

            this.r = otherColour.r;
            this.g = otherColour.g;
            this.b = otherColour.b;
            this.a = otherColour.a;
        } else {
            ColourRGBA<IImmutable> otherColour = (ColourRGBA<IImmutable>) other;

            this.r = otherColour.r;
            this.g = otherColour.g;
            this.b = otherColour.b;
            this.a = otherColour.a;
        }
    }

    public void setR(double n) {
        this.r = n;
    }

    public void setG(double n) {
        this.g = n;
    }

    public void setB(double n) {
        this.b = n;
    }

    public void setA(double n) {
        this.a = n;
    }

    /* Getters */
    public double getR() {
        return this.r;
    }

    public double getG() {
        return this.g;
    }

    public double getB() {
        return this.b;
    }

    public double getA() {
        return this.a;
    }

    /** Math Functions **/
    /* Math: Colour Distance */
    @Override
    public double distance(double[] colours) {
        double R = colours[0] - this.r;
        double G = colours[1] - this.g;
        double B = colours[2] - this.b;

        return Math.sqrt((R * R) + (G * G) + (B * B));
    }

    /* Math: Blend Colour */
    @Override
    public ColourRGBA<?> blendColour(Colour<?> other, BlendMode mode, DataContainer<String, Object> data) {
        if (mode == BlendMode.ADDITIVE_RATIO) {
            double ratio = (double) data.get("ratio");
            double inversedRatio = (1.0 - ratio);

            if (this.isMutable()) {
                ColourRGBA<IMutable> otherColour = (ColourRGBA<IMutable>) other;

                this.r = (this.r * ratio) + (otherColour.r * inversedRatio);
                this.g = (this.g * ratio) + (otherColour.g * inversedRatio);
                this.b = (this.b * ratio) + (otherColour.b * inversedRatio);

                return this;
            } else {
                ColourRGBA<IImmutable> otherColour = (ColourRGBA<IImmutable>) other;

                return new ColourRGBA<IImmutable>((this.r * ratio) + (otherColour.r * inversedRatio),
                        (this.g * ratio) + (otherColour.g * inversedRatio),
                        (this.b * ratio) + (otherColour.b * inversedRatio), this.a) {};
            }
        } else if (mode == BlendMode.ADDITIVE) {
            if (this.isMutable()) {
                ColourRGBA<IMutable> otherColour = (ColourRGBA<IMutable>) other;

                this.r = ((this.r + otherColour.r) / 2.0);
                this.g = ((this.g + otherColour.g) / 2.0);
                this.b = ((this.b + otherColour.b) / 2.0);

                return this;
            } else {
                ColourRGBA<IImmutable> otherColour = (ColourRGBA<IImmutable>) other;

                return new ColourRGBA<IImmutable>(((this.r + otherColour.r) / 2.0), ((this.g + otherColour.g) / 2.0),
                        ((this.b + otherColour.b) / 2.0), this.a) {};
            }
        } else if (mode == BlendMode.SUBTRACTIVE) {
            if (this.isMutable()) {
                ColourRGBA<IMutable> otherColour = (ColourRGBA<IMutable>) other;

                this.r = 255.0 - Math.sqrt((Math.pow((255 - this.r), 2) + Math.pow((255 - otherColour.r), 2)) / 2.0);
                this.g = 255.0 - Math.sqrt((Math.pow((255 - this.g), 2) + Math.pow((255 - otherColour.g), 2)) / 2.0);
                this.b = 255.0 - Math.sqrt((Math.pow((255 - this.b), 2) + Math.pow((255 - otherColour.b), 2)) / 2.0);

                return this;
            } else {
                ColourRGBA<IImmutable> otherColour = (ColourRGBA<IImmutable>) other;

                double R = 255.0 - Math.sqrt((Math.pow((255 - this.r), 2) + Math.pow((255 - otherColour.r), 2)) / 2.0);
                double G = 255.0 - Math.sqrt((Math.pow((255 - this.g), 2) + Math.pow((255 - otherColour.g), 2)) / 2.0);
                double B = 255.0 - Math.sqrt((Math.pow((255 - this.b), 2) + Math.pow((255 - otherColour.b), 2)) / 2.0);

                return new ColourRGBA<IImmutable>(R, G, B, this.a) {};
            }
        } else if (mode == BlendMode.SUBTRACTIVE_RATIO) {
            double ratio = (double) data.get("ratio");
            double inversedRatio = (1.0 - ratio);

            if (this.isMutable()) {
                ColourRGBA<IMutable> otherColour = (ColourRGBA<IMutable>) other;

                this.r = ((255.0 - Math.sqrt((Math.pow((255 - this.r), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.r), 2)) / 2.0)) * inversedRatio);
                this.g = ((255.0 - Math.sqrt((Math.pow((255 - this.g), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.g), 2)) / 2.0)) * inversedRatio);
                this.b = ((255.0 - Math.sqrt((Math.pow((255 - this.b), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.b), 2)) / 2.0)) * inversedRatio);

                return this;
            } else {
                ColourRGBA<IImmutable> otherColour = (ColourRGBA<IImmutable>) other;

                double R = ((255.0 - Math.sqrt((Math.pow((255 - this.r), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.r), 2)) / 2.0)) * inversedRatio);
                double G = ((255.0 - Math.sqrt((Math.pow((255 - this.g), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.g), 2)) / 2.0)) * inversedRatio);
                double B = ((255.0 - Math.sqrt((Math.pow((255 - this.b), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.b), 2)) / 2.0)) * inversedRatio);

                return new ColourRGBA<IImmutable>(R, G, B, this.a) {};
            }
        }

        return this;
    }

    /* Math: Make Lighter */
    @Override
    public ColourRGBA<?> makeLighter(double fraction) {
        double R = this.r * (1.0 + fraction);
        double G = this.g * (1.0 + fraction);
        double B = this.b * (1.0 + fraction);

        if (this.isByteColour()) {
            if (R < 0)
                R = 0;
            else if (R > 255)
                R = 255;
            if (G < 0)
                G = 0;
            else if (G > 255)
                G = 255;
            if (B < 0)
                B = 0;
            else if (B > 255)
                B = 255;
        } else {
            if (R < 0)
                R = 0;
            else if (R > 1.0)
                R = 1.0;
            if (G < 0)
                G = 0;
            else if (G > 1.0)
                G = 1.0;
            if (B < 0)
                B = 0;
            else if (B > 1.0)
                B = 1.0;
        }

        if (this.isMutable()) {
            this.r = R;
            this.g = G;
            this.b = B;

            return this;
        } else {
            return new ColourRGBA<IImmutable>(R, G, B, this.a) {};
        }
    }

    /* Math: Make Darker */
    @Override
    public ColourRGBA<?> makeDarker(double fraction) {
        double R = this.r * (1.0 - fraction);
        double G = this.g * (1.0 - fraction);
        double B = this.b * (1.0 - fraction);

        if (this.isByteColour()) {
            if (R < 0)
                R = 0;
            else if (R > 255)
                R = 255;
            if (G < 0)
                G = 0;
            else if (G > 255)
                G = 255;
            if (B < 0)
                B = 0;
            else if (B > 255)
                B = 255;
        } else {
            if (R < 0)
                R = 0;
            else if (R > 1.0)
                R = 1.0;
            if (G < 0)
                G = 0;
            else if (G > 1.0)
                G = 1.0;
            if (B < 0)
                B = 0;
            else if (B > 1.0)
                B = 1.0;
        }

        if (this.isMutable()) {
            this.r = R;
            this.g = G;
            this.b = B;

            return this;
        } else {
            return new ColourRGBA<IImmutable>(R, G, B, this.a) {};
        }
    }

    /* Math: Convert between double and byte colour */
    @Override
    public ColourRGBA<?> convertColour() {
        if (this.isMutable()) {
            if (this.isByteColour()) {
                this.set(this.toVector().divide(255));
            } else {
                this.set(this.toVector().mult(255));
            }

            return this;
        } else {
            if (this.isByteColour()) {
                Vector4<IImmutable> newVector = (Vector4<IImmutable>) this.toVector();

                return new ColourRGBA<IImmutable>(newVector.divide(255)) {};
            } else {
                Vector4<IImmutable> newVector = (Vector4<IImmutable>) this.toVector();

                return new ColourRGBA<IImmutable>(newVector.mult(255)) {};
            }
        }
    }

    /** Utility Functions **/
    @Override
    public boolean isDarkColour() {
        double whiteDistance = this.distance(new ColourRGBA<IImmutable>(1.0, 1.0, 1.0, 1.0) {});
        double blackDistance = this.distance(new ColourRGBA<IImmutable>(0.0, 0.0, 0.0, 0.0) {});

        return blackDistance < whiteDistance;
    }

    @Override
    public boolean isByteColour() {
        return (this.r > 1.0 || this.r > 1.0 || this.g > 1.0 || this.a > 1.0);
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.r == ((ColourRGBA<?>) obj).r && this.g == ((ColourRGBA<?>) obj).g && this.b == ((ColourRGBA<?>) obj).b
                && this.a == ((ColourRGBA<?>) obj).a)
            return true;

        return false;
    }

    @Override
    public ColourRGBA<?> fromInt(int colour) {
        int r = (int) (((byte) (colour >> 16) & 0xFF));
        int g = (int) (((byte) (colour >> 8) & 0xFF));
        int b = (int) (((byte) (colour >> 0) & 0xFF));
        int a = (int) (((byte) (colour >> 24) & 0xFF));

        return new ColourRGBA<IImmutable>(r, g, b, a) {};
    }

    @Override
    public int toInt() {
        if (this.isByteColour()) {
            int rgba = (((int) (this.r) & 0xFF) << 24) | (((int) (this.g) & 0xFF) << 16)
                    | (((int) (this.b) & 0xFF) << 8) | (((int) (this.a) & 0xFF));

            return rgba;
        } else {
            int rgba = (((int) (this.r * 255) & 0xFF) << 24) | (((int) (this.g * 255) & 0xFF) << 16)
                    | (((int) (this.b * 255) & 0xFF) << 8) | (((int) (this.a * 255) & 0xFF));

            return rgba;
        }
    }

    @Override
    public Vector4<?> toVector() {
        if (this.isMutable()) {
            return new Vector4<IMutable>(this.r, this.g, this.b, this.a) {};
        } else {
            return new Vector4<IImmutable>(this.r, this.g, this.b, this.a) {};
        }
    }

    @Override
    public String toString() {
        return String.format("[%s, %s, %s, %s]", this.r, this.g, this.b, this.a);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.r, this.g, this.b, this.a
        };
    }
}