package com.trixo.math.colour;

import com.trixo.constants.ColourConstants.BlendMode;
import com.trixo.data.DataContainer;
import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector3;

@SuppressWarnings("unchecked")
public class ColourRGB<T> extends Colour<T> {
    private double r = 0, g = 0, b = 0;

    /** Constructors **/
    public ColourRGB() {
        this(1.0, 1.0, 1.0);
    }

    public ColourRGB(double N) {
        this(N, N, N);
    }

    public ColourRGB(Vector3<?> colour) {
        this(colour.getX(), colour.getY(), colour.getZ());
    }

    public ColourRGB(double R, double G, double B) {
        this.r = R;
        this.g = G;
        this.b = B;
    }

    /** Setters and Getters **/
    /* Setters */
    @Override
    public void set(Vector<?> colour) {
        if (this.isMutable()) {
            Vector3<IMutable> otherColour = (Vector3<IMutable>) colour;

            this.r = otherColour.getX();
            this.g = otherColour.getY();
            this.b = otherColour.getZ();
        } else {
            Vector3<IImmutable> otherColour = (Vector3<IImmutable>) colour;

            this.r = otherColour.getX();
            this.g = otherColour.getY();
            this.b = otherColour.getZ();
        }
    }

    @Override
    public void set(Colour<?> other) {
        if (this.isMutable()) {
            ColourRGB<IMutable> otherColour = (ColourRGB<IMutable>) other;

            this.r = otherColour.r;
            this.g = otherColour.g;
            this.b = otherColour.b;
        } else {
            ColourRGB<IImmutable> otherColour = (ColourRGB<IImmutable>) other;

            this.r = otherColour.r;
            this.g = otherColour.g;
            this.b = otherColour.b;
        }
    }

    public void setR(double n) {
        this.r = n;
    }

    public void setG(double n) {
        this.g = n;
    }

    public void setB(double n) {
        this.b = n;
    }

    /* Getters */
    public double getR() {
        return this.r;
    }

    public double getG() {
        return this.g;
    }

    public double getB() {
        return this.b;
    }

    /** Math Functions **/
    /* Math: Colour Distance */
    @Override
    public double distance(double[] colours) {
        double R = colours[0] - this.r;
        double G = colours[1] - this.g;
        double B = colours[2] - this.b;

        return Math.sqrt((R * R) + (G * G) + (B * B));
    }

    /* Math: Blend Colour */
    @Override
    public ColourRGB<?> blendColour(Colour<?> other, BlendMode mode, DataContainer<String, Object> data) {
        if (mode == BlendMode.ADDITIVE_RATIO) {
            double ratio = (double) data.get("ratio");
            double inversedRatio = (1.0 - ratio);

            if (this.isMutable()) {
                ColourRGB<IMutable> otherColour = (ColourRGB<IMutable>) other;

                this.r = (this.r * ratio) + (otherColour.r * inversedRatio);
                this.g = (this.g * ratio) + (otherColour.g * inversedRatio);
                this.b = (this.b * ratio) + (otherColour.b * inversedRatio);

                return this;
            } else {
                ColourRGB<IImmutable> otherColour = (ColourRGB<IImmutable>) other;

                return new ColourRGB<IImmutable>((this.r * ratio) + (otherColour.r * inversedRatio),
                        (this.g * ratio) + (otherColour.g * inversedRatio),
                        (this.b * ratio) + (otherColour.b * inversedRatio)) {};
            }
        } else if (mode == BlendMode.ADDITIVE) {
            if (this.isMutable()) {
                ColourRGB<IMutable> otherColour = (ColourRGB<IMutable>) other;

                this.r = ((this.r + otherColour.r) / 2.0);
                this.g = ((this.g + otherColour.g) / 2.0);
                this.b = ((this.b + otherColour.b) / 2.0);

                return this;
            } else {
                ColourRGB<IImmutable> otherColour = (ColourRGB<IImmutable>) other;

                return new ColourRGB<IImmutable>(((this.r + otherColour.r) / 2.0), ((this.g + otherColour.g) / 2.0),
                        ((this.b + otherColour.b) / 2.0)) {};
            }
        } else if (mode == BlendMode.SUBTRACTIVE) {
            if (this.isMutable()) {
                ColourRGB<IMutable> otherColour = (ColourRGB<IMutable>) other;

                this.r = 255.0 - Math.sqrt((Math.pow((255 - this.r), 2) + Math.pow((255 - otherColour.r), 2)) / 2.0);
                this.g = 255.0 - Math.sqrt((Math.pow((255 - this.g), 2) + Math.pow((255 - otherColour.g), 2)) / 2.0);
                this.b = 255.0 - Math.sqrt((Math.pow((255 - this.b), 2) + Math.pow((255 - otherColour.b), 2)) / 2.0);

                return this;
            } else {
                ColourRGB<IImmutable> otherColour = (ColourRGB<IImmutable>) other;

                double R = 255.0 - Math.sqrt((Math.pow((255 - this.r), 2) + Math.pow((255 - otherColour.r), 2)) / 2.0);
                double G = 255.0 - Math.sqrt((Math.pow((255 - this.g), 2) + Math.pow((255 - otherColour.g), 2)) / 2.0);
                double B = 255.0 - Math.sqrt((Math.pow((255 - this.b), 2) + Math.pow((255 - otherColour.b), 2)) / 2.0);

                return new ColourRGB<IImmutable>(R, G, B) {};
            }
        } else if (mode == BlendMode.SUBTRACTIVE_RATIO) {
            double ratio = (double) data.get("ratio");
            double inversedRatio = (1.0 - ratio);

            if (this.isMutable()) {
                ColourRGB<IMutable> otherColour = (ColourRGB<IMutable>) other;

                this.r = ((255.0 - Math.sqrt((Math.pow((255 - this.r), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.r), 2)) / 2.0)) * inversedRatio);
                this.g = ((255.0 - Math.sqrt((Math.pow((255 - this.g), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.g), 2)) / 2.0)) * inversedRatio);
                this.b = ((255.0 - Math.sqrt((Math.pow((255 - this.b), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.b), 2)) / 2.0)) * inversedRatio);

                return this;
            } else {
                ColourRGB<IImmutable> otherColour = (ColourRGB<IImmutable>) other;

                double R = ((255.0 - Math.sqrt((Math.pow((255 - this.r), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.r), 2)) / 2.0)) * inversedRatio);
                double G = ((255.0 - Math.sqrt((Math.pow((255 - this.g), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.g), 2)) / 2.0)) * inversedRatio);
                double B = ((255.0 - Math.sqrt((Math.pow((255 - this.b), 2)) * ratio)
                        + ((Math.pow((255 - otherColour.b), 2)) / 2.0)) * inversedRatio);

                return new ColourRGB<IImmutable>(R, G, B) {};
            }
        }

        return this;
    }

    /* Math: Make Lighter */
    @Override
    public ColourRGB<?> makeLighter(double fraction) {
        double R = this.r * (1.0 + fraction);
        double G = this.g * (1.0 + fraction);
        double B = this.b * (1.0 + fraction);

        if (this.isByteColour()) {
            if (R < 0)
                R = 0;
            else if (R > 255)
                R = 255;
            if (G < 0)
                G = 0;
            else if (G > 255)
                G = 255;
            if (B < 0)
                B = 0;
            else if (B > 255)
                B = 255;
        } else {
            if (R < 0)
                R = 0;
            else if (R > 1.0)
                R = 1.0;
            if (G < 0)
                G = 0;
            else if (G > 1.0)
                G = 1.0;
            if (B < 0)
                B = 0;
            else if (B > 1.0)
                B = 1.0;
        }

        if (this.isMutable()) {
            this.r = R;
            this.g = G;
            this.b = B;

            return this;
        } else {
            return new ColourRGB<IImmutable>(R, G, B) {};
        }
    }

    /* Math: Make Darker */
    @Override
    public ColourRGB<?> makeDarker(double fraction) {
        double R = this.r * (1.0 - fraction);
        double G = this.g * (1.0 - fraction);
        double B = this.b * (1.0 - fraction);

        if (this.isByteColour()) {
            if (R < 0)
                R = 0;
            else if (R > 255)
                R = 255;
            if (G < 0)
                G = 0;
            else if (G > 255)
                G = 255;
            if (B < 0)
                B = 0;
            else if (B > 255)
                B = 255;
        } else {
            if (R < 0)
                R = 0;
            else if (R > 1.0)
                R = 1.0;
            if (G < 0)
                G = 0;
            else if (G > 1.0)
                G = 1.0;
            if (B < 0)
                B = 0;
            else if (B > 1.0)
                B = 1.0;
        }

        if (this.isMutable()) {
            this.r = R;
            this.g = G;
            this.b = B;

            return this;
        } else {
            return new ColourRGB<IImmutable>(R, G, B) {};
        }
    }

    /* Math: Convert between double and byte colour */
    @Override
    public ColourRGB<?> convertColour() {
        if (this.isMutable()) {
            if (this.isByteColour()) {
                this.set(this.toVector().divide(255.0));
            } else {
                this.set(this.toVector().mult(255.0));
            }

            return this;
        } else {
            if (this.isByteColour()) {
                Vector3<IImmutable> newVector = (Vector3<IImmutable>) this.toVector();

                return new ColourRGB<IImmutable>(newVector.divide(255.0)) {};
            } else {
                Vector3<IImmutable> newVector = (Vector3<IImmutable>) this.toVector();

                return new ColourRGB<IImmutable>(newVector.mult(255.0)) {};
            }
        }
    }

    /** Utility Functions **/
    @Override
    public boolean isDarkColour() {
        double whiteDistance = this.distance(new ColourRGB<IImmutable>(1.0, 1.0, 1.0) {});
        double blackDistance = this.distance(new ColourRGB<IImmutable>(0.0, 0.0, 0.0) {});

        return blackDistance < whiteDistance;
    }

    @Override
    public boolean isByteColour() {
        return (this.r > 1.0 || this.r > 1.0 || this.g > 1.0);
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.r == ((ColourRGB<?>) obj).r && this.g == ((ColourRGB<?>) obj).g && this.b == ((ColourRGB<?>) obj).b)
            return true;

        return false;
    }

    @Override
    public ColourRGB<?> fromInt(int colour) {
        int r = (int) (((byte) (colour >> 16) & 0xFF));
        int g = (int) (((byte) (colour >> 8) & 0xFF));
        int b = (int) (((byte) (colour >> 0) & 0xFF));

        return new ColourRGB<IImmutable>(r, g, b) {};
    }

    @Override
    public int toInt() {
        if (this.isByteColour()) {
            int rgb = (int) this.r;
            rgb = (rgb << 8) + (int) this.g;
            rgb = (rgb << 8) + (int) this.b;

            return rgb;
        } else {
            int rgb = (int) (this.r * 255);
            rgb = (rgb << 8) + (int) (this.g * 255);
            rgb = (rgb << 8) + (int) (this.b * 255);

            return rgb;
        }
    }

    @Override
    public Vector3<?> toVector() {
        if (this.isMutable()) {
            return new Vector3<IMutable>(this.r, this.g, this.b) {};
        } else {
            return new Vector3<IImmutable>(this.r, this.g, this.b) {};
        }
    }

    @Override
    public String toString() {
        return String.format("[%s, %s, %s]", this.r, this.g, this.b);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.r, this.g, this.b
        };
    }
}
