package com.trixo.math.vector;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.Transformation;

@SuppressWarnings("unchecked")
public class Vector2<T> extends Vector<T> {
    private double x = 0, y = 0;

    /* Constructors */
    public Vector2() {
        this(0);
    }

    public Vector2(double N) {
        this(N, N);
    }

    public Vector2(Vector2<?> other) {
        this(other.x, other.y);
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /** Setters and Getters **/
    /* Setters */
    public Vector2<T> set(double N) {
        return this.set(N, N);
    }

    public Vector2<T> set(Vector2<?> other) {
        return this.set(other.x, other.y);
    }

    public Vector2<T> set(double x, double y) {
        if (this.isMutable()) {
            this.x = x;
            this.y = y;

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(this) {};
            tmpVector.x = x;
            tmpVector.y = y;

            return (Vector2<T>) tmpVector;
        }
    }

    public void setX(double n) {
        this.set(n, this.y);
    }

    public void setY(double n) {
        this.set(this.x, n);
    }

    /* Getters */
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    /** Math Functions **/
    /* Math: Add */
    public Vector2<T> add(double N) {
        return this.add(N, N);
    }

    public Vector2<T> add(Vector2<?> other) {
        return this.add(other.x, other.y);
    }

    public Vector2<T> add(double x, double y) {
        if (this.isMutable()) {
            this.x += x;
            this.y += y;

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(this) {};
            tmpVector.x += x;
            tmpVector.y += y;

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Subtract */
    public Vector2<T> sub(double N) {
        return this.sub(N, N);
    }

    public Vector2<T> sub(Vector2<?> other) {
        return this.sub(other.x, other.y);
    }

    public Vector2<T> sub(double x, double y) {
        if (this.isMutable()) {
            this.x -= x;
            this.y -= y;

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(this) {};
            tmpVector.x -= x;
            tmpVector.y -= y;

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Multiply */
    public Vector2<T> mult(double N) {
        return this.mult(N, N);
    }

    public Vector2<T> mult(Vector2<?> other) {
        return this.mult(other.x, other.y);
    }

    public Vector2<T> mult(double x, double y) {
        if (this.isMutable()) {
            this.x *= x;
            this.y *= y;

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(this) {};
            tmpVector.x *= x;
            tmpVector.y *= y;

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Divide */
    public Vector2<T> divide(double N) {
        return this.divide(N, N);
    }

    public Vector2<T> divide(Vector2<?> other) {
        return this.divide(other.x, other.y);
    }

    public Vector2<T> divide(double x, double y) {
        if (this.isMutable()) {
            this.x /= x;
            this.y /= y;

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(this) {};
            tmpVector.x /= x;
            tmpVector.y /= y;

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Power */
    public Vector2<T> pow(double N) {
        return this.pow(N, N);
    }

    public Vector2<T> pow(Vector2<?> other) {
        return this.pow(other.x, other.y);
    }

    public Vector2<T> pow(double x, double y) {
        if (this.isMutable()) {
            this.x = Math.pow(this.x, x);
            this.y = Math.pow(this.y, y);

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>() {};
            tmpVector.x = Math.pow(tmpVector.x, x);
            tmpVector.y = Math.pow(tmpVector.y, y);

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Square Root */
    public Vector2<T> sqrt() {
        if (this.isMutable()) {
            this.x = Math.sqrt(this.x);
            this.y = Math.sqrt(this.y);

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>() {};
            tmpVector.x = Math.sqrt(this.x);
            tmpVector.y = Math.sqrt(this.y);

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Cube Root */
    public Vector2<T> cbrt() {
        if (this.isMutable()) {
            this.x = Math.cbrt(this.x);
            this.y = Math.cbrt(this.y);

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>() {};
            tmpVector.x = Math.cbrt(this.x);
            tmpVector.y = Math.cbrt(this.y);

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Normalize */
    public Vector2<T> normalize() {
        if (this.isMutable()) {
            this.set(this.divide(this.length()));

            return this;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(this) {};
            tmpVector.set(tmpVector.divide(this.length()));

            return (Vector2<T>) tmpVector;
        }
    }

    /* Math: Dot */
    public double dot(Vector2<?> other) {
        return this.x * other.x + this.y * other.y;
    }

    /* Math: Utilities */
    public double length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    /** Vector Functions **/
    @Override
    public Vector2<?> applyTransformation(Transformation transformation) {
        if (this.isMutable()) {
            this.set(transformation.toV2(this));

            return this;
        } else {
            return this.set(transformation.toV2(this));
        }
    }

    /** Conversion **/
    public Vector4<?> toV4(int offset) {
        if (this.isMutable()) {
            return (Vector4<T>) ((offset == 0) ? new Vector4<IMutable>(this.x, this.y, 0, 0) {}
                    : (offset == 1) ? new Vector4<IMutable>(this.y, this.x, 0, 0) {} : this.toV4(0));
        } else {
            return (Vector4<T>) ((offset == 0) ? new Vector4<IImmutable>(this.x, this.y, 0, 0) {}
                    : (offset == 1) ? new Vector4<IImmutable>(this.y, this.x, 0, 0) {} : this.toV4(0));
        }
    }

    public Vector3<?> toV3(int offset) {
        if (this.isMutable()) {
            return (Vector3<T>) ((offset == 0) ? new Vector3<IMutable>(this.x, this.y, 0) {}
                    : (offset == 1) ? new Vector3<IMutable>(this.y, this.x, 0) {} : this.toV3(0));
        } else {
            return (Vector3<T>) ((offset == 0) ? new Vector3<IImmutable>(this.x, this.y, 0) {}
                    : (offset == 1) ? new Vector3<IImmutable>(this.y, this.x, 0) {} : this.toV3(0));
        }
    }

    /** MathObject Functions **/
    @Override
    public String toString() {
        return String.format("[%s, %s]", this.x, this.y);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.x, this.y
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.x == ((Vector2<?>) obj).x && this.y == ((Vector2<?>) obj).y)
            return true;

        return false;
    }
}