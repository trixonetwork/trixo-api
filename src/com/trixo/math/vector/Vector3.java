package com.trixo.math.vector;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.Transformation;

@SuppressWarnings("unchecked")
public class Vector3<T> extends Vector<T> {
    private double x = 0, y = 0, z = 0;

    /* Constructors */
    public Vector3() {
        this(0);
    }

    public Vector3(double N) {
        this(N, N, N);
    }

    public Vector3(Vector3<?> other) {
        this(other.x, other.y, other.z);
    }

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /** Setters and Getters **/
    /* Setters */
    public Vector3<T> set(double N) {
        return this.set(N, N, N);
    }

    public Vector3<T> set(Vector3<?> other) {
        return this.set(other.x, other.y, other.z);
    }

    public Vector3<T> set(double x, double y, double z) {
        if (this.isMutable()) {
            this.x = x;
            this.y = y;
            this.z = z;

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(this) {};
            tmpVector.x = x;
            tmpVector.y = y;
            tmpVector.z = z;

            return (Vector3<T>) tmpVector;
        }
    }

    public void setX(double n) {
        this.set(n, this.y, this.z);
    }

    public void setY(double n) {
        this.set(this.x, n, this.z);
    }

    public void setZ(double n) {
        this.set(this.x, this.y, n);
    }

    /* Getters */
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    /** Math Functions **/
    /* Math: Add */
    public Vector3<T> add(double N) {
        return this.add(N, N, N);
    }

    public Vector3<T> add(Vector3<?> other) {
        return this.add(other.x, other.y, other.z);
    }

    public Vector3<T> add(double x, double y, double z) {
        if (this.isMutable()) {
            this.x += x;
            this.y += y;
            this.z += z;

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(this) {};
            tmpVector.x += x;
            tmpVector.y += y;
            tmpVector.z += z;

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Subtract */
    public Vector3<T> sub(double N) {
        return this.sub(N, N, N);
    }

    public Vector3<T> sub(Vector3<?> other) {
        return this.sub(other.x, other.y, other.z);
    }

    public Vector3<T> sub(double x, double y, double z) {
        if (this.isMutable()) {
            this.x -= x;
            this.y -= y;
            this.z -= z;

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(this) {};
            tmpVector.x -= x;
            tmpVector.y -= y;
            tmpVector.z -= z;

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Multiply */
    public Vector3<T> mult(double N) {
        return this.mult(N, N, N);
    }

    public Vector3<T> mult(Vector3<?> other) {
        return this.mult(other.x, other.y, other.z);
    }

    public Vector3<T> mult(double x, double y, double z) {
        if (this.isMutable()) {
            this.x *= x;
            this.y *= y;
            this.z *= z;

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(this) {};
            tmpVector.x *= x;
            tmpVector.y *= y;
            tmpVector.z *= z;

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Divide */
    public Vector3<T> divide(double N) {
        return this.divide(N, N, N);
    }

    public Vector3<T> divide(Vector3<?> other) {
        return this.divide(other.x, other.y, other.z);
    }

    public Vector3<T> divide(double x, double y, double z) {
        if (this.isMutable()) {
            this.x /= x;
            this.y /= y;
            this.z /= z;

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(this) {};
            tmpVector.x /= x;
            tmpVector.y /= y;
            tmpVector.z /= z;

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Power */
    public Vector3<T> pow(double N) {
        return this.pow(N, N, N);
    }

    public Vector3<T> pow(Vector3<?> other) {
        return this.pow(other.x, other.y, other.z);
    }

    public Vector3<T> pow(double x, double y, double z) {
        if (this.isMutable()) {
            this.x = Math.pow(this.x, x);
            this.y = Math.pow(this.y, y);
            this.z = Math.pow(this.z, z);

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>() {};
            tmpVector.x = Math.pow(this.x, x);
            tmpVector.y = Math.pow(this.y, y);
            tmpVector.z = Math.pow(this.z, z);

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Square Root */
    public Vector3<T> sqrt() {
        if (this.isMutable()) {
            this.x = Math.sqrt(this.x);
            this.y = Math.sqrt(this.y);
            this.z = Math.sqrt(this.z);

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>() {};
            tmpVector.x = Math.sqrt(this.x);
            tmpVector.y = Math.sqrt(this.y);
            tmpVector.z = Math.sqrt(this.z);

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Cube Root */
    public Vector3<T> cbrt() {
        if (this.isMutable()) {
            this.x = Math.cbrt(this.x);
            this.y = Math.cbrt(this.y);
            this.z = Math.cbrt(this.z);

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>() {};
            tmpVector.x = Math.cbrt(this.x);
            tmpVector.y = Math.cbrt(this.y);
            tmpVector.z = Math.cbrt(this.z);

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Normalize */
    public Vector3<T> normalize() {
        if (this.isMutable()) {
            this.set(this.divide(this.length()));

            return this;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(this) {};
            tmpVector.set(tmpVector.divide(this.length()));

            return (Vector3<T>) tmpVector;
        }
    }

    /* Math: Dot */
    public double dot(Vector3<?> other) {
        return this.x * other.x + this.y * other.y + this.z * other.z;
    }

    /* Math: Utilities */
    public double length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    /** Vector Functions **/
    @Override
    public Vector3<?> applyTransformation(Transformation transformation) {
        if (this.isMutable()) {
            this.set(transformation.toV3(this));

            return this;
        } else {
            return this.set(transformation.toV3(this));
        }
    }

    /** Conversion **/
    public Vector4<T> toV4(int offset) {
        if (this.isMutable()) {
            return (Vector4<T>) ((offset == 0) ? new Vector4<IMutable>(this.x, this.y, this.z, 0) {}
                    : (offset == 1) ? new Vector4<IMutable>(this.y, this.z, this.x, 0) {}
                            : (offset == 2) ? new Vector4<IMutable>(this.z, this.x, this.y, 0) {} : this.toV4(0));
        } else {
            return (Vector4<T>) ((offset == 0) ? new Vector4<IImmutable>(this.x, this.y, this.z, 0) {}
                    : (offset == 1) ? new Vector4<IImmutable>(this.y, this.z, this.x, 0) {}
                            : (offset == 2) ? new Vector4<IImmutable>(this.z, this.x, this.y, 0) {} : this.toV4(0));
        }
    }

    public Vector2<T> toV2(int offset) {
        if (this.isMutable()) {
            return (Vector2<T>) ((offset == 0) ? new Vector2<IMutable>(this.x, this.y) {}
                    : (offset == 1) ? new Vector2<IMutable>(this.y, this.z) {}
                            : (offset == 2) ? new Vector2<IMutable>(this.z, this.x) {} : this.toV2(0));
        } else {
            return (Vector2<T>) ((offset == 0) ? new Vector2<IImmutable>(this.x, this.y) {}
                    : (offset == 1) ? new Vector2<IImmutable>(this.y, this.z) {}
                            : (offset == 2) ? new Vector2<IImmutable>(this.z, this.x) {} : this.toV2(0));
        }
    }

    /** MathObject Functions **/
    @Override
    public String toString() {
        return String.format("[%s, %s, %s]", this.x, this.y, this.z);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.x, this.y, this.z
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.x == ((Vector3<?>) obj).x && this.y == ((Vector3<?>) obj).y && this.z == ((Vector3<?>) obj).z)
            return true;

        return false;
    }
}