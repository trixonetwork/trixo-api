package com.trixo.math.vector;

import org.bukkit.Location;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;

public class VectorUtils {
    /** Location Functions **/
    public static Vector3<?> positionFromLocation(Location location, boolean mutable) {
        if (mutable) {
            return new Vector3<IMutable>(location.getX(), location.getY(), location.getZ()) {};
        } else {
            return new Vector3<IImmutable>(location.getX(), location.getY(), location.getZ()) {};
        }
    }

    public static Vector2<?> rotationFromLocation(Location location, boolean mutable) {
        if (mutable) {
            return new Vector2<IMutable>(location.getPitch(), location.getYaw()) {};
        } else {
            return new Vector2<IImmutable>(location.getX(), location.getYaw()) {};
        }
    }
}
