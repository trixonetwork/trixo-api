package com.trixo.math.vector;

import com.trixo.math.MathObject;
import com.trixo.math.Transformation;

public abstract class Vector<T> extends MathObject<T> {
    /** Vector Functions **/
    public Vector<?> applyTransformation(Transformation transformation) {
        return this;
    }
}
