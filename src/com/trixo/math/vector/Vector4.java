package com.trixo.math.vector;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.Transformation;

@SuppressWarnings("unchecked")
public class Vector4<T> extends Vector<T> {
    private double x = 0, y = 0, z = 0, w = 0;

    /* Constructors */
    public Vector4() {
        this(0);
    }

    public Vector4(double N) {
        this(N, N, N, N);
    }

    public Vector4(Vector4<?> other) {
        this(other.x, other.y, other.z, other.w);
    }

    public Vector4(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /** Setters and Getters **/
    /* Setters */
    public Vector4<T> set(double N) {
        return this.set(N, N, N, N);
    }

    public Vector4<T> set(Vector4<?> other) {
        return this.set(other.x, other.y, other.z, other.w);
    }

    public Vector4<T> set(double x, double y, double z, double w) {
        if (this.isMutable()) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(this) {};
            tmpVector.x = x;
            tmpVector.y = y;
            tmpVector.z = z;
            tmpVector.w = w;

            return (Vector4<T>) tmpVector;
        }
    }

    public void setX(double n) {
        this.set(n, this.y, this.z, this.w);
    }

    public void setY(double n) {
        this.set(this.x, n, this.z, this.w);
    }

    public void setZ(double n) {
        this.set(this.x, this.y, n, this.w);
    }

    public void setW(double n) {
        this.set(this.x, this.y, this.z, n);
    }

    /* Getters */
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double getW() {
        return this.w;
    }

    /** Math Functions **/
    /* Math: Add */
    public Vector4<T> add(double N) {
        return this.add(N, N, N, N);
    }

    public Vector4<T> add(Vector4<?> other) {
        return this.add(other.x, other.y, other.z, other.w);
    }

    public Vector4<T> add(double x, double y, double z, double w) {
        if (this.isMutable()) {
            this.x += x;
            this.y += y;
            this.z += z;
            this.w += w;

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(this) {};
            tmpVector.x += x;
            tmpVector.y += y;
            tmpVector.z += z;
            tmpVector.w += w;

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Subtract */
    public Vector4<T> sub(double N) {
        return this.sub(N, N, N, N);
    }

    public Vector4<T> sub(Vector4<?> other) {
        return this.sub(other.x, other.y, other.z, other.w);
    }

    public Vector4<T> sub(double x, double y, double z, double w) {
        if (this.isMutable()) {
            this.x -= x;
            this.y -= y;
            this.z -= z;
            this.w -= w;

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(this) {};
            tmpVector.x -= x;
            tmpVector.y -= y;
            tmpVector.z -= z;
            tmpVector.w -= w;

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Multiply */
    public Vector4<T> mult(double N) {
        return this.mult(N, N, N, N);
    }

    public Vector4<T> mult(Vector4<?> other) {
        return this.mult(other.x, other.y, other.z, other.w);
    }

    public Vector4<T> mult(double x, double y, double z, double w) {
        if (this.isMutable()) {
            this.x *= x;
            this.y *= y;
            this.z *= z;
            this.w *= w;

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(this) {};
            tmpVector.x *= x;
            tmpVector.y *= y;
            tmpVector.z *= z;
            tmpVector.w *= w;

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Divide */
    public Vector4<T> divide(double N) {
        return this.divide(N, N, N, N);
    }

    public Vector4<T> divide(Vector4<?> other) {
        return this.divide(other.x, other.y, other.z, other.w);
    }

    public Vector4<T> divide(double x, double y, double z, double w) {
        if (this.isMutable()) {
            this.x /= x;
            this.y /= y;
            this.z /= z;
            this.w /= w;

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(this) {};
            tmpVector.x /= x;
            tmpVector.y /= y;
            tmpVector.z /= z;
            tmpVector.w /= w;

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Power */
    public Vector4<T> pow(double N) {
        return this.pow(N, N, N, N);
    }

    public Vector4<T> pow(Vector4<?> other) {
        return this.pow(other.x, other.y, other.z, other.w);
    }

    public Vector4<T> pow(double x, double y, double z, double w) {
        if (this.isMutable()) {
            this.x = Math.pow(this.x, x);
            this.y = Math.pow(this.y, y);
            this.z = Math.pow(this.z, z);
            this.w = Math.pow(this.w, w);

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>() {};
            tmpVector.x = Math.pow(tmpVector.x, x);
            tmpVector.y = Math.pow(tmpVector.y, y);
            tmpVector.z = Math.pow(tmpVector.z, z);
            tmpVector.w = Math.pow(tmpVector.w, w);

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Square Root */
    public Vector4<T> sqrt() {
        if (this.isMutable()) {
            this.x = Math.sqrt(this.x);
            this.y = Math.sqrt(this.y);
            this.z = Math.sqrt(this.z);
            this.w = Math.sqrt(this.w);

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>() {};
            tmpVector.x = Math.sqrt(this.x);
            tmpVector.y = Math.sqrt(this.y);
            tmpVector.z = Math.sqrt(this.z);
            tmpVector.w = Math.sqrt(this.w);

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Cube Root */
    public Vector4<T> cbrt() {
        if (this.isMutable()) {
            this.x = Math.cbrt(this.x);
            this.y = Math.cbrt(this.y);
            this.z = Math.cbrt(this.z);
            this.w = Math.cbrt(this.w);

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>() {};
            tmpVector.x = Math.cbrt(this.x);
            tmpVector.y = Math.cbrt(this.y);
            tmpVector.z = Math.cbrt(this.z);
            tmpVector.w = Math.cbrt(this.w);

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Normalize */
    public Vector4<T> normalize() {
        if (this.isMutable()) {
            this.set(this.divide(this.length()));

            return this;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(this.divide(this.length())) {};

            return (Vector4<T>) tmpVector;
        }
    }

    /* Math: Dot */
    public double dot(Vector4<?> other) {
        return this.x * other.x + this.y * other.y + this.z * other.z + this.w * other.w;
    }

    /* Math: Utilities */
    public double length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2) + Math.pow(this.w, 2));
    }

    /** Vector Functions **/
    @Override
    public Vector4<?> applyTransformation(Transformation transformation) {
        if (this.isMutable()) {
            this.set(transformation.toV4(this));

            return this;
        } else {
            return this.set(transformation.toV4(this));
        }
    }

    /** Conversion **/
    public Vector3<T> toV3(int offset) {
        if (this.isMutable()) {
            return (Vector3<T>) ((offset == 0) ? new Vector3<IMutable>(this.x, this.y, this.z) {}
                    : (offset == 1) ? new Vector3<IMutable>(this.y, this.z, this.w) {}
                            : (offset == 2) ? new Vector3<IMutable>(this.z, this.w, this.x) {}
                                    : (offset == 3) ? new Vector3<IMutable>(this.w, this.x, this.y) {} : this.toV3(0));
        } else {
            return (Vector3<T>) ((offset == 0) ? new Vector3<IImmutable>(this.x, this.y, this.z) {}
                    : (offset == 1) ? new Vector3<IImmutable>(this.y, this.z, this.w) {}
                            : (offset == 2) ? new Vector3<IImmutable>(this.z, this.w, this.x) {}
                                    : (offset == 3) ? new Vector3<IImmutable>(this.w, this.x, this.y) {}
                                            : this.toV3(0));
        }
    }

    public Vector2<T> toV2(int offset) {
        if (this.isMutable()) {
            return (Vector2<T>) ((offset == 0) ? new Vector2<IMutable>(this.x, this.y) {}
                    : (offset == 1) ? new Vector2<IMutable>(this.y, this.z) {}
                            : (offset == 2) ? new Vector2<IMutable>(this.z, this.w) {}
                                    : (offset == 3) ? new Vector2<IMutable>(this.w, this.x) {} : this.toV2(0));
        } else {
            return (Vector2<T>) ((offset == 0) ? new Vector2<IImmutable>(this.x, this.y) {}
                    : (offset == 1) ? new Vector2<IImmutable>(this.y, this.z) {}
                            : (offset == 2) ? new Vector2<IImmutable>(this.z, this.w) {}
                                    : (offset == 3) ? new Vector2<IImmutable>(this.w, this.x) {} : this.toV2(0));
        }
    }

    /** MathObject Functions **/
    @Override
    public String toString() {
        return String.format("[%s, %s, %s, %s]", this.x, this.y, this.z, this.w);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.x, this.y, this.z, this.w
        };
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.x == ((Vector4<?>) obj).x && this.y == ((Vector4<?>) obj).y && this.z == ((Vector4<?>) obj).z
                && this.w == ((Vector4<?>) obj).w)
            return true;

        return false;
    }
}
