package com.trixo.math;

import com.trixo.data.DataContainer;
import com.trixo.interfaces.IDataObject;
import com.trixo.math.vector.Vector2;
import com.trixo.math.vector.Vector3;
import com.trixo.math.vector.Vector4;

public class Transformation implements IDataObject<String, Object> {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public Transformation() {
        this.data = new DataContainer<String, Object>(false);
    }

    /** IDataObject Functions **/
    @Override
    public DataContainer<String, Object> asDataContainer() {
        return this.data;
    }

    /** Transformation Functions **/
    public <T> Vector4<T> toV4(Vector4<T> toTransform) {
        return new Vector4<T>() {};
    }

    public <T> Vector3<T> toV3(Vector3<T> toTransform) {
        return new Vector3<T>() {};
    }

    public <T> Vector2<T> toV2(Vector2<T> toTransform) {
        return new Vector2<T>() {};
    }
}
