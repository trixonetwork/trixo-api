package com.trixo.math;

import com.trixo.interfaces.IMutable;
import com.trixo.utils.GenericObject;
import com.trixo.utils.MathUtils;

public abstract class MathObject<T> extends GenericObject<T> {
    public boolean isMutable() {
        try {
            return (this.getGenericType() == IMutable.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;

        for (int i = 0; i < this.toArray().length; i++) {
            hashCode ^= MathUtils.toHash(this.toArray()[i]);
        }

        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        return false;
    }

    public double[] toArray() {
        return new double[] {};
    }

    public void flush() {
        System.out.println(this.toString());
    }
}
