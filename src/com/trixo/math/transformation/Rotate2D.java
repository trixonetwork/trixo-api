package com.trixo.math.transformation;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.Transformation;
import com.trixo.math.vector.Vector2;
import com.trixo.math.vector.Vector3;
import com.trixo.math.vector.Vector4;

@SuppressWarnings("unchecked")
public class Rotate2D extends Transformation {
    /** Constructor **/
    public <T> Rotate2D(double angle) {
        super();

        this.asDataContainer().put("angle", angle);
    }

    /** Applying **/
    @Override
    public <T> Vector4<T> toV4(Vector4<T> toTransform) {
        if (toTransform.isMutable()) {
            Vector4<IMutable> tmpVector = new Vector4<IMutable>(toTransform) {};
            tmpVector = (Vector4<IMutable>) this.rotate(tmpVector.toV3(0), (double) this.asDataContainer().get("angle"))
                    .toV4(0);

            return (Vector4<T>) tmpVector;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(toTransform) {};
            tmpVector = (Vector4<IImmutable>) this
                    .rotate(tmpVector.toV3(0), (double) this.asDataContainer().get("angle")).toV4(0);

            return (Vector4<T>) tmpVector;
        }
    }

    @Override
    public <T> Vector3<T> toV3(Vector3<T> toTransform) {
        if (toTransform.isMutable()) {
            Vector3<IMutable> tmpVector = new Vector3<IMutable>(toTransform) {};
            tmpVector = (Vector3<IMutable>) this.rotate(tmpVector, (double) this.asDataContainer().get("angle"))
                    .toV3(0);

            return (Vector3<T>) tmpVector;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(toTransform) {};
            tmpVector = (Vector3<IImmutable>) this.rotate(tmpVector, (double) this.asDataContainer().get("angle"))
                    .toV3(0);

            return (Vector3<T>) tmpVector;
        }
    }

    @Override
    public <T> Vector2<T> toV2(Vector2<T> toTransform) {
        if (toTransform.isMutable()) {
            Vector2<IMutable> tmpVector = new Vector2<IMutable>(toTransform) {};
            tmpVector = (Vector2<IMutable>) this.rotate(tmpVector.toV3(0),
                    (double) this.asDataContainer().get("angle"));

            return (Vector2<T>) tmpVector;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(toTransform) {};
            tmpVector = (Vector2<IImmutable>) this.rotate(tmpVector.toV3(0),
                    (double) this.asDataContainer().get("angle"));

            return (Vector2<T>) tmpVector;
        }
    }

    /** Math **/
    public <T> Vector2<?> rotate(Vector3<?> toTransform, double angle) {
        double angleRadians = (Math.PI * angle) / 180.0;

        if (toTransform.isMutable()) {
            return new Vector2<IMutable>(
                    (toTransform.getX() * Math.cos(angleRadians)) - (toTransform.getY() * Math.sin(angleRadians)),
                    (toTransform.getY() * Math.cos(angleRadians)) + (toTransform.getX() * Math.sin(angleRadians))) {};
        } else {
            return new Vector2<IImmutable>(
                    (toTransform.getX() * Math.cos(angleRadians)) - (toTransform.getY() * Math.sin(angleRadians)),
                    (toTransform.getY() * Math.cos(angleRadians)) + (toTransform.getX() * Math.sin(angleRadians))) {};
        }
    }
}