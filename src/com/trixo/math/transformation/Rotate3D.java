package com.trixo.math.transformation;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.Transformation;
import com.trixo.math.vector.Vector2;
import com.trixo.math.vector.Vector3;
import com.trixo.math.vector.Vector4;

@SuppressWarnings("unchecked")
public class Rotate3D extends Transformation {
    /** Constructor **/
    public <T> Rotate3D(Vector3<T> axis, double angle) {
        super();

        this.asDataContainer().put("axis", axis);
        this.asDataContainer().put("angle", angle);
    }

    /** Applying **/
    @Override
    public <T> Vector4<T> toV4(Vector4<T> toTransform) {
        if (toTransform.isMutable()) {
            Vector4<IMutable> tmpVector = new Vector4<IMutable>(toTransform) {};
            tmpVector = (Vector4<IMutable>) this.rotate(tmpVector.toV3(0),
                    (Vector3<T>) this.asDataContainer().get("axis"), (double) this.asDataContainer().get("angle"))
                    .toV4(0);

            return (Vector4<T>) tmpVector;
        } else {
            Vector4<IImmutable> tmpVector = new Vector4<IImmutable>(toTransform) {};
            tmpVector = (Vector4<IImmutable>) this.rotate(tmpVector.toV3(0),
                    (Vector3<T>) this.asDataContainer().get("axis"), (double) this.asDataContainer().get("angle"))
                    .toV4(0);

            return (Vector4<T>) tmpVector;
        }
    }

    @Override
    public <T> Vector3<T> toV3(Vector3<T> toTransform) {
        if (toTransform.isMutable()) {
            Vector3<IMutable> tmpVector = new Vector3<IMutable>(toTransform) {};
            tmpVector = (Vector3<IMutable>) this.rotate(tmpVector, (Vector3<T>) this.asDataContainer().get("axis"),
                    (double) this.asDataContainer().get("angle"));

            return (Vector3<T>) tmpVector;
        } else {
            Vector3<IImmutable> tmpVector = new Vector3<IImmutable>(toTransform) {};
            tmpVector = (Vector3<IImmutable>) this.rotate(tmpVector, (Vector3<T>) this.asDataContainer().get("axis"),
                    (double) this.asDataContainer().get("angle"));

            return (Vector3<T>) tmpVector;
        }
    }

    @Override
    public <T> Vector2<T> toV2(Vector2<T> toTransform) {
        if (toTransform.isMutable()) {
            Vector2<IMutable> tmpVector = new Vector2<IMutable>(toTransform) {};
            tmpVector = (Vector2<IMutable>) this.rotate(tmpVector.toV3(0),
                    (Vector3<T>) this.asDataContainer().get("axis"), (double) this.asDataContainer().get("angle"))
                    .toV2(0);

            return (Vector2<T>) tmpVector;
        } else {
            Vector2<IImmutable> tmpVector = new Vector2<IImmutable>(toTransform) {};
            tmpVector = (Vector2<IImmutable>) this.rotate(tmpVector.toV3(0),
                    (Vector3<T>) this.asDataContainer().get("axis"), (double) this.asDataContainer().get("angle"))
                    .toV2(0);

            return (Vector2<T>) tmpVector;
        }
    }

    /** Math **/
    public <T> Vector3<?> rotate(Vector3<?> toTransform, Vector3<T> axis, double angle) {
        double angleRadians = (Math.PI * angle) / 180.0;

        double newX = toTransform.getX();
        double newY = toTransform.getY();
        double newZ = toTransform.getZ();

        if (axis.getX() >= 1) {
            newY = (newY * Math.cos(angleRadians)) - (newZ * Math.sin(angleRadians));
            newZ = (newY * Math.sin(angleRadians)) + (newZ * Math.cos(angleRadians));
        }

        if (axis.getY() >= 1) {
            newX = (newZ * Math.cos(angleRadians)) - (newX * Math.sin(angleRadians));
            newZ = (newZ * Math.sin(angleRadians)) + (newX * Math.cos(angleRadians));
        }

        if (axis.getZ() >= 1) {
            newX = (newX * Math.cos(angleRadians)) - (newY * Math.sin(angleRadians));
            newY = (newX * Math.sin(angleRadians)) + (newY * Math.cos(angleRadians));
        }

        if (toTransform.isMutable()) {
            return new Vector3<IMutable>(newX, newY, newZ) {};
        } else {
            return new Vector3<IImmutable>(newX, newY, newZ) {};
        }
    }
}
