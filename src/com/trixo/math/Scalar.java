package com.trixo.math;

import javax.annotation.concurrent.Immutable;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.vector.Vector2;
import com.trixo.math.vector.Vector3;
import com.trixo.math.vector.Vector4;

@SuppressWarnings("unchecked")
public class Scalar<T> extends MathObject<T> {
    private double x = 0;

    /* Constructors */
    public Scalar() {
        this(0);
    }

    public Scalar(Scalar<?> other) {
        this(other.x);
    }

    public Scalar(double x) {
        this.x = x;
    }

    /** Setters and Getters **/
    /* Setters */
    public Scalar<T> set(Scalar<?> other) {
        return this.set(other.x);
    }

    public Scalar<T> set(double x) {
        if (this.isMutable()) {
            this.x = x;

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>(this) {};
            tmpScalar.x = x;

            return (Scalar<T>) tmpScalar;
        }
    }
    
    /* Getters */
    public double get() {
        return this.x;
    }
    
    /** Math Functions **/
    /* Math: Add */
    public Scalar<T> add(Scalar<?> other) {
        return this.add(other.x);
    }

    public Scalar<T> add(double x) {
        if (this.isMutable()) {
            this.x += x;

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>(this) {};
            tmpScalar.x += x;

            return (Scalar<T>) tmpScalar;
        }
    }

    /* Math: Subtract */
    public Scalar<T> sub(Scalar<?> other) {
        return this.sub(other.x);
    }

    public Scalar<T> sub(double x) {
        if (this.isMutable()) {
            this.x -= x;

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>(this) {};
            tmpScalar.x -= x;

            return (Scalar<T>) tmpScalar;
        }
    }

    /* Math: Multiply */
    public Scalar<T> mult(Scalar<?> other) {
        return this.mult(other.x);
    }

    public Scalar<T> mult(double x) {
        if (this.isMutable()) {
            this.x *= x;

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>(this) {};
            tmpScalar.x *= x;

            return (Scalar<T>) tmpScalar;
        }
    }

    /* Math: Divide */
    public Scalar<T> divide(Scalar<?> other) {
        return this.divide(other.x);
    }

    public Scalar<T> divide(double x) {
        if (this.isMutable()) {
            this.x /= x;

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>(this) {};
            tmpScalar.x /= x;

            return (Scalar<T>) tmpScalar;
        }
    }

    /* Math: Power */
    public Scalar<T> pow(Scalar<?> other) {
        return this.pow(other.x);
    }

    public Scalar<T> pow(double x) {
        if (this.isMutable()) {
            this.x = Math.pow(this.x, x);

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>() {};
            tmpScalar.x = Math.pow(tmpScalar.x, x);

            return (Scalar<T>) tmpScalar;
        }
    }

    /* Math: Square Root */
    public Scalar<T> sqrt() {
        if (this.isMutable()) {
            this.x = Math.sqrt(this.x);

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>() {};
            tmpScalar.x = Math.sqrt(this.x);

            return (Scalar<T>) tmpScalar;
        }
    }

    /* Math: Cube Root */
    public Scalar<T> cbrt() {
        if (this.isMutable()) {
            this.x = Math.cbrt(this.x);

            return this;
        } else {
            Scalar<IImmutable> tmpScalar = new Scalar<IImmutable>() {};
            tmpScalar.x = Math.cbrt(this.x);

            return (Scalar<T>) tmpScalar;
        }
    }

    /** Conversion **/
    public Vector4<?> toV4() {
        if (this.isMutable()) {
            return new Vector4<IMutable>(this.x, this.x, this.x, this.x) {};
        } else {
            return new Vector4<Immutable>(this.x, this.x, this.x, this.x) {};
        }
    }

    public Vector3<?> toV3() {
        if (this.isMutable()) {
            return new Vector3<IMutable>(this.x, this.x, this.x) {};
        } else {
            return new Vector3<Immutable>(this.x, this.x, this.x) {};
        }
    }

    public Vector2<?> toV2() {
        if (this.isMutable()) {
            return new Vector2<IMutable>(this.x, this.x) {};
        } else {
            return new Vector2<Immutable>(this.x, this.x) {};
        }
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.x == ((Scalar<?>) obj).x)
            return true;

        return false;
    }

    @Override
    public String toString() {
        return String.format("[%s]", this.x);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.x
        };
    }
}