package com.trixo.math.shapes;

import java.util.ArrayList;

import com.trixo.interfaces.IImmutable;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector3;

public class Box<T> extends Shape<T> {
    private Vector3<?> size = null;

    /** Constructor **/
    public Box() {
        this(new Vector3<IImmutable>(0) {}, new Vector3<IImmutable>(0) {});
    }
    
    public Box(double x, double y, double z, double sX, double sY, double sZ) {
        this(new Vector3<IImmutable>(x, y, z) {}, new Vector3<IImmutable>(sX, sY, sZ) {});
    }

    public Box(Vector3<?> position, Vector3<?> size) {
        this.setPosition(position);
        this.size = size;
    }

    /** Bounds **/
    public void setBounds(Vector3<?> position, Vector3<?> size) {
        this.setPosition(position);
        this.size = size;
    }

    public void setSize(Vector3<?> size) {
        this.size = size;
    }

    public Vector3<?> getSize() {
        return this.size;
    }

    /** Math Functions **/
    public double getDiagionalLength() {
        return Math.sqrt(Math.pow(this.size.getX(), 2) + Math.pow(this.size.getY(), 2) + Math.pow(this.size.getZ(), 2));
    }

    public boolean containsPoint(Point<?> point) {
        return (point.getPosition().getX() > this.getPosition().getX() && point.getPosition().getX() < this.size.getX()
                && point.getPosition().getY() > this.getPosition().getY()
                && point.getPosition().getY() < this.size.getY()
                && point.getPosition().getZ() > this.getPosition().getZ()
                && point.getPosition().getZ() < this.size.getZ());
    }

    public boolean intersects(Box<?> other) {
        return (this.size.getX() > other.getPosition().getX() && this.getPosition().getX() < other.getSize().getX()
                && this.size.getY() > other.getPosition().getY() && this.getPosition().getY() < other.getSize().getY()
                && this.size.getZ() > other.getPosition().getZ() && this.getPosition().getZ() < other.getSize().getZ());
    }

    /** Shape: Iterator **/
    @Override
    public void calculateIterator(double increment) {
        if (this.getIteratorData() != null) {
            this.getIteratorData().clear();
        } else {
            this.setIteratorData(new ArrayList<Vector<?>>());
        }

        for (double x = this.getPosition().getX(); x < (this.getPosition().getX() + this.size.getX()); x += increment) {
            for (double y = this.getPosition().getY(); y < (this.getPosition().getY() + this.size.getY()); y += increment) {
                for (double z = this.getPosition().getZ(); z < (this.getPosition().getZ() + this.size.getZ()); z += increment) {
                    this.getIteratorData().add(new Vector3<IImmutable>(x, y, z) {});
                }
            }
        }
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.getPosition() == ((Box<?>) obj).getPosition() && this.size == ((Box<?>) obj).size)
            return true;

        return false;
    }

    @Override
    public String toString() {
        return String.format("[%s | %s]", this.getPosition(), this.size);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ(), this.size.getX(),
                this.size.getY(), this.size.getZ()
        };
    }
}
