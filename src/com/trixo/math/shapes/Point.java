package com.trixo.math.shapes;

import java.util.ArrayList;

import com.trixo.interfaces.IImmutable;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector3;

public class Point<T> extends Shape<T> {
    /** Constructors **/
    public Point() {
        this(new Vector3<IImmutable>(0) {});
    }

    public Point(double x, double y, double z) {
        this(new Vector3<IImmutable>(x, y, z) {});
    }

    public Point(Vector3<?> position) {
        this.setPosition(position);
    }

    /** Shape: Iterator **/
    @Override
    public void calculateIterator(double increment) {
        if (this.getIteratorData() != null) {
            this.getIteratorData().clear();
        } else {
            this.setIteratorData(new ArrayList<Vector<?>>());
        }

        this.getIteratorData().add(this.getPosition());
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.getPosition() == ((Point<?>) obj).getPosition())
            return true;

        return false;
    }

    @Override
    public String toString() {
        return this.getPosition().toString();
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ()
        };
    }
}
