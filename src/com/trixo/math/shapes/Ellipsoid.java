package com.trixo.math.shapes;

import java.util.ArrayList;

import com.trixo.interfaces.IImmutable;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector3;

@SuppressWarnings("unchecked")
public class Ellipsoid<T> extends Shape<T> {
    private Vector3<?> radius = null;

    /** Constructor **/
    public Ellipsoid() {
        this(new Vector3<IImmutable>(0) {}, new Vector3<IImmutable>(0) {});
    }

    public Ellipsoid(double x, double y, double z, double rX, double rY, double rZ) {
        this(new Vector3<IImmutable>(x, y, z) {}, new Vector3<IImmutable>(rX, rY, rZ) {});
    }

    public Ellipsoid(Vector3<?> position, Vector3<?> radius) {
        this.setPosition(position);
        this.radius = radius;
    }

    /** Bounds **/
    public void setRadius(Vector3<T> radius) {
        this.radius = radius;
    }

    public Vector3<?> getRadius() {
        return this.radius;
    }

    /** Math Functions **/
    public double getDiagionalLength() {
        return Math.sqrt(
                Math.pow(this.radius.getX(), 2) + Math.pow(this.radius.getY(), 2) + Math.pow(this.radius.getZ(), 2));
    }

    public boolean containsPoint(Vector3<?> point) {
        Vector3<IImmutable> centerPos = (Vector3<IImmutable>) this.getPosition();
        Vector3<IImmutable> pointPos = (Vector3<IImmutable>) point;
        Vector3<IImmutable> radius = (Vector3<IImmutable>) this.getRadius();

        Vector3<IImmutable> distance = pointPos.sub(centerPos);
        
        return ((double) (distance.getX() * distance.getX()) / (radius.getX() * radius.getX()))
                + ((double) (distance.getY() * distance.getY())) / (radius.getY() * radius.getY())
                + ((double) (distance.getZ() * distance.getZ())) / (radius.getZ() * radius.getZ()) <= 1.0;
    }

    public boolean containsPoint(Point<?> point) {
        return this.containsPoint(point.getPosition());
    }

    public boolean intersects(Ellipsoid<?> other) {
        Vector3<IImmutable> centerPos = (Vector3<IImmutable>) this.getPosition();
        Vector3<IImmutable> pointPos = (Vector3<IImmutable>) other.getPosition();
        Vector3<IImmutable> radius = (Vector3<IImmutable>) this.getRadius();
        Vector3<IImmutable> radiusOther = (Vector3<IImmutable>) other.getRadius();

        Vector3<IImmutable> distanceSQ = pointPos.sub(centerPos).pow(2);
        Vector3<IImmutable> radiiSumSQ = radius.add(radiusOther).pow(2);

        if (distanceSQ.getX() <= radiiSumSQ.getX())
            if (distanceSQ.getY() <= radiiSumSQ.getY())
                if (distanceSQ.getZ() <= radiiSumSQ.getZ())
                    return true;

        return false;
    }

    /** Shape: Iterator **/
    @Override
    public void calculateIterator(double increment) {
        if (this.getIteratorData() != null) {
            this.getIteratorData().clear();
        } else {
            this.setIteratorData(new ArrayList<Vector<?>>());
        }

        for (double x = -this.radius.getX(); x < this.radius.getX(); x += increment) {
            for (double y = -this.radius.getY(); y < this.radius.getY(); y += increment) {
                for (double z = -this.radius.getZ(); z < this.radius.getZ(); z += increment) {
                    Vector3<IImmutable> absolutePosition = new Vector3<IImmutable>(x, y, z) {}.add(this.getPosition());

                    if (this.containsPoint(new Point<IImmutable>(absolutePosition) {})) {
                        this.getIteratorData().add(absolutePosition);
                    }
                }
            }
        }
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.getPosition() == ((Ellipsoid<?>) obj).getPosition() && this.radius == ((Ellipsoid<?>) obj).getRadius())
            return true;

        return false;
    }

    @Override
    public String toString() {
        return String.format("[%s | %s]", this.getPosition(), this.radius);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ(), this.radius.getX(),
                this.radius.getY(), this.radius.getZ()
        };
    }
}