package com.trixo.math.shapes;

import java.util.ArrayList;

import com.trixo.interfaces.IImmutable;
import com.trixo.interfaces.IMutable;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector3;

public class Line<T> extends Shape<T> {
    private double width = 0;
    private Point<?> p1 = null, p2 = null;

    /** Constructor **/
    public Line() {
        this(new Point<IImmutable>() {}, new Point<IImmutable>() {}, 0);
    }

    public Line(double sX, double sY, double sZ, double eX, double eY, double eZ) {
        this(new Point<IImmutable>(sX, sY, sZ) {}, new Point<IImmutable>(eX, eY, eZ) {}, 0);
    }

    public Line(Vector3<?> p1, Vector3<?> p2) {
        this(new Point<IImmutable>(p1) {}, new Point<IImmutable>(p2) {}, 0);
    }

    public Line(Point<?> p1, Point<?> p2, double width) {
        this.setPosition(p1.getPosition());
        this.width = width;
        this.p1 = p1;
        this.p2 = p2;
    }

    /** Setters and Getters **/
    /* Setters */
    public void setWidth(double width) {
        this.width = width;
    }

    public void setStart(Point<?> start) {
        this.p1 = start;
    }

    public void setEnd(Point<?> end) {
        this.p2 = end;
    }

    public void setStart(double x, double y, double z) {
        if (this.isMutable()) {
            this.p1 = new Point<IMutable>(x, y, z) {};
        } else {
            this.p1 = new Point<IImmutable>(x, y, z) {};
        }

    }

    public void setEnd(Vector3<?> end) {
        if (this.isMutable()) {
            this.p2 = new Point<IMutable>(end) {};
        } else {
            this.p2 = new Point<IImmutable>(end) {};
        }

    }

    public void setEnd(double x, double y, double z) {
        if (this.isMutable()) {
            this.p2 = new Point<IMutable>(x, y, z) {};
        } else {
            this.p2 = new Point<IImmutable>(x, y, z) {};
        }

    }

    /* Getters */
    public double getWidth() {
        return this.width;
    }

    public Point<?> getStart() {
        return this.p1;
    }

    public Point<?> getEnd() {
        return this.p2;
    }

    /** Shape: Iterator **/
    @Override
    public void calculateIterator(double increment) {
        if (this.getIteratorData() != null) {
            this.getIteratorData().clear();
        } else {
            this.setIteratorData(new ArrayList<Vector<?>>());
        }

        this.calculateLine();
    }

    /* Calculates the line using the Bresenham algorithm */
    private void calculateLine() {
        Vector3<IImmutable> currentPosition = new Vector3<IImmutable>(this.getStart().getPosition()) {};

        int x1 = (int) this.getStart().getPosition().getX();
        int y1 = (int) this.getStart().getPosition().getY();
        int z1 = (int) this.getStart().getPosition().getZ();
        int x2 = (int) this.getEnd().getPosition().getX();
        int y2 = (int) this.getEnd().getPosition().getY();
        int z2 = (int) this.getEnd().getPosition().getZ();

        int d1 = 0;
        int d2 = 0;

        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);
        int dz = Math.abs(z2 - z1);

        int dx2 = (dx << 1);
        int dy2 = (dy << 1);
        int dz2 = (dz << 1);

        int ix = x1 < x2 ? 1 : -1; // increment direction
        int iy = y1 < y2 ? 1 : -1;
        int iz = z1 < z2 ? 1 : -1;

        if (dx >= dy && dx >= dz) {
            for (;;) {
                this.getIteratorData().add(currentPosition.set(x1, y1, z1));

                if (x1 == x2)
                    break;

                x1 += ix;

                d1 += dy2;
                d2 += dz2;

                if (d1 > dx) {
                    y1 += iy;
                    d1 -= dx2;
                }

                if (d2 > dx) {
                    z1 += iz;
                    d2 -= dx2;
                }
            }
        } else if (dy >= dx && dy >= dz) {
            for (;;) {
                this.getIteratorData().add(currentPosition.set(x1, y1, z1));

                if (y1 == y2)
                    break;

                y1 += iy;
                d1 += dx2;
                d2 += dz2;

                if (d1 > dy) {
                    x1 += ix;
                    d1 -= dy2;
                }

                if (d2 > dy) {
                    z1 += iz;
                    d2 -= dy2;
                }
            }
        } else {
            for (;;) {
                this.getIteratorData().add(currentPosition.set(x1, y1, z1));

                if (z1 == z2)
                    break;

                z1 += iz;
                d1 += dz2;

                if (d1 > dz) {
                    x1 += ix;
                    d1 -= dz2;
                }

                if (d2 > dz) {
                    y1 += iy;
                    d2 -= dz2;
                }
            }
        }

        this.getIteratorData().add(currentPosition);
    }

    /** MathObject Functions **/
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj.getClass().getSuperclass() != this.getClass())
            return false;

        if (this.width == ((Line<?>) obj).getWidth() && this.p1 == ((Line<?>) obj).getStart()
                && this.p2 == ((Line<?>) obj).getEnd())
            return true;

        return false;
    }

    @Override
    public String toString() {
        return String.format("[%s | %s | %s]", this.p1, this.p2, this.width);
    }

    @Override
    public double[] toArray() {
        return new double[] {
                this.p1.getPosition().getX(), this.p1.getPosition().getY(), this.p1.getPosition().getZ(),
                this.p2.getPosition().getX(), this.p2.getPosition().getY(), this.p2.getPosition().getZ(), this.width
        };
    }
}