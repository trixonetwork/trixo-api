package com.trixo.math.shapes;

import java.util.ArrayList;

import com.trixo.math.MathObject;
import com.trixo.math.vector.Vector;
import com.trixo.math.vector.Vector3;

public abstract class Shape<T> extends MathObject<T> {
    private Vector3<?> position = null;
    private ArrayList<Vector<?>> iteratorData = null;
    private ShapeIterator iterator = null;
    
    /** ShapeIterator **/
    public ShapeIterator getIterator() {
        if(this.iterator == null) {
            this.iterator = new ShapeIterator(this.getIteratorData());
        } else {
            if(this.iterator.finished()) {
                this.iterator = new ShapeIterator(this.getIteratorData());
            }
        }
        
        return this.iterator;
    }

    public abstract void calculateIterator(double increment);

    public void setIteratorData(ArrayList<Vector<?>> iteratorData) {
        this.iteratorData = iteratorData;
    }

    public ArrayList<Vector<?>> getIteratorData() {
        return this.iteratorData;
    }

    /** Position **/
    public void setPosition(Vector3<?> position) {
        this.position = position;
    }

    public Vector3<?> getPosition() {
        return this.position;
    }
}
