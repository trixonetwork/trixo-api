package com.trixo.math.shapes;

import java.util.ArrayList;

import com.trixo.math.vector.Vector;

public class ShapeIterator {
    private ArrayList<Vector<?>> data = null;
    private int currentSize = 0, currentIndex = 0;

    /** Constructor **/
    public ShapeIterator(ArrayList<Vector<?>> data) {
        this.data = data;
        this.currentSize = (data == null ? 0 : data.size());
    }

    /** Iterator **/
    public boolean hasNext() {
        return this.currentIndex < this.currentSize && this.data.get(this.currentIndex) != null;
    }

    public boolean finished() {
        return (this.currentIndex - 1) == this.currentSize;
    }
    
    public Vector<?> next() {
        return this.data.get(this.currentIndex++);
    }
}
