package com.trixo.math.matrix;

import java.nio.DoubleBuffer;

public class Matrix3<T> extends Matrix<T> {
    public double m00, m10, m20;
    public double m01, m11, m21;
    public double m02, m12, m22;

    /** Setters **/
    /* Setters */
    @Override
    public Matrix<T> set(double[] dArr) {
        return null;
    }

    @Override
    public Matrix<T> set(DoubleBuffer dBuf) {
        return null;
    }

    @Override
    public Matrix<T> set(Matrix<?> m) {
        return null;
    }
}
