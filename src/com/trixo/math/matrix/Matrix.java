package com.trixo.math.matrix;

import java.nio.DoubleBuffer;

import com.trixo.math.MathObject;

public abstract class Matrix<T> extends MathObject<T> {
    /** Getters, Setters **/
    /* Getters */
    public DoubleBuffer get(DoubleBuffer dBuf) {
        return dBuf;
    }

    /* Setters */
    public abstract Matrix<T> set(double[] dArr);

    public abstract Matrix<T> set(DoubleBuffer dBuf);

    public abstract Matrix<T> set(Matrix<?> m);
}
