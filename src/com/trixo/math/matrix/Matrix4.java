package com.trixo.math.matrix;

import java.nio.DoubleBuffer;

import com.trixo.interfaces.IImmutable;
import com.trixo.math.vector.Vector3;

@SuppressWarnings("unchecked")
public class Matrix4<T> extends Matrix<T> {
    public double m00, m10, m20, m30;
    public double m01, m11, m21, m31;
    public double m02, m12, m22, m32;
    public double m03, m13, m23, m33;

    /** Constructors **/
    public Matrix4(double m00, double m10, double m20, double m30, double m01, double m11, double m21, double m31,
            double m02, double m12, double m22, double m32, double m03, double m13, double m23, double m33) {
        this.set(m00, m10, m20, m30, m01, m11, m21, m31, m02, m12, m22, m32, m03, m13, m23, m33);
    }

    public Matrix4(double[] values) {
        this.set(values);
    }

    public Matrix4(DoubleBuffer buf) {
        this.set(buf);
    }

    public Matrix4(Matrix4<?> other) {
        this.set(other);
    }

    public Matrix4() {
        this.setToIdentity();
    }

    /** Math Functions **/
    /* Identity */
    public Matrix4<?> setToIdentity() {
        return this.set(1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1f);
    }

    /* Scale */
    public Matrix4<T> setToScale(Vector3<?> scale) {
        return this.setToScale(scale.getX(), scale.getY(), scale.getZ());
    }

    public Matrix4<T> setToScale(double s) {
        return this.setToScale(s, s, s);
    }

    public Matrix4<T> setToScale(double x, double y, double z) {
        return this.set(x, 0f, 0f, 0f, 0f, y, 0f, 0f, 0f, 0f, z, 0f, 0f, 0f, 0f, 1f);
    }

    /* Reflection */
    public Matrix4<T> setToReflection(Vector3<?> normal) {
        return this.setToReflection(normal.getX(), normal.getY(), normal.getZ());
    }

    public Matrix4<T> setToReflection(double x, double y, double z) {
        double x2 = -2f * x, y2 = -2f * y, z2 = -2f * z;
        double xy2 = x2 * y, xz2 = x2 * z, yz2 = y2 * z;

        return this.set(1f + x2 * x, xy2, xz2, 0f, xy2, 1f + y2 * y, yz2, 0f, xz2, yz2, 1f + z2 * z, 0f, 0f, 0f, 0f,
                1f);
    }

    public Matrix4<T> setToReflection(Vector3<?> normal, double constant) {
        return this.setToReflection(normal.getX(), normal.getY(), normal.getZ(), constant);
    }

    public Matrix4<T> setToReflection(double x, double y, double z, double w) {
        double x2 = -2f * x, y2 = -2f * y, z2 = -2f * z;
        double xy2 = x2 * y, xz2 = x2 * z, yz2 = y2 * z;
        double x2y2z2 = x * x + y * y + z * z;

        return this.set(1f + x2 * x, xy2, xz2, x2 * w * x2y2z2, xy2, 1f + y2 * y, yz2, y2 * w * x2y2z2, xz2, yz2,
                1f + z2 * z, z2 * w * x2y2z2, 0f, 0f, 0f, 1f);
    }

    /* Skew */
    public Matrix4<T> setToSkew(Vector3<?> normal, double constant, Vector3<?> amount) {
        return this.setToSkew(normal.getX(), normal.getY(), normal.getZ(), constant, amount.getX(), amount.getY(),
                amount.getZ());
    }

    public Matrix4<T> setToSkew(double a, double b, double c, double d, double x, double y, double z) {
        return this.set(1f + a * x, b * x, c * x, d * x, a * y, 1f + b * y, c * y, d * y, a * z, b * z, 1f + c * z,
                d * z, 0f, 0f, 0f, 1f);
    }

    /* Perspective */
    public Matrix4<T> setToPerspective(double fovy, double aspect, double near, double far) {
        double f = 1f / Math.tan(fovy / 2f), dscale = 1f / (near - far);

        return this.set(f / aspect, 0f, 0f, 0f, 0f, f, 0f, 0f, 0f, 0f, (far + near) * dscale, 2f * far * near * dscale,
                0f, 0f, -1f, 0f);
    }

    /* Frustum */
    public Matrix4<T> setToFrustum(double left, double right, double bottom, double top, double near, double far) {
        return this.setToFrustum(left, right, bottom, top, near, far, new Vector3<IImmutable>(0.0, 0.0, 1.0) {});
    }

    public Matrix4<T> setToFrustum(double left, double right, double bottom, double top, double near, double far,
            Vector3<?> nearFarNormal) {
        double rrl = 1f / (right - left);
        double rtb = 1f / (top - bottom);
        double rnf = 1f / (near - far);
        double n2 = 2f * near;
        double s = (far + near) / (near * nearFarNormal.getZ() - far * nearFarNormal.getZ());

        return this.set(n2 * rrl, 0f, (right + left) * rrl, 0f, 0f, n2 * rtb, (top + bottom) * rtb, 0f,
                s * nearFarNormal.getX(), s * nearFarNormal.getY(), (far + near) * rnf, n2 * far * rnf, 0f, 0f, -1f,
                0f);
    }

    /* Ortho */
    public Matrix4<T> setToOrtho(double left, double right, double bottom, double top, double near, double far) {
        return this.setToOrtho(left, right, bottom, top, near, far, new Vector3<IImmutable>(0.0, 0.0, 1.0) {});
    }

    public Matrix4<T> setToOrtho(double left, double right, double bottom, double top, double near, double far,
            Vector3<?> nearFarNormal) {
        double rlr = 1f / (left - right);
        double rbt = 1f / (bottom - top);
        double rnf = 1f / (near - far);
        double s = 2f / (near * nearFarNormal.getZ() - far * nearFarNormal.getZ());

        return this.set(-2f * rlr, 0f, 0f, (right + left) * rlr, 0f, -2f * rbt, 0f, (top + bottom) * rbt,
                s * nearFarNormal.getX(), s * nearFarNormal.getY(), 2f * rnf, (far + near) * rnf, 0f, 0f, 0f, 1f);
    }

    /** Getters, Setters **/
    /* Getters */
    public DoubleBuffer get(DoubleBuffer dBuf) {
        dBuf.put(this.m00).put(this.m01).put(this.m02).put(this.m03);
        dBuf.put(this.m10).put(this.m11).put(this.m12).put(this.m13);
        dBuf.put(this.m20).put(this.m21).put(this.m22).put(this.m23);
        dBuf.put(this.m30).put(this.m31).put(this.m32).put(this.m33);

        return dBuf;
    }

    /* Setters */
    public Matrix4<T> set(double m00, double m10, double m20, double m30, double m01, double m11, double m21,
            double m31, double m02, double m12, double m22, double m32, double m03, double m13, double m23,
            double m33) {
        if (this.isMutable()) {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m03 = m03;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m13 = m13;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
            this.m23 = m23;
            this.m30 = m30;
            this.m31 = m31;
            this.m32 = m32;
            this.m33 = m33;
        } else {
            this.m00 = m00;
            this.m01 = m01;
            this.m02 = m02;
            this.m03 = m03;
            this.m10 = m10;
            this.m11 = m11;
            this.m12 = m12;
            this.m13 = m13;
            this.m20 = m20;
            this.m21 = m21;
            this.m22 = m22;
            this.m23 = m23;
            this.m30 = m30;
            this.m31 = m31;
            this.m32 = m32;
            this.m33 = m33;
        }

        return this;
    }

    @Override
    public Matrix4<T> set(double[] values) {
        return this.set(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7],
                values[8], values[9], values[10], values[11], values[12], values[13], values[14], values[15]);
    }

    @Override
    public Matrix4<T> set(DoubleBuffer dBuf) {
        return this.set(dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(),
                dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get(), dBuf.get());
    }

    @Override
    public Matrix4<T> set(Matrix<?> m) {
        Matrix4<T> other = (Matrix4<T>) m;

        return set(other.m00(), other.m10(), other.m20(), other.m30(), other.m01(), other.m11(), other.m21(),
                other.m31(), other.m02(), other.m12(), other.m22(), other.m32(), other.m03(), other.m13(), other.m23(),
                other.m33());
    }

    /** Matrix4 Functions **/

    public double m00() {
        return this.m00;
    }

    public double m10() {
        return this.m10;
    }

    public double m20() {
        return this.m20;
    }

    public double m30() {
        return this.m30;
    }

    public double m01() {
        return this.m01;
    }

    public double m11() {
        return this.m11;
    }

    public double m21() {
        return this.m21;
    }

    public double m31() {
        return this.m31;
    }

    public double m02() {
        return this.m02;
    }

    public double m12() {
        return this.m12;
    }

    public double m22() {
        return this.m22;
    }

    public double m32() {
        return this.m32;
    }

    public double m03() {
        return this.m03;
    }

    public double m13() {
        return this.m13;
    }

    public double m23() {
        return this.m23;
    }

    public double m33() {
        return this.m33;
    }
}
