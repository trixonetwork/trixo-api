package com.trixo.network;

import org.bukkit.plugin.messaging.PluginMessageListener;

import com.trixo.TrixoAPI;
import com.trixo.data.DataCenter;

public class PMCManager {
    public void registerIPC(String channel, Class<? extends PluginMessageListener> c) {
        TrixoAPI plugin = DataCenter.retrieveData("api", "plugin", TrixoAPI.class);

        try {
            plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, channel, c.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void registerOPC(String channel) {
        TrixoAPI plugin = DataCenter.retrieveData("api", "plugin", TrixoAPI.class);

        plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, channel);
    }
}
