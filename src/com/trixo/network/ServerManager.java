package com.trixo.network;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.trixo.TrixoAPI;
import com.trixo.data.DataCenter;
import com.trixo.server.player.TrixoPlayer;

public class ServerManager {
    public void sendPlayer(TrixoPlayer player, String serverName) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(serverName);
        
        player.getPlayerInstance().sendPluginMessage(DataCenter.retrieveData("api", "plugin", TrixoAPI.class), "BungeeCord", out.toByteArray());
    }
}
