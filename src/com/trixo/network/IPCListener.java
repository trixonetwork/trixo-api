package com.trixo.network;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

public abstract class IPCListener implements PluginMessageListener {
    /** PluginMessageListener Functions **/
    @Override
    @Deprecated
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        this.handleMessage(channel, player, ByteStreams.newDataInput(message));
    }

    /** IPCListener Functions **/
    public abstract void handleMessage(String channel, Player player, ByteArrayDataInput input);
}
