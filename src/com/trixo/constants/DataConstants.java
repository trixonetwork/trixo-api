package com.trixo.constants;

public class DataConstants {
    public enum DataResponseCode {
        /** Enum Data **/
        NO_ERROR(0),
        NO_DATABASE_DRIVER(1),
        DATABASE_CONNECT_ERROR(2),
        DATABASE_LOGIN_ERROR(3),
        FAILLOAD_DATABASE_DRIVER(4);

        /** Enum Structure **/
        private int code = -1;

        /* Constructor */
        DataResponseCode(int code) {
            this.code = code;
        }

        /* Getters */
        public int getCode() {
            return code;
        }
    }
}
