package com.trixo.constants;

public class ColourConstants {
    public enum BlendMode {
        /** Enum Data **/
        ADDITIVE(0),
        ADDITIVE_RATIO(1),
        SUBTRACTIVE(2),
        SUBTRACTIVE_RATIO(3);

        /** Enum Structure **/
        private int code = -1;

        /* Constructor */
        BlendMode(int code) {
            this.code = code;
        }

        /* Getters */
        public int getCode() {
            return code;
        }
    }
}
