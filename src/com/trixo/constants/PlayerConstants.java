package com.trixo.constants;

public class PlayerConstants {
    public enum PlayerDataType {
        /** Enum Data **/
        ALL(),
        PLAYER(),
        FRIENDS(),
        STATS(),
        COSMETICS(),
        CLASSES();
    }

    public static enum MorphType {
        /** Enum Data **/
        /* Passive */
        PIG("Pig"),
        COW("Cow"),
        SHEEP("Sheep"),
        CHICKEN("Chicken"),
        BAT("Bat", true),
        SQUID("Squid"),
        WOLF("Wolf"),
        GOLEM("Golem"),
        IRON_GOLEM("IronGolem"),
        OCELOT("Ocelot"),
        VILLAGER("Villager"),
        HORSE("Horse"),
        MOOSHROOM("MushroomCow"),

        /* Monster */
        ZOMBIE("Zombie"),
        SKELETON("Skeleton"),
        WITHER_SKELETON("Skeleton"),
        PIGZOMBIE("PigZombie"),
        BLAZE("Blaze", true),
        ENDERMAN("Enderman"),
        CREEPER("Creeper"),
        SPIDER("Spider"),
        CAVE_SPIDER("CaveSpider"),
        WITCH("Witch"),
        WITHER("Wither", true),
        GHAST("Ghast", true),
        GIANT("GiantZombie"),
        SLIME("Slime"),
        ENDER_DRAGON("EnderDragon", true),

        /* Other */
        BLOCK("FallingBlock"),
        PLAYER("");

        /** Enum Structure **/
        private String entityName = null;
        private boolean canFly = false;

        /* Constructor */
        MorphType(String entityName) {
            this.entityName = entityName;
        }
        
        MorphType(String entityName, boolean canFly) {
            this.entityName = entityName;
            this.canFly = canFly;
        }

        /* Getters */
        public String getEntityName() {
            return this.entityName;
        }
        
        public boolean canFly() {
            return this.canFly;
        }
    }
}