package com.trixo.constants;

import org.bukkit.ChatColor;

import com.trixo.server.chat.ChatFormatter;
import com.trixo.server.chat.ChatMessage;

public class ChatConstants {
    public enum ChatPrefixes {
        /** Enum Data **/
        FRIENDS(ChatColor.BLUE + "Friends>");

        /** Enum Structure **/
        private String prefix = "";

        /* Constructor */
        private ChatPrefixes(String prefix) {
            this.prefix = prefix;
        }

        /* ChatMessages Functions */
        @Override
        public String toString() {
            return this.prefix;
        }
    }

    public enum ChatMessages {
        /** Enum Data **/
        FRIENDS_INVITE(ChatFormatter.create(ChatColor.GREEN, ChatPrefixes.FRIENDS.toString(), ""));

        /** Enum Structure **/
        private String message = "";

        /* Constructor */
        private ChatMessages(ChatMessage message) {
            this.message = message.toString();
        }

        /* ChatMessages Functions */
        @Override
        public String toString() {
            return this.message;
        }
    }

    public enum ChatColours {
        /** Enum Data **/
        SYSTEM_DEFAULT(ChatColor.GREEN);

        /** Enum Structure **/
        private ChatColor colour = ChatColor.WHITE;

        /* Constructor */
        private ChatColours(ChatColor colour) {
            this.colour = colour;
        }

        /* Getters */
        public ChatColor getColour() {
            return this.colour;
        }
    }
}
