package com.trixo.minigame.team;

import java.util.UUID;

import org.bukkit.ChatColor;

import com.trixo.data.DataContainer;
import com.trixo.server.player.TrixoPlayer;

@SuppressWarnings("unchecked")
public class Team {
    private DataContainer<String, Object> data = null;

    /** Constructors **/
    public Team() {
        this.data = new DataContainer<String, Object>(false);
    }

    public Team(DataContainer<String, Object> data) {
        this.data = data;
    }

    /** Team Functions **/
    /* Other */
    public boolean isPlayerInTeam(UUID uuid) {
        return this.getPlayer(uuid) != null;
    }

    /** Getters, Setters, Adders, Removers **/
    /* Getters */
    public String getTeamName() {
        return (String) this.data.get("name");
    }

    public ChatColor getTeamColour() {
        return (ChatColor) this.data.get("colour");
    }

    public DataContainer<UUID, TrixoPlayer> getPlayers() {
        return (DataContainer<UUID, TrixoPlayer>) this.data.get("players");
    }

    public TrixoPlayer getPlayer(UUID uuid) {
        return this.getPlayer(uuid);
    }

    /* Setters */
    public void setTeamName(String name) {
        this.data.put("name", name);
    }

    public void setTeamColour(ChatColor colour) {
        this.data.put("colour", colour);
    }

    /* Adders */
    public void addPlayer(TrixoPlayer player) {
        player.setDisplayName(this.getTeamColour() + player.getName());
        player.setDisplayTag(this.getTeamName(), ChatColor.BOLD + "" + this.getTeamColour() + this.getTeamName());

        this.getPlayers().put(player.getUUID(), player);
    }

    /* Removers */
    public void removePlayer(TrixoPlayer player) {
        player.setPermissionGroupTag(player.getPermissionGroup());

        this.getPlayers().remove(player.getUUID());
    }
}
