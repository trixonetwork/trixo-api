package com.trixo.minigame.team;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataContainer;
import com.trixo.server.event.ITrixoEvent;
import com.trixo.server.player.TrixoPlayer;

@SuppressWarnings("unchecked")
public class TeamManager implements ITrixoEvent {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public TeamManager() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("teams", new DataContainer<String, Team>(true));
    }

    /** TeamManager Functions **/
    public boolean isPlayerInTeam(String teamID, TrixoPlayer player) {
        return this.isPlayerInTeam(teamID, player.getUUID());
    }

    public boolean isPlayerInTeam(String teamID, UUID uuid) {
        return this.getTeam(teamID).isPlayerInTeam(uuid);
    }

    public boolean isPlayerInTeam(UUID uuid) {
        for (Team team : this.getTeams().valueSet()) {
            return team.isPlayerInTeam(uuid);
        }

        return false;
    }
    
    /* Balancing */
    public void balancePlayers() {
        // ToDo: Party Checks
        // ToDo: Statistics
    }
    
    /** Getters, Setters, Adders, Removers **/
    /* Getters */
    public DataContainer<String, Team> getTeams() {
        return (DataContainer<String, Team>) this.data.get("teams");
    }

    public Team getTeam(String teamID) {
        return this.getTeams().get(teamID);
    }

    public Team getPlayerTeam(UUID uuid) {
        for (Team team : this.getTeams().valueSet()) {
            if (team.isPlayerInTeam(uuid)) {
                return team;
            }
        }

        return null;
    }

    /** Events **/
    /* Register */
    @Override
    public void register(Plugin plugin) {
        this.data.put("registered", true);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        this.data.put("registered", false);

        HandlerList.unregisterAll(this);
    }

    /* Bukkit Events */
    @EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getDamager() instanceof Player) {
                if (this.isPlayerInTeam(event.getEntity().getUniqueId())
                        && this.getPlayerTeam(event.getEntity().getUniqueId()) == this
                                .getPlayerTeam(event.getDamager().getUniqueId())) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
