package com.trixo.minigame;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.trixo.data.DataContainer;
import com.trixo.interfaces.IDataObject;
import com.trixo.server.event.ITrixoEvent;

public abstract class TrixoMinigame extends JavaPlugin implements IDataObject<String, Object>, ITrixoEvent {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public TrixoMinigame() {
        this.data = new DataContainer<String, Object>(false);
        
        this.data.put("mininumPlayers", 8);
        this.data.put("maximumPlayers", 16);
        this.data.put("timeLeft", 20);
        this.data.put("timerID", 0);
    }

    /** Getters, Setters **/
    /* Getters */
    public int getMininumPlayers() {
        return (Integer) this.data.get("mininumPlayers");
    }

    public int getMaximumPlayers() {
        return (Integer) this.data.get("maximumPlayers");
    }

    public Collection<? extends Player> getPlayers() {
        return this.getServer().getOnlinePlayers();
    }

    public int getPlayerCount() {
        return this.getPlayers().size();
    }

    public int getTimeLeft() {
        return (Integer) this.data.get("timeLeft");
    }

    private int getTimerID() {
        return (Integer) this.data.get("timerID");
    }

    /* Setters */
    public void setMininumPlayers(int N) {
        this.data.put("mininumPlayers", N);
    }

    public void setMaximumPlayers(int N) {
        this.data.put("maximumPlayers", N);
    }

    private void setTimeLeft(int N) {
        this.data.put("timeLeft", N);
    }

    private void setTimerID(int N) {
        this.data.put("timerID", N);
    }

    /** Minigame Functions **/
    public boolean canStart() {
        return this.getPlayerCount() >= this.getMininumPlayers();
    }

    public void tryStart() {
        if (this.canStart()) {
            this.setTimerID(Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
                @Override
                public void run() {
                    if (getTimeLeft() == 0) {
                        start();
                        setTimeLeft(20);
                        
                        Bukkit.getServer().getScheduler().cancelTask(getTimerID());
                    } else {
                        if (canStart()) {
                            setTimeLeft(getTimeLeft() - 1);
                            
                            onCount();
                        } else {
                            setTimeLeft(20);
                            
                            Bukkit.getServer().getScheduler().cancelTask(getTimerID());
                        }
                    }
                }
            }, 0L, 20L));
        }
    }

    public void start() {
        this.onStart();
    }

    public void stop() {
        this.onStop();
    }

    /** Events **/
    @Override
    public void register(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        HandlerList.unregisterAll((Listener) this);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        this.tryStart();
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        
    }

    public abstract void onCount();
    public abstract void onStart();
    public abstract void onStop();

    /** IDataObject **/
    @Override
    public DataContainer<String, Object> asDataContainer() {
        return this.data;
    }
}
