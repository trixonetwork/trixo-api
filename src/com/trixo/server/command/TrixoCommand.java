package com.trixo.server.command;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;

import com.trixo.TrixoAPI;
import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.utils.ReflectionUtils;

@SuppressWarnings("unchecked")
public abstract class TrixoCommand extends Command {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public TrixoCommand(String command) {
        super(command);

        this.data = new DataContainer<String, Object>(false);
        this.data.put("command", command);
        this.data.put("aliases", new ArrayList<String>());
    }

    public TrixoCommand(String... commands) {
        super(commands[0]);

        this.data = new DataContainer<String, Object>(false);
        this.data.put("command", commands[0]);
        this.data.put("aliases", Arrays.asList(ArrayUtils.subarray(commands, 1, commands.length)));

        this.setAliases(this.getAliases());
    }

    public TrixoCommand(String command, String... aliases) {
        super(command);

        this.data = new DataContainer<String, Object>(false);
        this.data.put("command", command);
        this.data.put("aliases", Arrays.asList(aliases));

        this.setAliases(this.getAliases());
    }

    /** Getters, Setters **/
    public String getCommand() {
        return (String) this.data.get("command");
    }

    public ArrayList<String> getAliases() {
        return (ArrayList<String>) this.data.get("aliases");
    }

    /** CommandExecutor Functions **/
    @Override
    @Deprecated
    public boolean execute(CommandSender sender, String command, String[] args) {
        ArrayList<String> arguments = new ArrayList<String>();
        arguments.addAll(Arrays.asList(args));

        return this.executeCommand(sender, arguments);
    }

    /** TrixoCommand Functions **/
    public void register() {
        try {
            final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);

            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            commandMap.register(this.getCommand(), this);

            for (String alias : this.getAliases()) {
                commandMap.register(alias, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unregister() {
        try {
            Object result = ReflectionUtils.getPrivateField(
                    DataCenter.retrieveData("api", "plugin", TrixoAPI.class).getServer().getPluginManager(), "commandMap");
            SimpleCommandMap commandMap = (SimpleCommandMap) result;

            this.unregister(commandMap);
        } catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public abstract boolean executeCommand(CommandSender sender, ArrayList<String> arguments);
}
