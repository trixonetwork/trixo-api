package com.trixo.server.command;

public class CommandHelper {
    /** CommandHelper Functions **/
    public static String extractSentence(int offset, Object[] args) {
        StringBuilder tmp = new StringBuilder();

        for (int i = offset; i < args.length; i++) {
            if (i != args.length - 1)
                tmp.append(args[i]).append(" ");
            else
                tmp.append(args[i]);
        }

        return tmp.toString();
    }
}