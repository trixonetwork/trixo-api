package com.trixo.server.event;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

public interface ITrixoEvent extends Listener {
    public void register(Plugin plugin);
    public void unregister();
}
