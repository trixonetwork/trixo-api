package com.trixo.server.permissions;

import com.trixo.data.DataContainer;

public class TrixoPermission {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public TrixoPermission(String permission) {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("permission", permission);
    }

    /** Getters, Setters **/
    public String getPermission() {
        return (String) this.data.get("permission");
    }
}
