package com.trixo.server.permissions;

import com.trixo.data.DataContainer;

public class GroupManager {
    private static DataContainer<String, PermissionGroup> permissionGroups = null;

    static {
        permissionGroups = new DataContainer<String, PermissionGroup>(false);
    }

    /** Getters, Setters **/
    /* Getters */
    public static DataContainer<String, PermissionGroup> getPermissionGroups() {
        return permissionGroups;
    }

    public static PermissionGroup getPermissionGroup(String name) {
        return permissionGroups.get(name);
    }

    /* Setters */
    public static void addPermissionGroup(String name, PermissionGroup group) {
        permissionGroups.put(name, group);
    }
}