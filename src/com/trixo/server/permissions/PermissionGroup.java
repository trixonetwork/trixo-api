package com.trixo.server.permissions;

import java.util.ArrayList;

import org.bukkit.ChatColor;

import com.trixo.data.DataContainer;

@SuppressWarnings("unchecked")
public class PermissionGroup {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public PermissionGroup() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("permissions", new ArrayList<TrixoPermission>());
    }

    /** Getters, Setters, Adders, Removers **/
    /* Getters */
    public ArrayList<TrixoPermission> getPermissions() {
        return (ArrayList<TrixoPermission>) this.data.get("permissions");
    }

    public String getDisplayTag() {
        return ((String) this.data.get("tagColour")) + ChatColor.BOLD + this.getTag();
    }

    public String getTag() {
        return (String) this.data.get("tag");
    }

    /* Setters */
    public void setTagColour(ChatColor tagColour) {
        this.data.put("tagColour", tagColour.toString());
    }

    public void setTag(String name) {
        this.data.put("tag", name);
    }

    /* Adders */
    public void addPermission(TrixoPermission permission) {
        this.getPermissions().add(permission);
    }

    /* Removers */
    public void removePermission(TrixoPermission permission) {
        this.getPermissions().remove(permission);
    }

    /** PermissionGroup Functions **/
    public boolean hasAllPermissions() {
        return false;
    }
}
