package com.trixo.server.chat;

public enum MessageType {
    /** Enum Data **/
    GADGET,
    FRIEND,
    SERVER;
}
