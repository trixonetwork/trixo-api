package com.trixo.server.chat;

import org.bukkit.ChatColor;

import com.trixo.data.DataContainer;

public class ChatMessage {
    private DataContainer<String, Object> data = new DataContainer<String, Object>(false);

    /** Getters, Setters **/
    /* Getters */
    public String getPrefix() {
        return (String) this.data.get("prefix");
    }

    public String getMessage() {
        return (String) this.data.get("message");
    }

    public String getColour() {
        return (String) this.data.get("colour");
    }

    /* Setters */
    public void setPrefix(String prefix) {
        this.data.put("prefix", prefix);
    }

    public void setMessage(String message) {
        this.data.put("message", message);
    }

    public void setColour(String colour) {
        this.data.put("colour", colour);
    }

    /** ChatMessage Functions **/
    @Override
    public String toString() {
        return this.getPrefix() + ChatColor.RESET + " " + this.getColour() + this.getMessage();
    }
}
