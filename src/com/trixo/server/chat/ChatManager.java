package com.trixo.server.chat;

import java.util.ArrayList;

import com.trixo.data.DataContainer;
import com.trixo.server.player.TrixoPlayer;

@SuppressWarnings("unchecked")
public class ChatManager {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public ChatManager() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("chatModules", new ArrayList<ChatModule>());

       // this.addModule(new SwearModule(this));
    }

    /** ChatManager Functions **/
    public String parse(TrixoPlayer sender, String input) {
        String output = input;

        for (ChatModule module : this.getModules()) {
            output = module.parse(sender, output);
        }

        return output;
    }

    /** Getters, Adders **/
    /* Getters */
    public ArrayList<ChatModule> getModules() {
        return (ArrayList<ChatModule>) this.data.get("chatModules");
    }

    public ChatModule getModule(int ID) {
        return this.getModules().get(ID);
    }

    /* Adders */
    public void addModule(ChatModule module) {
        this.getModules().add(module);
        this.getModules().sort((ChatModule m1, ChatModule m2) -> (m1.getPriority() - m2.getPriority()));
    }
}
