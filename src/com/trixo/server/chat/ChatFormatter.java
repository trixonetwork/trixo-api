package com.trixo.server.chat;

import org.bukkit.ChatColor;

public class ChatFormatter {
    /** ChatFormatter Functions **/
    public static ChatMessage create(ChatColor colour, String prefix, String msg) {
        ChatMessage message = new ChatMessage();
        message.setColour(colour.toString());
        message.setPrefix(prefix);
        message.setMessage(msg);

        return message;
    }
}
