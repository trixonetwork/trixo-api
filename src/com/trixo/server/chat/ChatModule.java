package com.trixo.server.chat;

import com.trixo.server.permissions.TrixoPermission;
import com.trixo.server.player.TrixoPlayer;

public abstract class ChatModule {
    /** ChatModule Functions **/
    public abstract String parse(TrixoPlayer sender, String input);
    
    /** Getters **/
    /* Getters */
    public abstract int getPriority();  
    public abstract TrixoPermission getBypassPermission();
}
