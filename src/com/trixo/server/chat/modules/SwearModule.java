package com.trixo.server.chat.modules;

import java.util.ArrayList;
import java.util.Arrays;

import com.trixo.data.DataContainer;
import com.trixo.server.chat.ChatManager;
import com.trixo.server.chat.ChatModule;
import com.trixo.server.permissions.TrixoPermission;
import com.trixo.server.player.TrixoPlayer;

@SuppressWarnings("unchecked")
public class SwearModule extends ChatModule {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public SwearModule(ChatManager chatManager) {
        this.data = new DataContainer<String, Object>(false);

        {
            ArrayList<String> swearWords = new ArrayList<String>();
            swearWords.add("fuck");
            swearWords.add("slut");
            swearWords.add("cunt");
            swearWords.add("nigger");
            swearWords.add("whore");
            swearWords.add("ass");
            swearWords.add("bitch");
            swearWords.add("prostitute");
            swearWords.add("penis");
            swearWords.add("vagina");
            swearWords.add("pussy");
            swearWords.add("wanker");
            swearWords.add("wank");
            swearWords.add("sex");
            swearWords.add("intercourse");
            swearWords.add("tit");
            swearWords.add("dick");
            swearWords.add("asshole");
            swearWords.add("faggot");
            swearWords.add("faggit");
            swearWords.add("fag");
            swearWords.add("retard");
            swearWords.add("anus");
            swearWords.add("dildo");
            swearWords.add("arse");
            swearWords.add("cum");
            swearWords.add("cumming");
            swearWords.add("semen");
            swearWords.add("cock");
            swearWords.add("jizz");
            swearWords.add("jack off");
            swearWords.add("jacking off");
            swearWords.add("shag");
            swearWords.add("a-hole");
            swearWords.add("kill yourself");
            swearWords.add("kill thyself");
            swearWords.add("shit");

            ArrayList<Character> charList = new ArrayList<Character>();
            charList.add(' ');
            charList.add('.');
            charList.add('-');
            charList.add('_');
            charList.add('/');
            charList.add('\\');
            charList.add('<');
            charList.add('>');
            charList.add('!');
            charList.add('*');
            charList.add('@');
            charList.add('$');
            charList.add('%');
            charList.add('&');
            charList.add('(');
            charList.add(')');
            charList.add('+');
            charList.add('=');

            {
                ArrayList<String> tempSwearWords = (ArrayList<String>) swearWords.clone();

                for (char toCheck : charList) {
                    for (String swearWord : tempSwearWords) {
                        for (int i = 0; i < swearWord.length(); i++) {
                            StringBuilder sb = new StringBuilder();

                            {
                                for (char c : Arrays.copyOfRange(swearWord.toCharArray(), 0, i)) {
                                    if (c != ' ') {
                                        sb.append(c).append(toCheck);
                                    }
                                }

                                sb.append(Arrays.copyOfRange(swearWord.toCharArray(), i, swearWord.length()));
                            }

                            if (!sb.toString().trim().equalsIgnoreCase(swearWord)) {
                                swearWords.add(sb.toString().trim());
                            }
                        }
                    }
                }
            }

            this.data.put("swearWords", swearWords);
        }
    }

    /** ChatModule Functions **/
    public String parse(TrixoPlayer sender, String input) {
        String output = input;

        for (String swearword : this.getSwearWords()) {
            if (input.contains(swearword.toLowerCase())) {
                String replace = "";

                for (int i = 0; i < swearword.length(); i++) {
                    replace += "*";
                }

                output = output.replace(swearword, replace);
            } else if (input.replace(" ", "").contains(swearword.toLowerCase())) {
                return input.replaceAll("[a-zA-Z0-9]", "*");
            }
        }

        return output;
    }

    /** Getters **/
    /* Getters */
    public int getPriority() {
        return 0;
    }

    public ArrayList<String> getSwearWords() {
        return (ArrayList<String>) this.data.get("swearWords");
    }

    @Override
    public TrixoPermission getBypassPermission() {
        return new TrixoPermission("trixo.antiswear");
    }
}
