package com.trixo.server.world;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.metadata.MetadataValue;

import com.trixo.data.DataContainer;

@SuppressWarnings({
        "unchecked", "deprecation"
})
public class TrixoBlock {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public TrixoBlock() {
        this(Material.AIR);
    }

    public TrixoBlock(Material type) {
        this.data = new DataContainer<String, Object>(false);
        
        this.setType(type);
        this.setData((byte) 0);
        this.setBiome(Biome.PLAINS);
    }

    public TrixoBlock(Block block) {
        this.data = new DataContainer<String, Object>(false);
        
        this.setBlock(block);
    }

    /** Getters, Setters **/
    /* Getters */
    public Block getBlock() {
        return (Block) this.data.get("block");
    }

    public Material getType() {
        return (Material) this.data.get("type");
    }

    public byte getData() {
        return (byte) this.data.get("data");
    }

    public Biome getBiome() {
        return (Biome) this.data.get("biome");
    }

    public DataContainer<String, MetadataValue> getMetadata() {
        return (DataContainer<String, MetadataValue>) this.data.get("metadata");
    }

    public MetadataValue getMetadata(String key) {
        return this.getMetadata().get(key);
    }

    /* Setters */
    public void setBlock(Block block) {
        this.data.put("block", block);
    }

    public void setType(Material type) {
        this.data.put("type", type);
    }

    public void setData(byte data) {
        this.data.put("data", data);
    }

    public void setBiome(Biome biome) {
        this.data.put("biome", biome);
    }

    public void setMetadata(String key, MetadataValue value) {
        this.getMetadata().put(key, value);
    }

    public void setColour(DyeColor color) {
        this.setData(color.getData());
    }

    /** TrixoBlock Functions **/
    public static TrixoBlock fromMaterial(Material type) {
        return new TrixoBlock(type);
    }
}
