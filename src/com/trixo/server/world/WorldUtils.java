package com.trixo.server.world;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.trixo.math.vector.Vector3;

@SuppressWarnings("deprecation")
public class WorldUtils {
    /** Block Functions **/
    /* Setters */
    public static void setBlock(World world, Vector3<?> position, TrixoBlock block) {
        Block b = world.getBlockAt(new Location(world, position.getX(), position.getY(), position.getZ()));
        
        b.setType(block.getType());
        b.setData(block.getData());
        b.setBiome(block.getBiome());
    }
    
    /* Getters */
    public static TrixoBlock getBlock(World world, Vector3<?> position) {
        Block block = world.getBlockAt(new Location(world, position.getX(), position.getY(), position.getZ()));
        
        return new TrixoBlock(block);
    }
}
