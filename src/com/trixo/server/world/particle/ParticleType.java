package com.trixo.server.world.particle;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.trixo.interfaces.IImmutable;
import com.trixo.math.colour.ColourRGB;
import com.trixo.math.vector.Vector3;
import com.trixo.math.vector.VectorUtils;
import com.trixo.server.player.TrixoPlayer;
import com.trixo.server.world.WorldUtils;
import com.trixo.utils.ReflectionUtils;
import com.trixo.utils.ReflectionUtils.PackageType;

@SuppressWarnings("unchecked")
public enum ParticleType {
    EXPLOSION_NORMAL("explode", 0, -1, ParticleProperty.DIRECTIONAL),
    EXPLOSION_LARGE("largeexplode", 1, -1),
    EXPLOSION_HUGE("hugeexplosion", 2, -1),
    FIREWORKS_SPARK("fireworksSpark", 3, -1, ParticleProperty.DIRECTIONAL),
    WATER_BUBBLE("bubble", 4, -1, ParticleProperty.DIRECTIONAL, ParticleProperty.REQUIRES_WATER),
    WATER_SPLASH("splash", 5, -1, ParticleProperty.DIRECTIONAL),
    WATER_WAKE("wake", 6, 7, ParticleProperty.DIRECTIONAL),
    SUSPENDED("suspended", 7, -1, ParticleProperty.REQUIRES_WATER),
    SUSPENDED_DEPTH("depthSuspend", 8, -1, ParticleProperty.DIRECTIONAL),
    CRIT("crit", 9, -1, ParticleProperty.DIRECTIONAL),
    CRIT_MAGIC("magicCrit", 10, -1, ParticleProperty.DIRECTIONAL),
    SMOKE_NORMAL("smoke", 11, -1, ParticleProperty.DIRECTIONAL),
    SMOKE_LARGE("largesmoke", 12, -1, ParticleProperty.DIRECTIONAL),
    SPELL("spell", 13, -1),
    SPELL_INSTANT("instantSpell", 14, -1),
    SPELL_MOB("mobSpell", 15, -1, ParticleProperty.COLOURABLE),
    SPELL_MOB_AMBIENT("mobSpellAmbient", 16, -1, ParticleProperty.COLOURABLE),
    SPELL_WITCH("witchMagic", 17, -1),
    DRIP_WATER("dripWater", 18, -1),
    DRIP_LAVA("dripLava", 19, -1),
    VILLAGER_ANGRY("angryVillager", 20, -1),
    VILLAGER_HAPPY("happyVillager", 21, -1, ParticleProperty.DIRECTIONAL),
    TOWN_AURA("townaura", 22, -1, ParticleProperty.DIRECTIONAL),
    NOTE("note", 23, -1, ParticleProperty.COLOURABLE),
    PORTAL("portal", 24, -1, ParticleProperty.DIRECTIONAL),
    ENCHANTMENT_TABLE("enchantmenttable", 25, -1, ParticleProperty.DIRECTIONAL),
    FLAME("flame", 26, -1, ParticleProperty.DIRECTIONAL),
    LAVA("lava", 27, -1),
    FOOTSTEP("footstep", 28, -1),
    CLOUD("cloud", 29, -1, ParticleProperty.DIRECTIONAL),
    REDSTONE("reddust", 30, -1, ParticleProperty.COLOURABLE),
    SNOWBALL("snowballpoof", 31, -1),
    SNOW_SHOVEL("snowshovel", 32, -1, ParticleProperty.DIRECTIONAL),
    SLIME("slime", 33, -1),
    HEART("heart", 34, -1),
    BARRIER("barrier", 35, 8),
    ITEM_CRACK("iconcrack", 36, -1, ParticleProperty.DIRECTIONAL, ParticleProperty.REQUIRES_DATA),
    BLOCK_CRACK("blockcrack", 37, -1, ParticleProperty.REQUIRES_DATA),
    BLOCK_DUST("blockdust", 38, 7, ParticleProperty.DIRECTIONAL, ParticleProperty.REQUIRES_DATA),
    WATER_DROP("droplet", 39, 8),
    ITEM_TAKE("take", 40, 8),
    MOB_APPEARANCE("mobappearance", 41, 8);

    private static final Map<String, ParticleType> NAME_MAP = new HashMap<String, ParticleType>();
    private static final Map<Integer, ParticleType> ID_MAP = new HashMap<Integer, ParticleType>();
    private final String name;
    private final int id;
    private final int requiredVersion;
    private final List<ParticleProperty> properties;

    static {
        for (ParticleType effect : values()) {
            NAME_MAP.put(effect.name, effect);
            ID_MAP.put(effect.id, effect);
        }
    }

    private ParticleType(String name, int id, int requiredVersion, ParticleProperty... properties) {
        this.name = name;
        this.id = id;
        this.requiredVersion = requiredVersion;
        this.properties = Arrays.asList(properties);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getRequiredVersion() {
        return requiredVersion;
    }

    public boolean hasProperty(ParticleProperty property) {
        return properties.contains(property);
    }

    public boolean isSupported() {
        if (requiredVersion == -1) {
            return true;
        }
        return ParticlePacket.getVersion() >= requiredVersion;
    }

    public static ParticleType fromName(String name) {
        for (Entry<String, ParticleType> entry : NAME_MAP.entrySet()) {
            if (!entry.getKey().equalsIgnoreCase(name)) {
                continue;
            }
            return entry.getValue();
        }
        return null;
    }

    public static ParticleType fromId(int id) {
        for (Entry<Integer, ParticleType> entry : ID_MAP.entrySet()) {
            if (entry.getKey() != id) {
                continue;
            }
            return entry.getValue();
        }
        return null;
    }

    private static boolean isWater(World world, Vector3<?> position) {
        Material material = WorldUtils.getBlock(world, position).getType();
        return material == Material.WATER || material == Material.STATIONARY_WATER;
    }

    private static boolean isLongDistance(World world, Vector3<?> particlePosition, List<TrixoPlayer> players) {
        for (TrixoPlayer player : players) {
            Vector3<IImmutable> playerPosition = (Vector3<IImmutable>) player.getPosition();
            Vector3<IImmutable> position = (Vector3<IImmutable>) particlePosition;

            Vector3<IImmutable> distance = position.sub(playerPosition);
            Vector3<IImmutable> distanceSQ = distance.pow(2);

            if (!world.getName().equals(player.getWorld().getName())
                    || distanceSQ.length() < new Vector3<IImmutable>(65536) {}.length()) {
                continue;
            }

            return true;
        }

        return false;
    }

    private static boolean isDataCorrect(ParticleType effect, ParticleData data) {
        return ((effect == BLOCK_CRACK || effect == BLOCK_DUST) && data instanceof BlockData)
                || (effect == ITEM_CRACK && data instanceof ItemData);
    }

    private static boolean isColourCorrect(ParticleType effect, ParticleColour colour) {
        return ((effect == SPELL_MOB || effect == SPELL_MOB_AMBIENT || effect == REDSTONE)
                && colour instanceof OrdinaryColour) || (effect == NOTE && colour instanceof NoteColour);

    }

    private static boolean isColourCorrect(ParticleType effect, ColourRGB<?> colour) {
        return true;
    }

    public void display(World world, Vector3<?> center, double range)
            throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect requires additional data");
        }

        if (hasProperty(ParticleProperty.REQUIRES_WATER) && !isWater(world, center)) {
            throw new IllegalArgumentException("There is no water at the center location");
        }

        new ParticlePacket(this, 0, 0, 0, 0, 1, range > 256, null).sendTo(world, center, range);
    }

    public void display(World world, float offsetX, float offsetY, float offsetZ, float speed, int amount,
            Vector3<?> center, double range)
                    throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect requires additional data");
        }

        if (hasProperty(ParticleProperty.REQUIRES_WATER) && !isWater(world, center)) {
            throw new IllegalArgumentException("There is no water at the center location");
        }

        new ParticlePacket(this, offsetX, offsetY, offsetZ, speed, amount, range > 256, null).sendTo(world, center,
                range);
    }

    public void display(World world, float offsetX, float offsetY, float offsetZ, float speed, int amount,
            Vector3<?> center, List<TrixoPlayer> players)
                    throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect requires additional data");
        }

        if (hasProperty(ParticleProperty.REQUIRES_WATER) && !isWater(world, center)) {
            throw new IllegalArgumentException("There is no water at the center location");
        }

        new ParticlePacket(this, offsetX, offsetY, offsetZ, speed, amount, isLongDistance(world, center, players), null)
                .sendTo(center, players);
    }

    public void display(World world, float offsetX, float offsetY, float offsetZ, float speed, int amount,
            Vector3<?> center, TrixoPlayer... players)
                    throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        display(world, offsetX, offsetY, offsetZ, speed, amount, center, Arrays.asList(players));
    }

    public void display(World world, Vector3<?> direction, Vector3<?> center, float speed, double range)
            throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect requires additional data");
        }

        if (!hasProperty(ParticleProperty.DIRECTIONAL)) {
            throw new IllegalArgumentException("This ParticleType effect is not directional");
        }

        if (hasProperty(ParticleProperty.REQUIRES_WATER) && !isWater(world, center)) {
            throw new IllegalArgumentException("There is no water at the center location");
        }

        new ParticlePacket(this, direction, speed, range > 256, null).sendTo(world, center, range);
    }
    
    public void display(World world, Vector3<?> direction, Vector3<?> center, double range)
            throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect requires additional data");
        }

        if (!hasProperty(ParticleProperty.DIRECTIONAL)) {
            throw new IllegalArgumentException("This ParticleType effect is not directional");
        }

        if (hasProperty(ParticleProperty.REQUIRES_WATER) && !isWater(world, center)) {
            throw new IllegalArgumentException("There is no water at the center location");
        }

        new ParticlePacket(this, direction, 1, range > 256, null).sendTo(world, center, range);
    }

    public void display(World world, Vector3<?> direction, float speed, Vector3<?> center, List<TrixoPlayer> players)
            throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect requires additional data");
        }

        if (!hasProperty(ParticleProperty.DIRECTIONAL)) {
            throw new IllegalArgumentException("This ParticleType effect is not directional");
        }

        if (hasProperty(ParticleProperty.REQUIRES_WATER) && !isWater(world, center)) {
            throw new IllegalArgumentException("There is no water at the center location");
        }

        new ParticlePacket(this, direction, speed, isLongDistance(world, center, players), null).sendTo(center,
                players);
    }

    public void display(World world, Vector3<?> direction, float speed, Vector3<?> center, TrixoPlayer... players)
            throws ParticleVersionException, ParticleDataException, IllegalArgumentException {
        display(world, direction, speed, center, Arrays.asList(players));
    }

    public void display(World world, ParticleColour colour, Vector3<?> center, double range)
            throws ParticleVersionException, ParticlecolourException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.COLOURABLE)) {
            throw new ParticlecolourException("This ParticleType effect is not colourable");
        }

        if (!isColourCorrect(this, colour)) {
            throw new ParticlecolourException("The particle colour type is incorrect");
        }

        new ParticlePacket(this, colour, range > 256).sendTo(world, center, range);
    }

    public void display(World world, ParticleColour colour, Vector3<?> center, double range, float speed)
            throws ParticleVersionException, ParticlecolourException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.COLOURABLE)) {
            throw new ParticlecolourException("This ParticleType effect is not colourable");
        }

        if (!isColourCorrect(this, colour)) {
            throw new ParticlecolourException("The particle colour type is incorrect");
        }

        new ParticlePacket(this, colour, speed, range > 256).sendTo(world, center, range);
    }

    public void display(World world, ParticleColour colour, Vector3<?> center, List<TrixoPlayer> players)
            throws ParticleVersionException, ParticlecolourException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.COLOURABLE)) {
            throw new ParticlecolourException("This ParticleType effect is not colourable");
        }

        if (!isColourCorrect(this, colour)) {
            throw new ParticlecolourException("The particle colour type is incorrect");
        }

        new ParticlePacket(this, colour, isLongDistance(world, center, players)).sendTo(center, players);
    }

    public void display(World world, ParticleColour colour, Vector3<?> center, TrixoPlayer... players)
            throws ParticleVersionException, ParticlecolourException {
        display(world, colour, center, Arrays.asList(players));
    }

    public void display(World world, ColourRGB<?> colour, Vector3<?> center, double range)
            throws ParticleVersionException, ParticlecolourException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.COLOURABLE)) {
            throw new ParticlecolourException("This ParticleType effect is not colourable");
        }

        if (!isColourCorrect(this, colour)) {
            throw new ParticlecolourException("The particle colour type is incorrect");
        }

        new ParticlePacket(this, colour, range > 256).sendTo(world, center, range);
    }

    public void display(World world, ColourRGB<?> colour, Vector3<?> center, double range, float speed)
            throws ParticleVersionException, ParticlecolourException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.COLOURABLE)) {
            throw new ParticlecolourException("This ParticleType effect is not colourable");
        }

        if (!isColourCorrect(this, colour)) {
            throw new ParticlecolourException("The particle colour type is incorrect");
        }

        new ParticlePacket(this, colour, speed, range > 256).sendTo(world, center, range);
    }

    public void display(World world, ColourRGB<?> colour, Vector3<?> center, List<TrixoPlayer> players)
            throws ParticleVersionException, ParticlecolourException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.COLOURABLE)) {
            throw new ParticlecolourException("This ParticleType effect is not colourable");
        }

        if (!isColourCorrect(this, colour)) {
            throw new ParticlecolourException("The particle colour type is incorrect");
        }

        new ParticlePacket(this, (ColourRGB<IImmutable>) colour, isLongDistance(world, center, players)).sendTo(center,
                players);
    }

    public void display(World world, ColourRGB<?> colour, Vector3<?> center, TrixoPlayer... players)
            throws ParticleVersionException, ParticlecolourException {
        display(world, colour, center, Arrays.asList(players));
    }

    public void display(World world, ParticleData data, float offsetX, float offsetY, float offsetZ, float speed,
            int amount, Vector3<?> center, double range) throws ParticleVersionException, ParticleDataException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect does not require additional data");
        }

        if (!isDataCorrect(this, data)) {
            throw new ParticleDataException("The particle data type is incorrect");
        }

        new ParticlePacket(this, offsetX, offsetY, offsetZ, speed, amount, range > 256, data).sendTo(world, center,
                range);
    }

    public void display(World world, ParticleData data, float offsetX, float offsetY, float offsetZ, float speed,
            int amount, Vector3<?> center, List<TrixoPlayer> players)
                    throws ParticleVersionException, ParticleDataException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect does not require additional data");
        }

        if (!isDataCorrect(this, data)) {
            throw new ParticleDataException("The particle data type is incorrect");
        }

        new ParticlePacket(this, offsetX, offsetY, offsetZ, speed, amount, isLongDistance(world, center, players), data)
                .sendTo(center, players);
    }

    public void display(World world, ParticleData data, float offsetX, float offsetY, float offsetZ, float speed,
            int amount, Vector3<?> center, TrixoPlayer... players)
                    throws ParticleVersionException, ParticleDataException {
        display(world, data, offsetX, offsetY, offsetZ, speed, amount, center, Arrays.asList(players));
    }

    public void display(World world, ParticleData data, Vector3<?> direction, float speed, Vector3<?> center,
            double range) throws ParticleVersionException, ParticleDataException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect does not require additional data");
        }

        if (!isDataCorrect(this, data)) {
            throw new ParticleDataException("The particle data type is incorrect");
        }

        new ParticlePacket(this, direction, speed, range > 256, data).sendTo(world, center, range);
    }

    public void display(World world, ParticleData data, Vector3<?> direction, float speed, Vector3<?> center,
            List<TrixoPlayer> players) throws ParticleVersionException, ParticleDataException {
        if (!isSupported()) {
            throw new ParticleVersionException("This ParticleType effect is not supported by your server version");
        }

        if (!hasProperty(ParticleProperty.REQUIRES_DATA)) {
            throw new ParticleDataException("This ParticleType effect does not require additional data");
        }

        if (!isDataCorrect(this, data)) {
            throw new ParticleDataException("The particle data type is incorrect");
        }

        new ParticlePacket(this, direction, speed, isLongDistance(world, center, players), data).sendTo(center,
                players);
    }

    public void display(World world, ParticleData data, Vector3<?> direction, float speed, Vector3<?> center,
            TrixoPlayer... players) throws ParticleVersionException, ParticleDataException {
        display(world, data, direction, speed, center, Arrays.asList(players));
    }

    public static enum ParticleProperty {
        REQUIRES_WATER,
        REQUIRES_DATA,
        DIRECTIONAL,
        COLOURABLE;
    }

    public static abstract class ParticleData {
        private final Material material;
        private final byte data;
        private final int[] packetData;

        @SuppressWarnings("deprecation")
        public ParticleData(Material material, byte data) {
            this.material = material;
            this.data = data;
            this.packetData = new int[] {
                    material.getId(), data
            };
        }

        public Material getMaterial() {
            return material;
        }

        public byte getData() {
            return data;
        }

        public int[] getPacketData() {
            return packetData;
        }

        public String getPacketDataString() {
            return "_" + packetData[0] + "_" + packetData[1];
        }
    }

    public static final class ItemData extends ParticleData {

        public ItemData(Material material, byte data) {
            super(material, data);
        }
    }

    public static final class BlockData extends ParticleData {

        public BlockData(Material material, byte data) throws IllegalArgumentException {
            super(material, data);
            if (!material.isBlock()) {
                throw new IllegalArgumentException("The material is not a block");
            }
        }
    }

    public static abstract class ParticleColour {
        public abstract float getValueX();

        public abstract float getValueY();

        public abstract float getValueZ();
    }

    public static final class OrdinaryColour extends ParticleColour {
        private ColourRGB<IImmutable> colour = null;

        public OrdinaryColour(ColourRGB<?> colour) throws IllegalArgumentException {
            this.colour = (ColourRGB<IImmutable>) colour;

            if (!this.colour.isByteColour())
                this.colour = (ColourRGB<IImmutable>) this.colour.convertColour();
        }

        public int getRed() {
            return (int) this.colour.getR();
        }

        public int getGreen() {
            return (int) this.colour.getG();
        }

        public int getBlue() {
            return (int) this.colour.getB();
        }

        @Override
        public float getValueX() {
            return (float) (this.getRed() / 255.0);
        }

        @Override
        public float getValueY() {
            return (float) (this.getGreen() / 255.0);
        }

        @Override
        public float getValueZ() {
            return (float) (this.getBlue() / 255.0);
        }
    }

    public static final class NoteColour extends ParticleColour {
        private final int note;

        public NoteColour(int note) throws IllegalArgumentException {
            if (note < 0) {
                throw new IllegalArgumentException("The note value is lower than 0");
            }

            if (note > 24) {
                throw new IllegalArgumentException("The note value is higher than 24");
            }

            this.note = note;
        }

        @Override
        public float getValueX() {
            return note / 24.0F;
        }

        @Override
        public float getValueY() {
            return 0;
        }

        @Override
        public float getValueZ() {
            return 0;
        }
    }

    private static final class ParticleDataException extends RuntimeException {
        private static final long serialVersionUID = 3203085387160737484L;

        public ParticleDataException(String message) {
            super(message);
        }
    }

    private static final class ParticlecolourException extends RuntimeException {
        private static final long serialVersionUID = 3203085387160737484L;

        public ParticlecolourException(String message) {
            super(message);
        }
    }

    private static final class ParticleVersionException extends RuntimeException {
        private static final long serialVersionUID = 3203085387160737484L;

        public ParticleVersionException(String message) {
            super(message);
        }
    }

    public static final class ParticlePacket {
        private static int version;
        private static Class<?> enumParticle;
        private static Constructor<?> packetConstructor;
        private static Method getHandle;
        private static Field playerConnection;
        private static Method sendPacket;
        private static boolean initialized;
        private final ParticleType effect;
        private float offsetX;
        private final float offsetY;
        private final float offsetZ;
        private final float speed;
        private final int amount;
        private final boolean longDistance;
        private final ParticleData data;
        private Object packet;

        public ParticlePacket(ParticleType effect, float d, float e, float f, float speed, int amount,
                boolean longDistance, ParticleData data) throws IllegalArgumentException {
            initialize();

            if (speed < 0) {
                throw new IllegalArgumentException("The speed is lower than 0");
            }

            if (amount < 0) {
                throw new IllegalArgumentException("The amount is lower than 0");
            }

            this.effect = effect;
            this.offsetX = d;
            this.offsetY = e;
            this.offsetZ = f;
            this.speed = speed;
            this.amount = amount;
            this.longDistance = longDistance;
            this.data = data;
        }

        public ParticlePacket(ParticleType effect, Vector3<?> direction, float speed, boolean longDistance,
                ParticleData data) throws IllegalArgumentException {

            this(effect, (float) direction.getX(), (float) direction.getY(), (float) direction.getZ(), speed, 0,
                    longDistance, data);
        }

        public ParticlePacket(ParticleType effect, ParticleColour colour, float speed, boolean longDistance) {
            this(effect, colour.getValueX(), colour.getValueY(), colour.getValueZ(), speed, 0, longDistance, null);

            if (effect == ParticleType.REDSTONE && colour instanceof OrdinaryColour
                    && ((OrdinaryColour) colour).getRed() == 0) {

                offsetX = (float) 1 / 255F;
            }
        }

        public ParticlePacket(ParticleType effect, ParticleColour colour, boolean longDistance) {
            this(effect, colour.getValueX(), colour.getValueY(), colour.getValueZ(), 1, 0, longDistance, null);

            if (effect == ParticleType.REDSTONE && colour instanceof OrdinaryColour
                    && ((OrdinaryColour) colour).getRed() == 0) {

                offsetX = (float) 1 / 255F;
            }
        }

        public ParticlePacket(ParticleType effect, ColourRGB<?> colour, boolean longDistance) {
            this(effect, new OrdinaryColour(colour), longDistance);
        }

        public ParticlePacket(ParticleType effect, ColourRGB<?> colour, float speed, boolean longDistance) {
            this(effect, new OrdinaryColour(colour), speed, longDistance);
        }

        public static void initialize() throws VersionIncompatibleException {
            if (initialized) {
                return;
            }

            try {
                version = Integer.parseInt(Character.toString(PackageType.getServerVersion().charAt(3)));

                if (version > 7) {
                    enumParticle = PackageType.MINECRAFT_SERVER.getClass("EnumParticle");
                }

                Class<?> packetClass = PackageType.MINECRAFT_SERVER
                        .getClass(version < 7 ? "Packet63WorldParticles" : "PacketPlayOutWorldParticles");

                packetConstructor = ReflectionUtils.getConstructor(packetClass);
                getHandle = ReflectionUtils.getMethod("CraftPlayer", PackageType.CRAFTBUKKIT_ENTITY, "getHandle");
                playerConnection = ReflectionUtils.getField("EntityPlayer", PackageType.MINECRAFT_SERVER, false,
                        "playerConnection");
                sendPacket = ReflectionUtils.getMethod(playerConnection.getType(), "sendPacket",
                        PackageType.MINECRAFT_SERVER.getClass("Packet"));
            } catch (Exception exception) {
                throw new VersionIncompatibleException(
                        "Your current bukkit version seems to be incompatible with this library", exception);
            }

            initialized = true;
        }

        public static int getVersion() {
            if (!initialized) {
                initialize();
            }

            return version;
        }

        public static boolean isInitialized() {
            return initialized;
        }

        private void initializePacket(Vector3<?> center) throws PacketInstantiationException {
            if (packet != null) {
                return;
            }

            try {
                packet = packetConstructor.newInstance();

                if (version < 8) {
                    String name = effect.getName();
                    if (data != null) {
                        name += data.getPacketDataString();
                    }
                    ReflectionUtils.setValue(packet, true, "a", name);
                } else {
                    ReflectionUtils.setValue(packet, true, "a", enumParticle.getEnumConstants()[effect.getId()]);
                    ReflectionUtils.setValue(packet, true, "j", longDistance);
                    if (data != null) {
                        int[] packetData = data.getPacketData();
                        ReflectionUtils.setValue(packet, true, "k",
                                effect == ParticleType.ITEM_CRACK ? packetData : new int[] {
                                        packetData[0] | (packetData[1] << 12)
                        });
                    }
                }

                ReflectionUtils.setValue(packet, true, "b", (float) center.getX());
                ReflectionUtils.setValue(packet, true, "c", (float) center.getY());
                ReflectionUtils.setValue(packet, true, "d", (float) center.getZ());
                ReflectionUtils.setValue(packet, true, "e", offsetX);
                ReflectionUtils.setValue(packet, true, "f", offsetY);
                ReflectionUtils.setValue(packet, true, "g", offsetZ);
                ReflectionUtils.setValue(packet, true, "h", speed);
                ReflectionUtils.setValue(packet, true, "i", amount);
            } catch (Exception exception) {
                throw new PacketInstantiationException("Packet instantiation failed", exception);
            }
        }

        public void sendTo(Vector3<?> center, TrixoPlayer player)
                throws PacketInstantiationException, PacketSendingException {
            initializePacket(center);
            try {
                sendPacket.invoke(playerConnection.get(getHandle.invoke(player.getPlayerInstance())), packet);
            } catch (Exception exception) {
                throw new PacketSendingException("Failed to send the packet to player '" + player.getName() + "'",
                        exception);
            }
        }

        public void sendTo(Vector3<?> center, Player player)
                throws PacketInstantiationException, PacketSendingException {
            initializePacket(center);
            try {
                sendPacket.invoke(playerConnection.get(getHandle.invoke(player)), packet);
            } catch (Exception exception) {
                throw new PacketSendingException("Failed to send the packet to player '" + player.getName() + "'",
                        exception);
            }
        }

        public void sendTo(Vector3<?> center, List<TrixoPlayer> players) throws IllegalArgumentException {
            if (players.isEmpty()) {
                throw new IllegalArgumentException("The player list is empty");
            }

            for (TrixoPlayer player : players) {
                sendTo(center, player);
            }
        }

        public void sendTo(World world, Vector3<?> center, double range) throws IllegalArgumentException {
            if (range < 1) {
                throw new IllegalArgumentException("The range is lower than 1");
            }

            String worldName = world.getName();
            double squared = range * range;

            for (Player player : Bukkit.getOnlinePlayers()) {
                Vector3<IImmutable> playerPosition = (Vector3<IImmutable>) VectorUtils
                        .positionFromLocation(player.getLocation(), false);
                Vector3<IImmutable> position = (Vector3<IImmutable>) center;

                Vector3<IImmutable> distance = position.sub(playerPosition);
                Vector3<IImmutable> distanceSQ = distance.pow(2);

                if (!player.getWorld().getName().equals(worldName) || (distanceSQ.getX() > squared
                        || distanceSQ.getY() > squared || distanceSQ.getZ() > squared)) {
                    continue;
                }

                sendTo(center, player);
            }
        }

        private static final class VersionIncompatibleException extends RuntimeException {
            private static final long serialVersionUID = 3203085387160737484L;

            public VersionIncompatibleException(String message, Throwable cause) {
                super(message, cause);
            }
        }

        private static final class PacketInstantiationException extends RuntimeException {
            private static final long serialVersionUID = 3203085387160737484L;

            public PacketInstantiationException(String message, Throwable cause) {
                super(message, cause);
            }
        }

        private static final class PacketSendingException extends RuntimeException {
            private static final long serialVersionUID = 3203085387160737484L;

            public PacketSendingException(String message, Throwable cause) {
                super(message, cause);
            }
        }
    }
}
