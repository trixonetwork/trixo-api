package com.trixo.server.player;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.trixo.constants.PlayerConstants.MorphType;
import com.trixo.constants.PlayerConstants.PlayerDataType;
import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.data.database.MongoDB;
import com.trixo.math.vector.Vector2;
import com.trixo.math.vector.Vector3;
import com.trixo.math.vector.VectorUtils;
import com.trixo.server.chat.ChatManager;
import com.trixo.server.inventory.Menu;
import com.trixo.server.permissions.GroupManager;
import com.trixo.server.permissions.PermissionGroup;
import com.trixo.server.permissions.TrixoPermission;
import com.trixo.utils.MorphUtils;

@SuppressWarnings({
        "deprecation", "unchecked", "unused"
})
public class TrixoPlayer implements Listener {
    private DataContainer<String, Object> data = null;
    private DataContainer<String, Object> publicData = null;

    /** Constructors **/
    public TrixoPlayer(Plugin plugin, Player player) {
        this.data = new DataContainer<String, Object>(false);
        this.publicData = new DataContainer<String, Object>(false);

        this.data.put("player", player);

        {
            player.setAllowFlight(true);
        }
    }

    /** Getters, Setters **/
    /* Getters */
    public Player getPlayerInstance() {
        return (Player) this.data.get("player");
    }

    public Menu getOpenedMenu() {
        return (Menu) this.data.get("openedMenu");
    }

    public String getName() {
        return this.getPlayerInstance().getName();
    }

    public String getDisplayName() {
        return this.getPlayerInstance().getDisplayName();
    }

    public UUID getUUID() {
        return this.getPlayerInstance().getUniqueId();
    }

    public Location getLocation() {
        return this.getPlayerInstance().getLocation();
    }

    public World getWorld() {
        return this.getPlayerInstance().getWorld();
    }

    public Vector3<?> getPosition() {
        return VectorUtils.positionFromLocation(this.getPlayerInstance().getLocation(), false);
    }

    public Vector2<?> getRotation() {
        return VectorUtils.rotationFromLocation(this.getPlayerInstance().getLocation(), false);
    }

    public PermissionGroup getPermissionGroup() {
        if (this.data.get("group") != null) {
            return (PermissionGroup) this.data.get("group");
        } else {
            DataContainer<String, Object> data = (DataContainer<String, Object>) this
                    .getPlayerData(PlayerDataType.PLAYER).get("player");

            return GroupManager.getPermissionGroup((String) data.get("group"));
        }
    }

    protected DataContainer<String, Object> getData() {
        return this.data;
    }

    public DataContainer<String, Object> getPublicData() {
        return this.data;
    }

    public MorphType getMorph() {
        return (MorphType) this.data.get("morph");
    }

    /* Setters */
    public void setDisplayName(String name) {
        this.getPlayerInstance().setDisplayName(name);
    }

    public void setDisplayTag(String tag) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        Team team = scoreboard.getTeam(this.getName());

        if (team == null) {
            team = scoreboard.registerNewTeam(this.getName());
        }

        team.setPrefix(tag);
        team.addPlayer(this.getPlayerInstance());

        for (Player tmp : Bukkit.getOnlinePlayers()) {
            tmp.setScoreboard(scoreboard);
        }
    }

    public void setDisplayTag(String teamName, String tag) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        Team team = scoreboard.getTeam(teamName);

        if (team == null) {
            team = scoreboard.registerNewTeam(teamName);
        }

        team.setPrefix(tag);
        team.addPlayer(this.getPlayerInstance());

        for (Player tmp : Bukkit.getOnlinePlayers()) {
            tmp.setScoreboard(scoreboard);
        }
    }

    public void setOpenedMenu(Menu menu) {
        this.data.put("openedMenu", menu);
    }

    public void setPermissionGroup(PermissionGroup group) {
        {
            /*
             * if (this.data.get("group") != null) { ((PermissionGroup)
             * this.data.get("group")).revokePermissions(this); }
             */

            this.data.put("group", group);
        }

        {
            this.setPermissionGroupTag(group);

            // group.grantPermissions(this);
            this.updatePermissionGroup(group);
        }
    }

    public void setPermissionGroupTag(PermissionGroup group) {
        if (group.getTag() == null || group.getTag().isEmpty()) {
            this.setDisplayName(ChatColor.YELLOW + this.getName() + ChatColor.RESET);
            this.setDisplayTag("" + ChatColor.YELLOW);
        } else {
            this.setDisplayName(group.getDisplayTag() + " " + ChatColor.YELLOW + this.getName() + ChatColor.RESET);
            this.setDisplayTag(group.getDisplayTag() + " " + ChatColor.RESET + "" + ChatColor.YELLOW);
        }
    }

    /** Disguise Functions **/
    public void morph(MorphType type) {
        if (this.data.containsKey("morph")) {
            this.unmorph();
            this.morph(type);
        } else {
            MorphUtils.morphToAll(this.getPlayerInstance(), type);

            this.data.put("morph", type);
        }
    }

    public void unmorph() {
        if (this.data.containsKey("morph")) {
            MorphUtils.removeMorph(this.getPlayerInstance());

            this.data.remove("morph");
        }
    }

    public boolean isMorphed() {
        return this.data.containsKey("morph");
    }

    /** Database Functions **/
    /* Getters */
    public DataContainer<String, Object> getPlayerData(PlayerDataType dataType) {
        DataContainer<String, Object> data = new DataContainer<String, Object>(false);

        if (dataType == PlayerDataType.PLAYER) {
            DataContainer<String, Object> playerData = new DataContainer<String, Object>(false);
            DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
            cData.put("host", "localhost");
            cData.put("port", 27017);
            cData.put("database", "trixo");
            cData.put("password", "");

            MongoDB database = new MongoDB();
            database.connect(cData);

            {
                BasicDBObject query = new BasicDBObject();
                query.put("player", this.getUUID().toString());

                DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
                sData.put("selectAll", false);
                sData.put("collection", "players");
                sData.put("query", query);

                DataContainer<String, Object> results = database.select(sData);

                DBCursor result = (DBCursor) results.get("result");
                while (result.hasNext()) {
                    DBObject obj = result.next();

                    playerData.put("online", obj.get("online"));
                    playerData.put("group", obj.get("group"));
                    playerData.put("partyID", obj.get("partyID"));
                    playerData.put("serverID", obj.get("serverID"));
                    playerData.put("GP", obj.get("GP"));
                    playerData.put("CP", obj.get("CP"));
                }
            }

            data.put("player", playerData);

            database.disconnect();
        } else if (dataType == PlayerDataType.FRIENDS) {
            ArrayList<String> friendsList = new ArrayList<String>();
            DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
            cData.put("host", "localhost");
            cData.put("port", 27017);
            cData.put("database", "trixo");
            cData.put("password", "");

            MongoDB database = new MongoDB();
            database.connect(cData);

            {
                BasicDBObject query = new BasicDBObject();
                query.put("player", this.getUUID().toString());

                DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
                sData.put("selectAll", false);
                sData.put("collection", "friends");
                sData.put("query", query);

                DataContainer<String, Object> results = database.select(sData);

                DBCursor result = (DBCursor) results.get("result");
                while (result.hasNext()) {
                    DBObject obj = result.next();

                    if (((String) obj.get("otherPlayer")).equals(this.getUUID().toString())) {
                        friendsList.add((String) obj.get("otherPlayer"));
                    }
                }
            }

            data.put("friends", friendsList);

            database.disconnect();
        }

        return data;
    }

    public DataContainer<String, Object> getStats() {
        return this.getPlayerData(PlayerDataType.STATS);
    }

    public ArrayList<String> getFriends() {
        return (ArrayList<String>) this.getPlayerData(PlayerDataType.FRIENDS).get("friends");
    }

    public DataContainer<String, Object> getCosmetics() {
        return this.getPlayerData(PlayerDataType.COSMETICS);
    }

    public DataContainer<String, Object> getClasses() {
        return this.getPlayerData(PlayerDataType.CLASSES);
    }

    /* Updaters */
    private void updatePermissionGroup(PermissionGroup group) {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        {
            BasicDBObject query = new BasicDBObject();
            query.put("player", this.getUUID().toString());

            BasicDBObject data = new BasicDBObject();
            data.put("$set", new BasicDBObject("group", this.getPermissionGroup().getTag()));

            DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
            iData.put("collection", "players");
            iData.put("query", query);
            iData.put("data", data);
            iData.put("multi", false);

            database.update(iData);
        }

        database.disconnect();
    }

    /* Register */
    public boolean isRegistered() {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        boolean exists = false;

        {
            BasicDBObject query = new BasicDBObject();
            query.put("player", this.getUUID().toString());

            DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
            sData.put("selectAll", false);
            sData.put("collection", "players");
            sData.put("query", query);

            DataContainer<String, Object> results = database.select(sData);

            DBCursor result = (DBCursor) results.get("result");
            exists = result.hasNext();
        }

        database.disconnect();

        return exists;
    }

    public void registerPlayer() {
        if (!this.isRegistered()) {
            DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
            cData.put("host", "localhost");
            cData.put("port", 27017);
            cData.put("database", "trixo");
            cData.put("password", "");

            MongoDB database = new MongoDB();
            database.connect(cData);

            {
                BasicDBObject data = new BasicDBObject();
                data.put("player", this.getUUID().toString());
                data.put("online", false);
                data.put("group", "NULL");
                data.put("partyID", "NULL");
                data.put("serverID", "NULL");
                data.put("GP", 0);
                data.put("CP", 0);

                DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
                iData.put("collection", "players");
                iData.put("data", data);
                iData.put("multi", false);

                database.insert(iData);
            }

            database.disconnect();
        }
    }

    /* Other */
    public void markOnline() {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        {
            BasicDBObject query = new BasicDBObject();
            query.put("player", this.getUUID().toString());

            BasicDBObject data = new BasicDBObject();
            data.put("$set", new BasicDBObject("online", true));

            DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
            iData.put("collection", "players");
            iData.put("query", query);
            iData.put("data", data);
            iData.put("multi", false);

            database.update(iData);
        }

        database.disconnect();
    }

    public void markOffline() {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        {
            BasicDBObject query = new BasicDBObject();
            query.put("player", this.getUUID().toString());

            BasicDBObject data = new BasicDBObject();
            data.put("$set", new BasicDBObject("online", false));

            DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
            iData.put("collection", "players");
            iData.put("query", query);
            iData.put("data", data);
            iData.put("multi", false);

            database.update(iData);
        }

        database.disconnect();
    }

    public boolean isOnline(String name) {
        if (Bukkit.getOfflinePlayer(name) != null) {
            return isOnline(Bukkit.getOfflinePlayer(name).getUniqueId());
        }

        return false;
    }

    public boolean isOnline(UUID uuid) {
        DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
        cData.put("host", "localhost");
        cData.put("port", 27017);
        cData.put("database", "trixo");
        cData.put("password", "");

        MongoDB database = new MongoDB();
        database.connect(cData);

        boolean isOnline = false;

        {
            BasicDBObject query = new BasicDBObject();
            query.put("player", this.getUUID().toString());

            DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
            sData.put("selectAll", false);
            sData.put("collection", "players");
            sData.put("query", query);

            DataContainer<String, Object> results = database.select(sData);

            DBCursor result = (DBCursor) results.get("result");
            while (result.hasNext()) {
                isOnline = ((boolean) result.next().get("online"));
            }
        }

        database.disconnect();

        return isOnline;
    }

    /** Friend Functions **/
    /* Getters */
    public int getFriendStatus(String name) {
        if (Bukkit.getOfflinePlayer(name) != null) {
            return (int) this.getFriendStatus(Bukkit.getOfflinePlayer(name).getUniqueId()).get("friendStatus");
        }

        return 0;
    }

    public DataContainer<String, Object> getFriendStatus(UUID uuid) {
        DataContainer<String, Object> data = new DataContainer<String, Object>(false);

        if (Bukkit.getOfflinePlayer(uuid) != null) {
            DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
            cData.put("host", "localhost");
            cData.put("port", 27017);
            cData.put("database", "trixo");
            cData.put("password", "");

            MongoDB database = new MongoDB();
            database.connect(cData);

            int friendStatus = 0;
            boolean inviter = false;

            {
                BasicDBObject query = new BasicDBObject();
                query.put("player", this.getUUID().toString());
                query.put("otherPlayer", uuid.toString());

                DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
                sData.put("selectAll", false);
                sData.put("collection", "friends");
                sData.put("query", query);

                DataContainer<String, Object> results = database.select(sData);

                DBCursor result = (DBCursor) results.get("result");
                while (result.hasNext()) {
                    DBObject obj = result.next();

                    if (!((boolean) obj.get("accepted"))) {
                        friendStatus = 1;
                        inviter = true;
                    } else {
                        if ((obj.get("otherPlayer").equals(uuid.toString()))) {
                            friendStatus = 2;
                            inviter = true;
                        }
                    }
                }
            }

            if (friendStatus == 0 || friendStatus == 1) {
                BasicDBObject query = new BasicDBObject();
                query.put("player", uuid.toString());
                query.put("otherPlayer", this.getUUID().toString());

                DataContainer<String, Object> sData = new DataContainer<String, Object>(false);
                sData.put("selectAll", false);
                sData.put("collection", "friends");
                sData.put("query", query);

                DataContainer<String, Object> results = database.select(sData);

                DBCursor result = (DBCursor) results.get("result");
                while (result.hasNext()) {
                    DBObject obj = result.next();

                    if (!((boolean) obj.get("accepted"))) {
                        friendStatus = 1;
                        inviter = false;
                    } else {
                        if ((obj.get("otherPlayer").equals(this.getUUID().toString()))) {
                            friendStatus = 2;
                            inviter = false;
                        }
                    }
                }
            }

            data.put("friendStatus", friendStatus);
            data.put("inviter", inviter);

            database.disconnect();

            return data;
        }

        return data;
    }

    /* Adders */
    public void addFriend(String name) {
        if (Bukkit.getOfflinePlayer(name) != null) {
            this.addFriend(Bukkit.getOfflinePlayer(name).getUniqueId());
        }
    }

    public void addFriend(UUID uuid) {
        if (Bukkit.getOfflinePlayer(uuid) != null) {
            if (((int) this.getFriendStatus(uuid).get("friendStatus")) == 0) {
                DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
                cData.put("host", "localhost");
                cData.put("port", 27017);
                cData.put("database", "trixo");
                cData.put("password", "");

                MongoDB database = new MongoDB();
                database.connect(cData);

                {
                    BasicDBObject toInsert = new BasicDBObject();
                    toInsert.put("player", this.getUUID().toString());
                    toInsert.put("otherPlayer", uuid.toString());
                    toInsert.put("accepted", false);

                    DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
                    iData.put("collection", "friends");
                    iData.put("query", toInsert);
                    iData.put("data", toInsert);
                    iData.put("multi", false);

                    database.upsert(iData);
                }

                database.disconnect();
            }
        }
    }

    /* Removers */
    public void removeFriend(String name) {
        if (Bukkit.getOfflinePlayer(name) != null) {
            this.removeFriend(Bukkit.getOfflinePlayer(name).getUniqueId());
        }
    }

    public void removeFriend(UUID uuid) {
        if (Bukkit.getOfflinePlayer(uuid) != null) {
            if (((int) this.getFriendStatus(uuid).get("friendStatus")) != 0) {
                DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
                cData.put("host", "localhost");
                cData.put("port", 27017);
                cData.put("database", "trixo");
                cData.put("password", "");

                MongoDB database = new MongoDB();
                database.connect(cData);

                {
                    BasicDBObject query = new BasicDBObject();
                    query.put("player", this.getUUID().toString());
                    query.put("otherPlayer", uuid.toString());

                    DataContainer<String, Object> rData = new DataContainer<String, Object>(false);
                    rData.put("collection", "friends");
                    rData.put("query", query);

                    database.remove(rData);
                }

                {
                    BasicDBObject query = new BasicDBObject();
                    query.put("player", uuid.toString());
                    query.put("otherPlayer", this.getUUID().toString());

                    DataContainer<String, Object> rData = new DataContainer<String, Object>(false);
                    rData.put("collection", "friends");
                    rData.put("query", query);

                    database.remove(rData);
                }

                database.disconnect();
            }
        }
    }

    /* Other */
    public void acceptFriend(String name) {
        if (Bukkit.getOfflinePlayer(name) != null) {
            this.acceptFriend(Bukkit.getOfflinePlayer(name).getUniqueId());
        }
    }

    public void acceptFriend(UUID uuid) {
        if (Bukkit.getOfflinePlayer(uuid) != null) {
            if (((int) this.getFriendStatus(uuid).get("friendStatus")) == 1) {
                DataContainer<String, Object> cData = new DataContainer<String, Object>(false);
                cData.put("host", "localhost");
                cData.put("port", 27017);
                cData.put("database", "trixo");
                cData.put("password", "");

                MongoDB database = new MongoDB();
                database.connect(cData);

                {
                    BasicDBObject query = new BasicDBObject();
                    query.put("player", uuid.toString());
                    query.put("otherPlayer", this.getUUID().toString());
                    query.put("accepted", false);

                    BasicDBObject data = new BasicDBObject();
                    data.put("player", uuid.toString());
                    data.put("otherPlayer", this.getUUID().toString());
                    data.put("accepted", true);

                    DataContainer<String, Object> iData = new DataContainer<String, Object>(false);
                    iData.put("collection", "friends");
                    iData.put("query", query);
                    iData.put("data", data);
                    iData.put("multi", false);

                    database.update(iData);
                }

                database.disconnect();
            }
        }
    }

    public boolean isFriend(String name) {
        if (Bukkit.getOfflinePlayer(name) != null) {
            return this.isFriend(Bukkit.getOfflinePlayer(name).getUniqueId());
        }

        return false;
    }

    public boolean isFriend(UUID uuid) {
        return (((int) this.getFriendStatus(uuid).get("friendStatus")) == 2);
    }

    /** Menu Functions **/
    public void openMenu(Menu menu) {
        if (!this.isMenuOpened()) {
            this.setOpenedMenu(menu);
            menu.open(this);
        } else {
            this.closeMenu();
            this.openMenu(menu);
        }
    }

    public void closeMenu() {
        if (this.getOpenedMenu() != null) {
            this.getOpenedMenu().close(this);
        }

        this.setOpenedMenu(null);
    }

    public boolean isMenuOpened() {
        return (this.data.get("openedMenu") != null);
    }

    /** Permission Functions **/
    public boolean hasPermission(TrixoPermission permission) {
        return this.getPermissionGroup().hasAllPermissions()
                || this.getPermissionGroup().getPermissions().contains(permission);
    }

    /** Events **/
    /* Bukkit Events */
    public void register(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        this.registerPlayer();
    }

    public void unregister() {
        {
            Bukkit.getScoreboardManager().getMainScoreboard().resetScores(this.getPlayerInstance().getName());
            this.getPlayerInstance().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        }

        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        String parsed = DataCenter.retrieveData("api", "chatManager", ChatManager.class).parse(this,
                event.getMessage());

        event.setMessage(parsed);
    }

    /* Custom Events */
    public void onJoin() {

    }

    public void onLeave() {
        this.unmorph();
    }
}
