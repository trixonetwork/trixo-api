package com.trixo.server.player;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataContainer;
import com.trixo.server.event.ITrixoEvent;

@SuppressWarnings("unchecked")
public class PlayerManager implements ITrixoEvent {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public PlayerManager(Plugin plugin) {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("plugin", plugin);
        this.data.put("players", new DataContainer<UUID, TrixoPlayer>(false));
    }

    /** Getters, Adders, Removers **/
    /* Getters */
    public DataContainer<UUID, TrixoPlayer> getPlayers() {
        return (DataContainer<UUID, TrixoPlayer>) this.data.get("players");
    }

    public TrixoPlayer getPlayer(Player player) {
        return this.getPlayers().get(player.getUniqueId());
    }

    public TrixoPlayer getPlayer(UUID uuid) {
        return this.getPlayers().get(uuid);
    }

    public Plugin getPlugin() {
        return (Plugin) this.data.get("plugin");
    }

    /* Adders */
    public void addPlayer(Player player) {
        this.addPlayer(new TrixoPlayer(this.getPlugin(), player));
    }

    public void addPlayer(TrixoPlayer player) {
        player.register(this.getPlugin());
        player.onJoin();

        this.getPlayers().put(player.getUUID(), player);
    }

    /* Removers */
    public void removePlayer(TrixoPlayer player) {
        this.removePlayer(player.getUUID());
    }

    public void removePlayer(Player player) {
        this.removePlayer(player.getUniqueId());
    }

    public void removePlayer(UUID uuid) {
        if (this.getPlayers().containsKey(uuid)) {
            TrixoPlayer player = this.getPlayers().get(uuid);

            player.onLeave();
            player.unregister();
        }
    }

    public void removePlayers() {
        for (TrixoPlayer player : this.getPlayers().valueSet()) {
            this.removePlayer(player);
        }

        this.getPlayers().clear();
    }

    /** Events **/
    @Override
    public void register(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        this.addPlayer(event.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        this.removePlayer(event.getPlayer());
    }

    /** PlayerManager Functions **/
    public void loadPlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.addPlayer(player);
        }
    }
}
