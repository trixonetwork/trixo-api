package com.trixo.server.player.disguisetest;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class TrixoPacketReceiver implements Listener {
    private Map<Player, TrixoPacketHandler> packetHandlers = new HashMap<>();

    public TrixoPacketReceiver(JavaPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        this.packetHandlers.put(event.getPlayer(), new TrixoPacketHandler(event.getPlayer()));
    }

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event) {
        this.packetHandlers.remove(event.getPlayer());
    }
}