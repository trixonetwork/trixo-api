package com.trixo.server.player.disguisetest;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.NetworkManager;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity.EnumEntityUseAction;

public class TrixoPacketHandler extends ChannelDuplexHandler {
    private Player player;
    private Channel channel;

    public TrixoPacketHandler(Player player) {
        this.player = player;
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();

        try {
            Field channelField = NetworkManager.class.getDeclaredField("channel");
            channelField.setAccessible(true);
            this.channel = (Channel) channelField.get(entityPlayer.playerConnection.networkManager);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return;
        }

        this.channel.pipeline().addBefore("packet_handler", "packet_listener_player", this);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof PacketPlayInUseEntity) {
            {
                Field field = PacketPlayInUseEntity.class.getDeclaredField("a");
                field.setAccessible(true);

                Player player = this.player;
                Entity entity = null;

                for (Entity e : player.getWorld().getEntities()) {
                    if (e.getEntityId() == field.getInt(msg)) {
                        entity = e;
                    }
                }

                if (entity instanceof Player) {
                    if (((PacketPlayInUseEntity) msg).a() == EnumEntityUseAction.INTERACT) {
                        Bukkit.getServer().getPluginManager().callEvent(new PlayerInteractEntityEvent(player, entity));

                        return;
                    }
                } else {
                    super.channelRead(ctx, msg);
                }
            }
        } else {
            super.channelRead(ctx, msg);
        }
    }
}