package com.trixo.server.entity;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.Entity;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import com.trixo.constants.PlayerConstants.MorphType;
import com.trixo.utils.MorphUtils;

import net.minecraft.server.v1_8_R3.World;

public class EntityManager {
    /** EntityManager Functions **/
    public static Entity spawnEntity(Location location, Class<?> clazz) {
        World world = (World) ((CraftWorld) location.getWorld()).getHandle();
        net.minecraft.server.v1_8_R3.Entity entity = null;

        {
            try {
                entity = (net.minecraft.server.v1_8_R3.Entity) clazz.getConstructor(World.class).newInstance(world);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                e.printStackTrace();
            }

            entity.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(),
                    location.getPitch());
            ((CraftLivingEntity) entity.getBukkitEntity()).setRemoveWhenFarAway(false);

            world.addEntity(entity, SpawnReason.CUSTOM);
        }

        return (CraftEntity) entity.getBukkitEntity();
    }

    public static Entity spawnEntity(Location location, net.minecraft.server.v1_8_R3.Entity entity) {
        World world = (World) ((CraftWorld) location.getWorld()).getHandle();

        {
            entity.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(),
                    location.getPitch());
            ((CraftLivingEntity) entity.getBukkitEntity()).setRemoveWhenFarAway(false);

            world.addEntity(entity, SpawnReason.CUSTOM);
        }

        return (CraftEntity) entity.getBukkitEntity();
    }

    public static void morphEntity(Entity entity, MorphType type) {
        MorphUtils.morphToAll(entity, type);
    }

    public static void unmorphEntity(Entity entity) {
        MorphUtils.removeMorph(entity);
    }
}
