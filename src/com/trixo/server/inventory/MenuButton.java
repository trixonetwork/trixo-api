package com.trixo.server.inventory;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.trixo.data.DataContainer;

public class MenuButton {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public MenuButton(int slot, ItemStack content, Menu menu) {
        this.data = new DataContainer<String, Object>(false);
        
        this.data.put("slot", slot);
        this.data.put("content", content);
        this.data.put("menu", menu);
    }

    /** Getters **/
    public int getSlot() {
        return (int) this.data.get("slot");
    }

    public ItemStack getContent() {
        return (ItemStack) this.data.get("content");
    }

    public MenuButtonEvent getEvent() {
        return (MenuButtonEvent) this.data.get("event");
    }

    /** Setters **/
    /* Setters */
    public void setEvent(MenuButtonEvent event) {
        this.data.put("event", event);
    }

    /** MenuButton Functions **/
    public void onClick(Player player) {
        this.getEvent().execute(player, this.getContent());
    }

    public void leave(MenuPage page) {
        page.getInventory().remove(this.getContent());
    }

    public void join(MenuPage page) {
        page.addButton(this);
    }
}
