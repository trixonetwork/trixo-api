package com.trixo.server.inventory;

import java.util.Collection;

import org.bukkit.entity.Player;

import com.trixo.TrixoAPI;
import com.trixo.data.DataCenter;
import com.trixo.data.DataContainer;
import com.trixo.server.player.TrixoPlayer;

@SuppressWarnings("unchecked")
public class Menu {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public Menu() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("pages", new DataContainer<String, MenuPage>(false));

        this.putPage("landing", null);
    }

    /** Menu Functions **/
    public void showPage(String ID, Player player) {
        {
            if (this.getCurrentPage() != null) {
                this.getCurrentPage().unregister();

                this.data.put("currentPage", null);
            }
        }

        if (this.getPage(ID) != null) {
            MenuPage page = this.getPage(ID);

            {
                page.show(player);
                page.register(DataCenter.retrieveData("api", "plugin", TrixoAPI.class));
            }

            this.data.put("currentPage", page);
        }
    }

    public void open(TrixoPlayer player) {
        this.showPage("landing", player.getPlayerInstance());
    }

    public void open(Player player) {
        this.showPage("landing", player);
    }

    public void close(TrixoPlayer player) {
        player.getPlayerInstance().closeInventory();

        if (this.getCurrentPage() != null) {
            this.getCurrentPage().unregister();
        }
    }

    public void close(Player player) {
        player.closeInventory();

        if (this.getCurrentPage() != null) {
            this.getCurrentPage().unregister();
        }
    }

    public boolean allowHotbarDrag() {
        return false;
    }

    /** Getters, Putters, Removers **/
    /* Getters */
    public MenuPage getCurrentPage() {
        return (MenuPage) this.data.get("currentPage");
    }

    public MenuPage getLandingPage() {
        return this.getPage("landing");
    }

    public MenuPage getPage(String ID) {
        return this.getPagesAndIDs().get(ID);
    }

    public DataContainer<String, MenuPage> getPagesAndIDs() {
        return (DataContainer<String, MenuPage>) this.data.get("pages");
    }

    public Collection<MenuPage> getPages() {
        return (Collection<MenuPage>) this.getPagesAndIDs().valueSet();
    }

    /* Adders */
    public void putPage(String ID, MenuPage page) {
        this.getPagesAndIDs().put(ID, page);
    }

    /* Removers */
    public void removePage(String ID) {
        this.getPagesAndIDs().remove(ID);
    }
}
