package com.trixo.server.inventory;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.trixo.data.DataContainer;
import com.trixo.server.event.ITrixoEvent;
import com.trixo.server.player.TrixoPlayer;
import com.trixo.utils.ItemUtils;

@SuppressWarnings("unchecked")
public class MenuPage implements ITrixoEvent {
    private DataContainer<String, Object> data = null;

    /** Constructors **/
    public MenuPage(Menu container, int size, String name) {
        this(container, size, name, null);
    }

    public MenuPage(Menu container, int size, String name, TrixoPlayer player) {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("buttons", new ArrayList<MenuButton>());
        this.data.put("size", size);
        this.data.put("name", name);
        this.data.put("inventory", Bukkit.createInventory(player.getPlayerInstance(), size <= 54 ? size : 54, name));
        this.data.put("registered", false);
        this.data.put("container", container);
        this.data.put("player", player);
    }

    public MenuPage(Menu container, int size) {
        this(container, size, "Menu");
    }

    public MenuPage(Menu container) {
        this(container, 9);
    }

    /** Getters, Setters, Adders, ETC **/
    /* Getters */
    public TrixoPlayer getPlayer() {
        return (TrixoPlayer) this.data.get("player");
    }

    public MenuButton getButton(int slot) {
        return this.getButtons().get(slot);
    }

    public MenuButton getButtonByContent(ItemStack content) {
        for (MenuButton button : this.getButtons()) {
            if (button != null) {
                if (content.getType() != Material.SKULL_ITEM) {
                    if (button.getContent().equals(content)) {
                        return button;
                    }
                } else {
                    if (ItemUtils.skullEquals(button.getContent(), content)) {
                        return button;
                    }
                }
            }
        }

        return null;
    }

    public ArrayList<MenuButton> getButtons() {
        return (ArrayList<MenuButton>) this.data.get("buttons");
    }

    public int getSize() {
        return (int) this.data.get("size");
    }

    public String getName() {
        return (String) this.data.get("name");
    }

    public Inventory getInventory() {
        return (Inventory) this.data.get("inventory");
    }

    public ItemStack[] getContents() {
        return this.getInventory().getContents();
    }

    public Menu getContainer() {
        return (Menu) this.data.get("container");
    }

    public int getNextSlot(boolean avoidEdge) {
        if (avoidEdge) {
            for (int i = 0; i < this.getSize(); i++) {
                boolean isEdge = ((i >= 0 && i <= 9) || i == 17 || i == 18 || i == 26 || i == 27 || i == 35 || i == 36
                        || (i >= 44 && i <= 53));

                if (!isEdge) {
                    int i2 = i + this.getButtons().size();
                    boolean isEdge2 = ((i2 >= 0 && i <= 9) || i2 == 17 || i2 == 18 || i2 == 26 || i2 == 27 || i2 == 35
                            || i2 == 36 || (i2 >= 44 && i2 <= 53));

                    if (!isEdge2) {
                        return i2;
                    } else {
                        return i2 + 4;
                    }
                }
            }
        } else {
            for (int i = 0; i < this.getSize(); i++) {
                if (i >= this.getButtons().size()) {
                    return i;
                }
            }
        }

        return this.getSize();
    }

    /* Setters */
    public void setButton(int slot, MenuButton button) {
        this.getButtons().set(slot, button);
        this.getInventory().setItem(slot, button.getContent());
    }

    /* Adders */
    public void addButton(MenuButton button) {
        ArrayList<MenuButton> buttons = this.getButtons();

        if (buttons.contains(button)) {
            buttons.set(buttons.indexOf(button), button);
        } else {
            buttons.add(button);
        }

        this.getInventory().setItem(button.getSlot(), button.getContent());
    }

    public void addAll(ArrayList<MenuButton> list) {
        {
            for (MenuButton button : this.getButtons()) {
                button.leave(this);
            }

            this.getButtons().clear();
        }

        {
            for (MenuButton button : list) {
                this.getInventory().setItem(button.getSlot(), button.getContent());
                this.getButtons().add(button);
            }
        }
    }

    /* Removers */
    public void removeButton(int slot) {
        ArrayList<MenuButton> data = this.getButtons();

        if (data.contains(slot)) {
            data.get(slot).leave(this);
            data.remove(slot);
        }
    }

    public void removeAll() {
        this.getButtons().clear();
    }

    /** Menu Functions **/
    /* Contains */
    public boolean containsContent(ItemStack item) {
        return this.getInventory().contains(item);
    }

    public boolean containsButton(MenuButton button) {
        return this.getButtons().contains(button);
    }

    /* Open, Close */
    public void show(TrixoPlayer player) {
        player.getPlayerInstance().openInventory(this.getInventory());
    }

    public void show(Player player) {
        player.openInventory(this.getInventory());
    }

    /* Other */
    public boolean isRegistered() {
        return ((boolean) this.data.get("registered"));
    }

    /** Events **/
    /* Register */
    @Override
    public void register(Plugin plugin) {
        this.data.put("registered", true);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void unregister() {
        this.data.put("registered", false);

        HandlerList.unregisterAll(this);
    }

    /* Events */
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        ItemStack clicked = event.getCurrentItem();
        Inventory inventory = event.getInventory();

        if (player == this.getPlayer().getPlayerInstance()) {
            if (inventory.getName().equals(this.getInventory().getName())) {
                if (this.getContainer().allowHotbarDrag()) {
                    if (this.containsContent(clicked)) {
                        if (this.getButtonByContent(clicked) != null) {
                            this.getButtonByContent(clicked).onClick(player);
                        }

                        event.setCancelled(true);
                    }
                } else {
                    if (this.containsContent(clicked)) {
                        if (this.getButtonByContent(clicked) != null) {
                            this.getButtonByContent(clicked).onClick(player);
                        }
                    }

                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getPlayer() == this.getPlayer().getPlayerInstance()) {
            this.unregister();
        }
    }
}