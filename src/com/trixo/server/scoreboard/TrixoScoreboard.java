package com.trixo.server.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.trixo.data.DataContainer;

public class TrixoScoreboard {
    private DataContainer<String, Object> data = null;

    /** Constructor **/
    public TrixoScoreboard() {
        this.data = new DataContainer<String, Object>(false);
        this.data.put("objectives", new DataContainer<String, Objective>(false));
    }

    /** Getters, Adders **/
    /* Getters */
    public Scoreboard getScoreboard() {
        return (Scoreboard) this.data.get("scoreboard");
    }

    /** TrixoScoreboard Functions **/
    public void create() {
        Scoreboard tmp = Bukkit.getScoreboardManager().getNewScoreboard();

        this.data.put("scoreboard", tmp);
    }

    public void createObjective() {
        
    }
}
