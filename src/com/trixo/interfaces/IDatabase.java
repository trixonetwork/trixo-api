package com.trixo.interfaces;

import com.trixo.data.DataContainer;
import com.trixo.data.DataResponse;

public interface IDatabase {
    /** Getters **/
    public Object getConnection();

    public Object getDatabase();

    /** Database Functions **/
    public DataResponse loadDriver();

    public DataResponse connect(DataContainer<String, Object> connectionData);

    public DataResponse disconnect();

    public DataResponse dropTable(DataContainer<String, Object> data);

    public DataContainer<String, Object> insert(DataContainer<String, Object> data);

    public DataContainer<String, Object> upsert(DataContainer<String, Object> data);

    public DataContainer<String, Object> update(DataContainer<String, Object> data);

    public DataContainer<String, Object> remove(DataContainer<String, Object> data);

    public DataContainer<String, Object> select(DataContainer<String, Object> data);
}
