package com.trixo.interfaces;

import com.trixo.data.DataContainer;

public interface IDataObject<K, V> {
    public DataContainer<K, V> asDataContainer();
}
